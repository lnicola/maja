#!/bin/sh -x
#
# Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

INSTALL_DIR=$1

export GDAL_CONFIG="$INSTALL_DIR"/bin/gdal-config
#install dependencies for python aws copernicus dem download (dependencies need python and GDAL)
"$INSTALL_DIR"/bin/python3 -m pip install --upgrade pip # needed for installing numba
"$INSTALL_DIR"/bin/python3 -m pip install boto3 eotile numba pillow