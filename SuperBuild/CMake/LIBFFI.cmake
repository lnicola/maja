#
# Copyright (C) 2005-2022 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# declare dependencies

ExternalProject_Add(LIBFFI
    PREFIX LIBFFI
    URL "https://github.com/libffi/libffi/releases/download/v3.3/libffi-3.3.tar.gz"
    URL_MD5 6313289e32f1d38a9df4770b014a2ca7
    TMP_DIR      LIBFFI/tmp
    STAMP_DIR    LIBFFI/stamp
    SOURCE_DIR   LIBFFI/source
    INSTALL_DIR ${SB_INSTALL_PREFIX}
    DOWNLOAD_DIR ${DOWNLOAD_DIR}
    CONFIGURE_COMMAND
    ${SB_ENV_CONFIGURE_CMD}
    ${CMAKE_BINARY_DIR}/LIBFFI/source/configure
    "--prefix=${SB_INSTALL_PREFIX}"
    BUILD_COMMAND $(MAKE)
    INSTALL_COMMAND $(MAKE) install
    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
)

