#
# Copyright (C) 2021 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
#                                                                                                          #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################
# to get the original .whl just change .tar.gz to  .whl in the following URL
set(IDNA_URL "https://files.pythonhosted.org/packages/a2/38/928ddce2273eaa564f6f50de919327bf3a00f091b5baba8dfa9460f3a8a8/idna-2.10-py2.py3-none-any.whl")
set(IDNA_URL_MD5 8f7d13f63706aa265d31ffedc8aa3053)
set(IDNA_DEPENDS PYTHON)
set(IDNA_AUTOCONF_BUILD 1)
build_projects(IDNA_DEPENDS)

set(IDNA_SB_SRC ${CMAKE_BINARY_DIR}/IDNA/source/)

ExternalProject_Add(IDNA
  PREFIX IDNA
  URL "${IDNA_URL}"
  URL_MD5 ${IDNA_URL_MD5}
  TMP_DIR      IDNA/tmp
  STAMP_DIR    IDNA/stamp
  SOURCE_DIR   IDNA/source
  INSTALL_DIR ${SB_INSTALL_PREFIX}
  DEPENDS ${IDNA_DEPENDS}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  DOWNLOAD_NO_EXTRACT ON
  CONFIGURE_COMMAND ${SB_INSTALL_PREFIX}/bin/pip3.8 -v --disable-pip-version-check --no-cache-dir --no-color
    install <DOWNLOADED_FILE>
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
  LOG_DOWNLOAD ${WRITE_LOG}
  LOG_CONFIGURE ${WRITE_LOG}
  LOG_BUILD ${WRITE_LOG}
  LOG_INSTALL ${WRITE_LOG}
  )

SUPERBUILD_PATCH_SOURCE(IDNA)


