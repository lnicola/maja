#
# Copyright (C) 2005-2022 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# declare dependencies
set(OPENSSL_DEPENDS ZLIB)
build_projects(OPENSSL_DEPENDS)

ExternalProject_Add(OPENSSL
    PREFIX OPENSSL
    DEPENDS ${OPENSSL_DEPENDS}
    URL "https://www.openssl.org/source/old/1.1.1/openssl-1.1.1f.tar.gz"
    URL_MD5 3f486f2f4435ef14b81814dbbc7b48bb
    TMP_DIR      OPENSSL/tmp
    STAMP_DIR    OPENSSL/stamp
    SOURCE_DIR   OPENSSL/source
    INSTALL_DIR ${SB_INSTALL_PREFIX}
    DOWNLOAD_DIR ${DOWNLOAD_DIR}
    CONFIGURE_COMMAND
    ${SB_ENV_CONFIGURE_CMD}
    ${CMAKE_BINARY_DIR}/OPENSSL/source/config ${OPENSSL_BUILD_ARCH}
    "--prefix=${SB_INSTALL_PREFIX}"
    "--libdir=lib"
    shared
    zlib
    zlib-dynamic
    "-I${SB_INSTALL_PREFIX}/include"
    "-L${SB_INSTALL_PREFIX}/lib"
    BUILD_COMMAND $(MAKE)
    INSTALL_COMMAND $(MAKE) install
    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
)

ExternalProject_Add_Step(OPENSSL remove_static
    COMMAND ${CMAKE_COMMAND} -E remove
    ${SB_INSTALL_PREFIX}/lib/libssl.a
    ${SB_INSTALL_PREFIX}/lib/libcrypto.a
    DEPENDEES install
)
