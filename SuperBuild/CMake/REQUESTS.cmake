#
# Copyright (C) 2021 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
#                                                                                                          #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################
# to get the original .whl just change .tar.gz to  .whl in the following URL
set(REQUESTS_URL "https://files.pythonhosted.org/packages/29/c1/24814557f1d22c56d50280771a17307e6bf87b70727d975fd6b2ce6b014a/requests-2.25.1-py2.py3-none-any.whl")
set(REQUESTS_URL_MD5 ec79209809129bf13cb002a0a573ef45)
set(REQUESTS_DEPENDS PYTHON CHARDET CERTIFI URLLIB IDNA TQDM)
set(REQUESTS_AUTOCONF_BUILD 1)
build_projects(REQUESTS_DEPENDS)

set(REQUESTS_SB_SRC ${CMAKE_BINARY_DIR}/REQUESTS/source/)

ExternalProject_Add(REQUESTS
  PREFIX REQUESTS
  URL "${REQUESTS_URL}"
  URL_MD5 ${REQUESTS_URL_MD5}
  TMP_DIR      REQUESTS/tmp
  STAMP_DIR    REQUESTS/stamp
  SOURCE_DIR   REQUESTS/source
  INSTALL_DIR ${SB_INSTALL_PREFIX}
  DEPENDS ${REQUESTS_DEPENDS}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  DOWNLOAD_NO_EXTRACT ON
  CONFIGURE_COMMAND ${SB_INSTALL_PREFIX}/bin/pip3.8 -v --disable-pip-version-check --no-cache-dir --no-color
    install <DOWNLOADED_FILE>
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
  LOG_DOWNLOAD ${WRITE_LOG}
  LOG_CONFIGURE ${WRITE_LOG}
  LOG_BUILD ${WRITE_LOG}
  LOG_INSTALL ${WRITE_LOG}
  )

SUPERBUILD_PATCH_SOURCE(REQUESTS)


