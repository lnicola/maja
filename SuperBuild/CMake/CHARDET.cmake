#
# Copyright (C) 2021 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
#                                                                                                          #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################
# to get the original .whl just change .tar.gz to  .whl in the following URL
set(CHARDET_URL "https://files.pythonhosted.org/packages/bc/a9/01ffebfb562e4274b6487b4bb1ddec7ca55ec7510b22e4c51f14098443b8/chardet-3.0.4-py2.py3-none-any.whl")
set(CHARDET_URL_MD5 0004b00caff7bb543a1d0d0bd0185a03)
set(CHARDET_DEPENDS PYTHON)
set(CHARDET_AUTOCONF_BUILD 1)
build_projects(CHARDET_DEPENDS)

set(CHARDET_SB_SRC ${CMAKE_BINARY_DIR}/CHARDET/source/)

ExternalProject_Add(CHARDET
  PREFIX CHARDET
  URL "${CHARDET_URL}"
  URL_MD5 ${CHARDET_URL_MD5}
  TMP_DIR      CHARDET/tmp
  STAMP_DIR    CHARDET/stamp
  SOURCE_DIR   CHARDET/source
  INSTALL_DIR ${SB_INSTALL_PREFIX}
  DEPENDS ${CHARDET_DEPENDS}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  DOWNLOAD_NO_EXTRACT ON
  CONFIGURE_COMMAND ${SB_INSTALL_PREFIX}/bin/pip3.8 -v --disable-pip-version-check --no-cache-dir --no-color
    install <DOWNLOADED_FILE>
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
  LOG_DOWNLOAD ${WRITE_LOG}
  LOG_CONFIGURE ${WRITE_LOG}
  LOG_BUILD ${WRITE_LOG}
  LOG_INSTALL ${WRITE_LOG}
  )

SUPERBUILD_PATCH_SOURCE(CHARDET)


