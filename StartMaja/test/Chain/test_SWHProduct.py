#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import unittest
from StartMaja.Common import TestFunctions, FileSystem
from StartMaja.Chain.Product import MajaProduct
from StartMaja.Chain.SWHProduct import SpotWorldHeritage, SpotWorldHeritage5
import os


class TestSWHProduct(unittest.TestCase):
    prod_swh_mus = [
        "SPOT1-HRV1-XS_20000224-111306-014_L1C_040-264-0_C_V1-0",
        "SPOT1-HRV2-XS_19900412-102956-272_L1C_061-338-0_D_V1-0",
        "SPOT2-HRV1-XS_20090604-111419-585_L1C_032-288-0_C_V1-0",
        "SPOT2-HRV2-XS_20090604-111420-918_L1C_031-288-0_C_V1-0",
        "SPOT3-HRV1-XS_19960805-105916-750_L1C_039-250-9_C_V1-0",
        "SPOT3-HRV2-XS_19961022-143202-509_L1C_668-319-0_C_V1-0",
        "SPOT4-HRVIR1-XS_20120528-100912-797_L1C_049-262-2_C_V1-0",
        "SPOT4-HRVIR2-XS_20120802-071609-657_L1C_121-283-0_C_V1-0",
        "SPOT5-HRG1-XS_20140903-101822-515_L1C_042-262-0_C_V1-0",
        "SPOT5-HRG2-XS_20110505-110942-923_L1C_039-251-0_C_V1-0"
    ]

    @classmethod
    def setUpClass(cls):
        """
        Simulate the basic folder + metadata_file structure
        :return:
        """
        import os

        for root in cls.prod_swh_mus:
            os.makedirs(root)
            metadata = os.path.join(root, root + "_MTD_ALL.xml")
            TestFunctions.touch(metadata)

    @classmethod
    def tearDownClass(cls):
        import shutil

        for root in cls.prod_swh_mus:
            shutil.rmtree(root)

    def test_reg_swh_muscate(self):
        for prod in self.prod_s2_mus:
            items = prod.split("_")
            date = items[1]
            level = items[2].lower()
            tile = items[3].lower()
            
            p = MajaProduct.factory(prod)
            if "SPOT5" in prod:
                self.assertIsInstance(p, SpotWorldHeritage5)
                
                self.assertEqual(
                    p.mnt_resolutions_dict,
                    [{"name": "XS", "val": "10 -10"}]
                )
            else:
                self.assertIsInstance(p, SpotWorldHeritage)
                self.assertEqual(
                    p.mnt_resolutions_dict,
                    [{"name": "XS", "val": "20 -20"}]
                )
            self.assertEqual(p.level, level)
            self.assertEqual(p.platform, "spot")
            self.assertEqual(p.type, "muscate")
            self.assertEqual(p.nodata, 0)
            self.assertEqual(p.tile, tile)
            self.assertEqual(p.date.strftime("%Y%m%d-%H%M%S-%f"), date)
            self.assertTrue(os.path.basename(p.metadata_file).endswith("_MTD_ALL.xml"))
            self.assertTrue(os.path.exists(p.metadata_file))
            self.assertEqual(p.validity, True)
            link_dir = "linkdir"
            FileSystem.create_directory(link_dir)
            p.link(link_dir)
            self.assertTrue(os.path.islink(os.path.join(link_dir, p.base)))
            self.assertEqual(p, p)
            FileSystem.remove_directory(link_dir)

