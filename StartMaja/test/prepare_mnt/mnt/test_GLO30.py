#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2022 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import unittest
from pathlib import Path
import tempfile
import numpy as np
from StartMaja.prepare_mnt.mnt import MNTBase, SiteInfo, CopDEMGLO30
from StartMaja.Common.GDalDatasetWrapper import GDalDatasetWrapper
from StartMaja.Common import FileSystem


class TestCOPDEM(unittest.TestCase):


    def setUp(self):
        if not hasattr(self, 'raw_copdem'):
            self.raw_copdem = tempfile.mkdtemp(prefix="raw_dem_")

    def test_get_CopDEMGLO30_codes_tls(self):
        site = SiteInfo.Site(
            "T31TCJ", 32631, ul=(300000.000, 4900020.000), lr=(409800.000, 4790220.000)
        )

        cop_codes, cop_aws_ids = CopDEMGLO30.CopDEMGLO30.get_CopDEMGLO30_codes(site)
        self.assertEqual(
            cop_codes,
            [
                "Copernicus_DSM_10_N43_00_E000_00_DEM.dt2",
                "Copernicus_DSM_10_N43_00_E001_00_DEM.dt2",
                "Copernicus_DSM_10_N44_00_E000_00_DEM.dt2",
                "Copernicus_DSM_10_N44_00_E001_00_DEM.dt2",
            ],
        )

    def test_prepare_mnt_tls(self):
        resx, resy = 10000, -10000
        site = SiteInfo.Site(
            "T31TCJ",
            32631,
            ul=(300000.000, 4900020.000),
            lr=(409800.000, 4790220.000),
            res_x=resx,
            res_y=resy,
        )
        cop_30_dem = CopDEMGLO30.CopDEMGLO30(
            site, raw_dem=self.raw_copdem, wdir=Path()
        )
        print("DEM Dir : " + cop_30_dem.raw_dem)
        mnt_cop_30 = cop_30_dem.prepare_mnt()
        self.assertTrue(Path(mnt_cop_30).is_file())
        driver = GDalDatasetWrapper.from_file(mnt_cop_30)
        self.assertEqual(driver.resolution, (resx, resy))
        self.assertEqual(driver.nodata_value, 0)
        FileSystem.remove_directory(self.raw_copdem)


if __name__ == "__main__":
    import sys
    # Set MAJA-Data Input directory
    if len(sys.argv) > 2 :
        sys.argv.pop()
        TestCOPDEM.raw_copdem = sys.argv.pop()
    unittest.main()
