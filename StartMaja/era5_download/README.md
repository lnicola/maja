# download_ERA5

This tool is designed to download hourly ERA5 reanalysis data products from ECMWF. Otherwise, the graphical interface provided by CDS is available here: https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=form

The tool download the Specific Humidity wich is stored in the SH file.

Specific Humidity files are available currently each hour. SH files are downloaded in netCDF format and look like this:
```
ERA5_SH_yyyymmddUTChhmm.nc
```

The files are then converted in one archive using the Earth Explorer format, which was selected as the standard format for MAJA external data. 

# Configuration

 - Create a CDS account: https://cds.climate.copernicus.eu

 - The folder cdsapi/ and the file setup.py must be in the working directory.

 - Get your API key at: https://cds.climate.copernicus.eu/user/YourUserID

 - Create the file '.cdsapirc' in your home and type:
```
{
"url" : "https://cds.climate.copernicus.eu/api/v2"
"key" : "your_key",
}
```

# Example

`python download_ERA5_hourly.py -l 19960805T10 19960805T11  -w /path/to/my/ERA5folderNetCDF -a  /path/to/my/ERA5folderNetDBL`


# Parameters

The user can choose the following parameters with the command line:

 - w: where ERA5 files will be stored

 - l: list of dates
 - d,f: min/max range of dates to download

 - w: path to folder where netcdf data are stored (can be considered as a temporary file)
 - a: path to folder where DBL/HDR files are stored
 - k: to keep the netcdf files

Other parameters could be accessed within the code :

 - name of output files
 
 - format: grib or netcdf

# Info on downloaded files

 - SH files: Specific humidity (SH: expressed in the mass of water vapour per kilogram of moist air [kg/kg]) at 36 different altitude levels (pressure levels).
```
Variable name	  Description
q	 	  Specific humidity
```

Information about the specific humidity can also be found at https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation, or at http://apps.ecmwf.int/codes/grib/param-db/
