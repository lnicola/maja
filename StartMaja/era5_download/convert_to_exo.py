#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import absolute_import
from __future__ import print_function

import argparse
from datetime import datetime
import os
import sys
import re

sys.path.append(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
)  # Import relative modules


class RawERA5Archive(object):
    """
    Converts a set of raw .nc files to the Maja format
    """

    @staticmethod
    def get_era5_root():
        """
        Create the root of a single era5 file
        :return:
        """
        from xml.etree import ElementTree
        from collections import namedtuple

        header_info = namedtuple("header_info", ("schema_path", "schema_version"))
        schema_by_sensor = header_info("EXO_ERA5_Era5Data.xsd", "1.00")

        xmlns = "http://eop-cfi.esa.int/CFI"
        xsi = "http://www.w3.org/2001/XMLSchema-instance"
        schema_location = "%s ./%s" % (xmlns, schema_by_sensor.schema_path)
        type_xsi = "ERA5_Header_Type"

        root = ElementTree.Element(
            "Earth_Explorer_Header",
            attrib={
                "xmlns": xmlns,
                "schema_version": schema_by_sensor.schema_version,
                "{" + xsi + "}schemaLocation": schema_location,
                "{" + xsi + "}type": type_xsi,
            },
        )
        return root

    @staticmethod
    def get_raw_era5_date(era5_file):
        """
        Get the date from a file of the format ERA5_XXX_yyyymmddUTChhmmss.nc
        :param era5_file: The filename string of the era5 file
        :param era5_group: Specify the type of ERA5 group the file should belong in.
                           By default this is either AOT, RH or MR
        :return: The date of the given file as well as it's group type.
        """
        regex = r"ERA5_SH_(\d{8}UTC\d{6})\.nc$"
        result = re.search(regex, era5_file)
        if result:
            return datetime.strptime(result.group(1), "%Y%m%dUTC%H%M%S"), "SH"
        return None

    @staticmethod
    def _update_nodes(
        root, mission, basename_out, date_end, acquisition_date, rel_netcdf
    ):
        """
        Update the HDR file with the needed information.
        :param root: The HDR xml-root.
        :param mission: The full mission name, e.g. 'SENTINEL2_'
        :param basename_out: The basename of the HDR
        :param date_end: The validity end date, usually 2100-01-01
        :param acquisition_date: The acquisition date
        :param rel_netcdf: The list of ERA5 files inside the .DBL.DIR. The filepaths are all relative to the HDR.
        :return: Writes the HDR in the same directory as the .DBL.DIR.
        """
        from xml.etree import ElementTree

        creation_date = datetime.now()
        a1 = ElementTree.SubElement(root, "Fixed_Header")
        toto = ElementTree.SubElement(a1, "File_Name")
        toto.text = basename_out
        toto = ElementTree.SubElement(a1, "File_Description")
        toto.text = "ERA5Data"
        toto = ElementTree.SubElement(a1, "Notes")
        toto.text = "ERA5 Reanalysis"
        toto = ElementTree.SubElement(a1, "Mission")
        toto.text = mission
        toto = ElementTree.SubElement(a1, "File_Class")
        toto.text = "TEST"
        toto = ElementTree.SubElement(a1, "File_Type")
        toto.text = "EXO_ERA5"
        b1 = ElementTree.SubElement(a1, "Validity_Period")
        toto = ElementTree.SubElement(b1, "Validity_Start")
        toto.text = "UTC=%s" % acquisition_date.strftime("%Y-%m-%dT%H:%M:%S")
        toto = ElementTree.SubElement(b1, "Validity_Stop")
        toto.text = "UTC=%s" % date_end.strftime("%Y-%m-%dT%H:%M:%S")
        toto = ElementTree.SubElement(a1, "File_Version")
        toto.text = "0003"
        b2 = ElementTree.SubElement(a1, "Source")
        toto = ElementTree.SubElement(b2, "System")
        toto.text = "MAJA"
        toto = ElementTree.SubElement(b2, "Creator")
        toto.text = "Start_Maja"
        toto = ElementTree.SubElement(b2, "Creator_Version")
        toto.text = "1.14"
        toto = ElementTree.SubElement(b2, "Creation_Date")
        toto.text = "UTC=" + creation_date.strftime("%Y-%m-%dT%H:%M:%S")

        a = ElementTree.SubElement(root, "Variable_Header")
        b2 = ElementTree.SubElement(a, "Main_Product_Header")
        ElementTree.SubElement(b2, "List_of_Consumers", count="0")
        ElementTree.SubElement(b2, "List_of_Extensions", count="0")
        b3 = ElementTree.SubElement(a, "Specific_Product_Header")
        b4 = ElementTree.SubElement(b3, "Instance_Id")

        b5 = ElementTree.SubElement(b4, "Validity_Period")
        toto = ElementTree.SubElement(b5, "Validity_Start")
        toto.text = "UTC=%s" % acquisition_date.strftime("%Y-%m-%dT%H:%M:%S")
        toto = ElementTree.SubElement(b5, "Validity_Stop")
        toto.text = "UTC=%s" % date_end.strftime("%Y-%m-%dT%H:%M:%S")
        b5 = ElementTree.SubElement(b4, "Acquisition_Date_Time")
        b5.text = "UTC=" + acquisition_date.isoformat()

        b4 = ElementTree.SubElement(b3, "Data_Block_Source_Location")
        b4.text = "https://www.ecmwf.int"

        b4 = ElementTree.SubElement(b3, "DBL_Organization")
        b5 = ElementTree.SubElement(
            b4, "List_of_Packaged_DBL_Files", count=str(len(rel_netcdf))
        )
        for index, era5_file in enumerate(rel_netcdf):
            b6 = ElementTree.SubElement(b5, "Packaged_DBL_File", sn=str(index + 1))
            b7 = ElementTree.SubElement(b6, "Relative_File_Path")
            b7.text = era5_file
            b7 = ElementTree.SubElement(b6, "File_Definition")
            b7.text = "era5"


    @staticmethod
    def create_archive(output_dir, netcdf, output_file_basename):
        """
        Create the DBL.DIR folder
        :param output_dir: The output directory
        :param netcdf: The list of netcdf files
        :param output_file_basename: The basename of the era5 file of format .._EXO_ERA5_...
        :return: The DBL folder path as well as the relative path to the era5 files.
        """
        import shutil
        from StartMaja.Common import FileSystem

        destination_filename = "{}.DBL".format(output_file_basename)
        destination_filepath = os.path.join(output_dir, destination_filename)
        dbl_dir = os.path.join(output_dir, output_file_basename + ".DBL.DIR")
        FileSystem.create_directory(dbl_dir)
        era5_file_to_return = []
        for ncf in netcdf:
            shutil.copy(ncf, os.path.join(dbl_dir, os.path.basename(ncf)))
            era5_file_to_return.append(
                os.path.join(destination_filename + ".DIR", os.path.basename(ncf))
            )
        return destination_filepath, era5_file_to_return

    @staticmethod
    def get_list_of_era5_files(input_dir):
        """
        Get all available era5 files inside a directory tree sorting out the duplicates.
        :return: The list of available era5 files.
        """
        from StartMaja.Common import FileSystem

        regex = r"ERA5_(SH)_(\d{8}UTC\d{6})\.nc$"
        raw_files = FileSystem.find(regex, input_dir)
        unique_files, basenames = [], []
        # Sort out duplicates by basename
        for i in raw_files:
            bname = os.path.basename(i)
            if bname not in basenames:
                basenames.append(bname)
                unique_files.append(i)
        return unique_files


    @staticmethod
    def process_one_file(output_dir, sh_file):
        """
        Process a single netcdf triplet
        :param output_dir: The existing output directory
        :param sh_file: The SH .netcdf file
        :return: Writes the DBL.DIR and HDR to the given output directory
        """
        from StartMaja.Common import XMLTools
        from collections import namedtuple
        satellite = namedtuple("satellite", ("full_name", "short_name"))
        current_satellite = satellite("SPOT", "SPOT")
        mission = "spot"
        date_end = datetime(year=2100, month=1, day=1)
        acq_date = RawERA5Archive.get_raw_era5_date(sh_file)[0]

        if not acq_date:
            raise ValueError("Cannot get Acquisition date for ERA5 file %s " % sh_file)

        output_file_basename = "{}_OPER_EXO_ERA5_{}".format(
            current_satellite.short_name,
            acq_date.strftime("%Y%m%dT%H%M%S")
        )

        # Create archive
        netcdf = [sh_file]
        dbl_filename, era5 = RawERA5Archive.create_archive(
            output_dir, netcdf, output_file_basename
        )

        # Create hdr
        output_filename = os.path.join(output_dir, output_file_basename + ".HDR")
        basename_out = os.path.basename(os.path.splitext(output_filename)[0])
        root = RawERA5Archive.get_era5_root()
        RawERA5Archive._update_nodes(
            root, current_satellite.full_name, basename_out, date_end, acq_date, era5
        )
        XMLTools.write_xml(root, output_filename)
