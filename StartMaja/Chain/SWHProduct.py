#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
from datetime import datetime
from StartMaja.Chain.Product import MajaProduct
from StartMaja.Common import FileSystem, XMLTools
from StartMaja.Common.FileSystem import symlink
from StartMaja.prepare_mnt.mnt.SiteInfo import Site
from StartMaja.prepare_mnt.mnt.MNTFactory import MNTFactory


class SpotWorldHeritage(MajaProduct):
    """
    A Sport World Heritage product
    """

    base_resolution = (20, -20)
    coarse_resolution = (100, -100)

    def get_mnt(self, **kwargs):
        """
        Download the DTM
        :param kwargs: Parameters for the MNTFactory
        :return: A derived MNTBase object, None if mnt_type unknown
        """
        sat = self.base.split("-")[0]
        instru = '-'.join(self.base.split("_")[0].split('-')[1:])
        date = '-'.join(self.base.split("_")[1].split('-')[:-1])
        try:
            # If coarse_res is provided
            tmp = kwargs.pop("coarse_res")
            coarse_res = (tmp, -tmp)
        except KeyError:
            coarse_res = self.coarse_resolution
        return MNTFactory(
            site=self.mnt_site,
            platform_id=sat+"-"+instru+"_"+date,
            mission_field=self.type_xml_maja,
            mnt_resolutions=self.mnt_resolutions_dict,
            coarse_res=coarse_res,
            **kwargs
        ).factory()

    @property
    def platform(self):
        return "spot"

    @property
    def short_name(self):
        return "swh"

    @property
    def type(self):
        return "muscate"

    @property
    def level(self):
        return self.base.split("_")[2].lower()

    @property
    def nodata(self):
        return 0

    @property
    def tile(self):
        # No tile for SWH, it returns the GEOGRAPHICAL_ZONE
        return self.base.split("_")[3].lower()

    @property
    def metadata_file(self):
        return self.find_file("*MTD_ALL.xml")[0]

    @property
    def date(self):
        str_date = self.base.split("_")[1]
        return datetime.strptime(str_date, "%Y%m%d-%H%M%S-%f")

    @property
    def validity(self):
        """
            Check validity of SPOT product
        """
        # Check that metadata file exist
        if os.path.exists(self.metadata_file):
            return True
        # Or check that JobProcessingInformation file exist
        if self.level == "l2a":
            try:
                jpi = self.find_file("*JPI_ALL.xml", self.fpath)
            except ValueError:
                return False
            validity_xpath = (
                "./Processing_Flags_And_Modes_List/Processing_Flags_And_Modes/Value"
            )
            processing_flags = XMLTools.get_xpath(jpi, validity_xpath)
            validity_flags = [flg.text for flg in processing_flags]
            if "L2VALD" in validity_flags:
                return True
        return False

    def link(self, link_dir):
        symlink(self.fpath, os.path.join(link_dir, self.base))

    @property
    def mnt_site(self):
        try:
            band_b2 = self.find_file(pattern=r"*XS0?2.tif")[0]
        except IOError as e:
            raise e
        return Site.from_raster(self.tile, band_b2)


    @property
    def mnt_resolutions_dict(self):
        return [
            {
                "name": "XS",
                "val": str(self.mnt_resolution[0]) + " " + str(self.mnt_resolution[1]),
            }
        ]

    def get_synthetic_band(self, synthetic_band, **kwargs):
        raise NotImplementedError

    def rgb_values(self):
        raise NotImplementedError

class SpotWorldHeritage5(SpotWorldHeritage):
    base_resolution = (10, -10)
