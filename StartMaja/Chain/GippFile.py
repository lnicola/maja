#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import logging
from datetime import datetime
from StartMaja.Chain.AuxFile import EarthExplorer
from orchestrator.common.version import MAJA_VERSION


class GIPPFile(EarthExplorer):
    regex = (
        r"\w+_"
        r"(TEST|PROD)_"
        r"GIP_"
        r"(L2ALBD|L2DIFT|L2DIRT|L2TOCR|L2WATV|L2COMM|L2SITE|L2SMAC|CKEXTL|CKQLTL|DIRCOR)_"
        r"\w_(\w+)_\d{5}_\d{8}_\d{8}\.(HDR|EEF)"
    )

    regex_dbl = (
        r"\w+_"
        r"(TEST|PROD)_"
        r"GIP_"
        r"(L2ALBD|L2DIFT|L2DIRT|L2TOCR|L2WATV|L2COMM|L2SITE|L2SMAC|CKEXTL|CKQLTL|DIRCOR)_"
        r"\w_(\w+)_\d{5}_\d{8}_\d{8}\.DBL(.DIR)?"
    )

    @staticmethod
    def get_mission(hdr):
        """
        Return the "Mission" field for a single GIPP file.
        :param hdr: The full path to the HDR file
        :return:
        """
        from StartMaja.Common import XMLTools

        return XMLTools.get_single_xpath(hdr, "./Fixed_Header/Mission")


class GippALBD(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2ALBD_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippDIFT(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2DIFT_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippDIRT(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2DIRT_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippTOCR(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2TOCR_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippWATV(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2WATV_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippCOMM(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2COMM_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippSITE(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2SITE_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippSMAC(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_L2SMAC_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippEXTL(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_CKEXTL_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippQLTL(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_CKQLTL_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippDIRCOR(EarthExplorer):
    regex = r"\w+_(TEST|PROD)_GIP_DIRCOR_\w_(\w+)_\d{5}_\d{8}_\d{8}\.\w+"


class GippSet(object):
    """
    Stores a set of Gipp Files
    """

    platforms = ["sentinel2", "landsat8", "venus", "spot"]

    # Default model
    models_const = ["CONTINEN"]

    # The old CAMS support the following models
    models_cams_old = [
        "BLACKCAR",
        "CONTINEN",
        "DUST",
        "ORGANICM",
        "SEASALT",
        "SULPHATE",
    ]

    # The new CAMS introduced two additional models
    models_cams_46r1 = models_cams_old + ["AMMONIUM", "NITRATE"]

    # The CAMS issued from re-analysis have the same model than old cams
    models_cams_rea = models_cams_old

    # It results in 3 unique possible model lists
    expected_models = [models_const, models_cams_old,  sorted(models_cams_46r1)]

    def __init__(self, root, platform, version, **kwargs):
        """
        Set the path to the root gipp folder
        :param root: The full path to the root gipp folder
        :param platform: The platform name. Has to be in ["sentinel2", "landsat8", "venus", "spot"]
        :param cams: Build GIPP with CAMS models
        :param log_level: The log level for the messages displayed.
        """
        from StartMaja.Common import FileSystem

        self.fpath = os.path.realpath(root)
        if platform not in self.platforms:
            raise ValueError("Unknown platform found: %s" % platform)

        self.platform = platform

        self.url = "https://gitlab.orfeo-toolbox.org/maja/maja-gipp2/-/archive/maja-{}/maja-gipp2-maja-{}.zip".format(
            version[:3], version[:3]
        )

        self.log_level = kwargs.get("log_level", logging.INFO)
        self.lut_date = kwargs.get("date", datetime.now())
        self.cams_46r1 = datetime(2019, 7, 10)
        self.n_sat = 2 if platform == "sentinel2" else 1
        self.n_dircor = 1 if platform == "sentinel2" else 0
        # Create root if not existing:
        FileSystem.create_directory(self.fpath)

        # Create folder names:
        self.gipp_archive = os.path.join(self.fpath, "archive.zip")
        # self.lut_archive = os.path.join(self.fpath, "lut_archive.zip")
        self.temp_folder = os.path.join(self.fpath, "tempdir")
        self.gipp_folder_name = "%s" % (self.platform.upper())
        self.out_path = os.path.join(self.fpath, self.gipp_folder_name)

    def __clean_up(self):
        """
        Clean up the download directory.
        :return:
        """
        from StartMaja.Common import FileSystem

        FileSystem.remove_directory(self.temp_folder)
        # FileSystem.remove_file(self.lut_archive)
        FileSystem.remove_file(self.gipp_archive)

    def download(self):
        """
        Download a specific set of Gipps to the given folder (.EEF, .HDR and .DBL.DIR).
        :return:
        """
        import shutil
        from StartMaja.Common import FileSystem

        self.out_path = os.path.join(self.fpath, self.gipp_folder_name)
        FileSystem.download_file(
            self.url + "?path={}".format(self.gipp_folder_name),
            self.gipp_archive,
            self.log_level,
        )
        FileSystem.unzip(self.gipp_archive, self.temp_folder)
        gipp_maja_git = os.path.join(
            self.temp_folder,
            os.path.basename(self.url).split(".zip")[0]
            + "-{}".format(self.gipp_folder_name),
        )
        # TODO Remove second WATV for Venus-Natif
        platform_folder = FileSystem.find_single(
            path=gipp_maja_git, pattern="^" + self.gipp_folder_name + "$"
        )
        if not platform_folder:
            self.__clean_up()
            raise OSError(
                "Cannot find any gipp folder for platform %s" % self.gipp_folder_name
            )

        if os.path.isdir(self.out_path):
            FileSystem.remove_directory(self.out_path)
        shutil.move(platform_folder, self.out_path)
        self.__clean_up()
        # Sanity check:
        if not self.check_completeness():
            raise ValueError(
                "GIPP download failed. Please delete gipp/ folder and try again."
            )

    def link(self, dest):
        """
        Symlink a set of Gipps to a given destination
        :param dest: The destination directory
        :return:
        """
        from StartMaja.Common import FileSystem

        eefs = FileSystem.find(GIPPFile.regex, self.out_path)
        dbls = FileSystem.find(GIPPFile.regex_dbl, self.out_path)
        for f in eefs + dbls:
            base = os.path.basename(f)
            FileSystem.symlink(f, os.path.join(dest, base))

    def get_models(self):
        """
        Get the list of models present in the gipp-set.
        :return: List of models in alphabetical order.
        """
        from StartMaja.Common import FileSystem
        import re

        hdr_reg = os.path.splitext(GIPPFile.regex)[0] + ".HDR"
        hdrs = FileSystem.find(hdr_reg, self.out_path, depth=1)
        raw_models = [
            re.search(hdr_reg, h).group(3).replace("_", "").upper() for h in hdrs
        ]
        models = list(set(raw_models))
        return sorted(models)

    def check_completeness(self):
        """
        Check if the Gipp-folder exists already
        :return: True if existing. False if not.
        """
        from StartMaja.Common import FileSystem

        # Check that the folder containing gipp exist
        if not os.path.isdir(self.out_path):
            return False

        # Assess that all gipp correspond to an expected model list.
        try:
            found_models = sorted(self.get_models())
            n_models = len(found_models)
        except ValueError:
            return False
        if found_models not in self.expected_models:
            return False
        
        # Check the list of files
        n_files_per_lut = 4
        try:
            hdrs = FileSystem.find("*.HDR", self.out_path)
            dbls = FileSystem.find("*.DBL.DIR", self.out_path)
            eefs = FileSystem.find("*.EEF", self.out_path)
        except ValueError:
            return False
        if len(eefs) < 3:
            return False
        # LUTs = 4 (TOCR, DIRT, DIFT, ALBD) + 1 constant for WATV per satellite + 1 DIRCOR for both sats if existing
        if (
            len(hdrs)
            != len(dbls)
            != n_files_per_lut * self.n_sat * n_models + 1 * self.n_sat + self.n_dircor
        ):
            return False

        return True


if __name__ == "__main__":
    import sys

    assert sys.version_info > (3, 0), "Please update Python to >3.0"
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o", "--outpath", help="output directroy", required=True, type=str
    )
    parser.add_argument(
        "-p",
        "--platform",
        help="plugin of the gipps",
        choices=["sentinel2", "landsat8", "venus", "spot"],
        required=True,
        type=str,
    )
    parser.add_argument(
        "-v",
        "--version",
        help="your major maja version. For example '4.5'. Each version of maja is referenced in the gipp repository as a tag.",
        required=True,
        type=str,
    )
    args = parser.parse_args()

    if len(args.version) > 3:
        args.version = args.version[:3]

    g = GippSet(args.outpath, args.platform, args.version)
    print(
        "Donwload gipp for the platform {} and compatible with maja v{} ...".format(
            args.platform, args.version
        )
    )
    g.download()
    assert g.check_completeness()
