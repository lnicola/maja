#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys

sys.path.append(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
)  # Import relative modules


def download_files(dt, file_type, t_hour, step, path_out, platform):
    """
    Downloads necessary CAMS data for MAJA and converts them into MAJA input format
    """
    from cdsapi import Client
    from datetime import datetime

    server = Client()

    date_courante = str(dt.year) + "%02d" % dt.month + "%02d" % dt.day
    print("Current_date =", date_courante)
    date = (
        str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
        + "/"
        + str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
    )

    # Determine the cams type to download 
    # and configure the params accordingly:
    # TODO check common params:
    # - all params are not described in the current online documentation
    # - there may be remains of the old ecmwf api to remove
    if platform == "spot":
        # If spot, we query from cams reanalysis repo
        cams_repo = "cams-global-reanalysis-eac4"
        params = {
            # common params
            "stream": "oper",
            "class": "mc",
            "expver": "eac4",
            "type": "forecast",
            "format": "netcdf",
            "date": date,
            "time": t_hour + ":00",
            }
        cams_type = "cams_rea"
        print("Download cams from reanalysis")
    else:
        # Else, we query from main cams repo
        cams_repo = "cams-global-atmospheric-composition-forecasts"
        params = {
            # common params
            "stream": "oper",
            "class": "mc",
            "expver": "0001",
            "step": "0",
            "type": "forecast",
            "format": "netcdf",
            "date": date,
            "time": t_hour + ":00",
            "leadtime_hour": "0",
            }
        # If date > 20190710, we query the new cams
        if dt >= datetime(2019, 7, 10):
            cams_type = "new_cams"
            print("Download new cams")
        # Else default cams
        else:
            cams_type = "cams"
            print("Download cams")

    # Define the suffix for the downloaded cams files
    cams_file_suffix = (date_courante
                        + "UTC"
                        + str(int(t_hour) + int(step)).zfill(2)
                        + "0000.nc")

    if file_type["surface"]:
        # Surface
        # Recupere AOT a 550nm pour BC, SS, SU, DU, OM
        # Et (si possible p. CAMS46r1) AM, NI
        if platform == "spot":
            nom_aot = "CAMS_RA_AOT_" + cams_file_suffix
        else:
            nom_aot = "CAMS_AOT_" + cams_file_suffix
        path_aot = os.path.join(path_out, nom_aot)
        print("Nom fichier de sortie AOT :", path_aot)

        models = [
                "dust_aerosol_optical_depth_550nm",
                "sea_salt_aerosol_optical_depth_550nm",
                "sulphate_aerosol_optical_depth_550nm",
                "black_carbon_aerosol_optical_depth_550nm",
                "organic_matter_aerosol_optical_depth_550nm",
                ]
        
        # If new cams, querying the additional models
        if cams_type == "new_cams":
            models.extend([
                           "nitrate_aerosol_optical_depth_550nm",
                           "ammonium_aerosol_optical_depth_550nm",
                          ])
        params_aot = params.copy()
        params_aot.update({
                        # add specific params
                        "variable": models,
                        })

        server.retrieve(
            cams_repo,
            params_aot,
            path_aot,
        )

    if file_type["pressure"]:
        # Pressure levels
        # Recupere Relative Humidity RH
        if platform == "spot":
            nom_rh = "CAMS_RA_RH_" + cams_file_suffix
        else:
            nom_rh = "CAMS_RH_" + cams_file_suffix

        path_rh = os.path.join(path_out, nom_rh)
        print("Nom fichier de sortie RH :", path_rh)
        params_rh = params.copy()
        params_rh.update({
                        # add specific params
                        "variable": ["relative_humidity"],
                        "pressure_level": [
                            "1",
                            "2",
                            "3",
                            "5",
                            "7",
                            "10",
                            "20",
                            "30",
                            "50",
                            "70",
                            "100",
                            "150",
                            "200",
                            "250",
                            "300",
                            "400",
                            "500",
                            "600",
                            "700",
                            "850",
                            "925",
                            "1000",
                        ],
                        })
        server.retrieve(
            cams_repo,
            params_rh,
            path_rh,
        )

    if file_type["model"]:
        # Model levels
        # Recupere les mixing ratios :
        # 3 bins DUST,
        # 3 bins SEASALT,
        # ORGANICMATTER hydrophile et hydrophobe,
        # BLACKCARBON hydrophile et hydrophobe,
        # et SULFATE.
        if platform == "spot":
            nom_mr = "CAMS_RA_MR_" + cams_file_suffix
        else:
            nom_mr = "CAMS_MR_" + cams_file_suffix

        path_mr = os.path.join(path_out, nom_mr)
        print("Nom fichier de sortie mixRatios :", path_mr)

        model_level = [str(i) for i in range(1, 61)]
        models = [
            "sea_salt_aerosol_0.03-0.5um_mixing_ratio",
            "sea_salt_aerosol_0.5-5um_mixing_ratio",
            "sea_salt_aerosol_5-20um_mixing_ratio",
            "dust_aerosol_0.03-0.55um_mixing_ratio",
            "dust_aerosol_0.55-0.9um_mixing_ratio",
            "dust_aerosol_0.9-20um_mixing_ratio",
            "hydrophilic_organic_matter_aerosol_mixing_ratio",
            "hydrophobic_organic_matter_aerosol_mixing_ratio",
            "hydrophilic_black_carbon_aerosol_mixing_ratio",
            "hydrophobic_black_carbon_aerosol_mixing_ratio",
            "sulphate_aerosol_mixing_ratio",
        ]
        # If new cams, querying the additional models
        if cams_type == "new_cams":
            model_level = [str(i) for i in range(1, 138)]
            models.extend([
                        "nitrate_fine_mode_aerosol_mass_mixing_ratio",
                        "nitrate_coarse_mode_aerosol_mass_mixing_ratio",
                        "ammonium_aerosol_mass_mixing_ratio",
                        ])
        params_mr = params.copy()
        params_mr.update({
                        # add specific params
                        "variable": models,
                        "model_level": model_level,
                        })

        server.retrieve(
            cams_repo,
            params_mr,
            path_mr,
        )

    return path_aot, path_rh, path_mr


if __name__ == "__main__":
    from convert_to_exo import RawCAMSArchive
    import datetime
    import argparse

    # ORDRES DE GRANDEUR
    # 1 fichier Surface  = 0.8 Mo
    # 1 fichier Pressure = 1.7 Mo
    # 1 fichier Model    = 53 Mo
    # => 20 Go par an (40 Go par an avec les deux forecasts (minuit et midi))

    # AEROSOLS DISPONIBLES
    # BC = BlackCarbon
    # SS = SeaSalt
    # SU = Sulfate
    # DU = Dust
    # OM = OrganicMatter

    # A partir du 20190710
    # AM = Ammonium
    # NI = Nitrate

    argParser = argparse.ArgumentParser()
    required_arguments = argParser.add_argument_group("required arguments")
    required_arguments.add_argument(
        "-d", "--start_date", required=True, help='start date, fmt("20171201")'
    )
    required_arguments.add_argument(
        "-f", "--end_date", required=True, help='end date, fmt("20171201")'
    )
    required_arguments.add_argument(
        "-a",
        "--archive_dir",
        required=True,
        help="Path where the archive DBL files are stored",
    )
    required_arguments.add_argument(
        "-w",
        "--write_dir",
        required=True,
        help="Temporary path where the products should be downloaded",
    )

    required_arguments.add_argument(
        "-k",
        "--keep",
        help="keep raw netcdf files",
        action="store_true",
        default=False,
        required=False,
    )
    required_arguments.add_argument(
        "-p", "--platform", choices=["s2", "l8", "ve", "spot"], required=True, default="s2"
    )
    required_arguments.add_argument(
        "-e",
        "--extented_time",
        help="Retrieve CAMS reanalysis every 3 hours (only for SPOT platform)",
        action="store_true",
        default=False,
        required=False,
    )

    args = argParser.parse_args()

    # Creation objets dates
    dt1 = datetime.datetime.strptime(args.start_date, "%Y%m%d")
    dt2 = datetime.datetime.strptime(args.end_date, "%Y%m%d")
    nb_days = (dt2 - dt1).days + 1
    print("Number of days =", nb_days)
    # Time de l'analyse voulue
    # Two possibilities :
    # - 00:00:00 UTC (minuit)
    # - 12:00:00 UTC (midi)
    time = ["00", "12"]

    # For CAMS reanalysis they are eight possibilities (for every 3 hours)
    if args.platform == "spot" and args.extented_time:
        time = ["00", "03", "06", "09", "12", "15", "18", "21"]
    elif args.extented_time:
        print('Extented time only for spot! Default time = ["00","12"]')
    # Step du forecast voulu
    # step = 3 indique qu'on telecharge les previsions a 3h apres l'heure de l'analyse.
    # Exemples : time = 00 et step = 3 => 03:00:00 UTC
    #            time = 12 et step = 3 => 15:00:00 UTC
    step_choice = "0"

    # Choix des fichiers a telecharger
    # Surface  : AOT (aerosol optical thickness)
    # Pressure : RH  (relative humidity)
    # Model    : MR  (mixing ratios)
    ftype = {"surface": True, "pressure": True, "model": True}

    # Create directories
    from StartMaja.Common import FileSystem

    FileSystem.create_directory(args.archive_dir)
    FileSystem.create_directory(args.write_dir)

    # Boucle sur les jours a telecharger
    for i in range(nb_days):
        date = dt1 + datetime.timedelta(days=i)
        print("==================================")
        print("Downloading files for date %s" % date)
        print("==================================")
        for t in range(len(time)):
            print(time[t])
            aot, rh, mr = download_files(
                date, ftype, time[t], step_choice, args.write_dir, args.platform
            )
            # # Conversion to MAJA DBL/HDR format
            RawCAMSArchive.process_one_file(
                args.archive_dir, aot, rh, mr, args.platform
            )
            if not args.keep:
                os.remove(aot)
                os.remove(mr)
                os.remove(rh)
