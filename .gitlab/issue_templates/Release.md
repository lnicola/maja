Issue to mark the release of Maja 4.x.x

* [ ] Create a new branch : release_v4.x.x
* [ ] Generate documentation
* [ ] Update ddc.txt
* [ ] Make sure the version number in `CMakeLists.txt` is 4.x.x
* [ ] Validate the package on Ubuntu / Debian
* [ ] Generate the docker image and publish it on docker hub
