// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 07 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 14 avril 2014 : Suppression du filtre non utilise         *
 *                                                                         m_PXDComputeRaisedBitsImageFilte *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 8 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL3QuicklookTool_h
#define __vnsL3QuicklookTool_h

#include "vnsChangeValueVectorFunctor.h"
#include "itkProcessObject.h"
#include "otbMultiChannelExtractROI.h"
#include "vnsLinearSubSampleVectorImageFilter.h"
#include "vnsLinearSubSampleImageFilter.h"
#include "otbVectorRescaleIntensityImageFilter.h"
#include "vnsScalarToRainbowVectorImageFilter.h"
#include "vnsWriteTxtOnImageFilter.h"
#include "otbImageFileReader.h"
#include "vnsMacro.h"

namespace vns
{
    /** \class  L3QuicklookTool
     * \brief This class implements ROI extraction and quicklook generation for L3 data.
     *
     *
     *
     * \author CS Systemes d'Information
     *
     * \sa ImageToImageFilter
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        class L3QuicklookTool : public itk::ProcessObject
        {
        public:
            /** Standard class typedefs. */
            typedef L3QuicklookTool Self;
            typedef itk::ProcessObject Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L3QuicklookTool, ImageToImageFilter )

            /** Some convenient typedefs. */
            typedef TInputReflectanceImage InputReflectanceImageType;
            typedef typename InputReflectanceImageType::ConstPointer InputReflectanceImageConstPointer;
            typedef typename InputReflectanceImageType::RegionType RegionType;
            typedef typename InputReflectanceImageType::PixelType InputReflectanceImagePixelType;
            typedef typename InputReflectanceImageType::InternalPixelType InputReflectanceImageInternalPixelType;
            typedef typename InputReflectanceImageType::SizeType SizeType;
            typedef TOutputImage OutputImageType;
            typedef typename OutputImageType::Pointer OutputImagePointer;
            typedef typename OutputImageType::PixelType OutputImagePixelType;
            typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;
            typedef TPXDTImage PXDInputImageType;
            typedef typename PXDInputImageType::PixelType PXDInputImagePixelType;
            typedef typename PXDInputImageType::ConstPointer PXDInputImageConstPointer;
            typedef typename PXDInputImageType::Pointer PXDInputImagePointer;

            typedef otb::MultiChannelExtractROI<InputReflectanceImageInternalPixelType, InputReflectanceImageInternalPixelType> MultiChannelExtractROIFilterType;
            typedef Functor::VectorChangeValue<InputReflectanceImagePixelType, InputReflectanceImagePixelType> VectorChangeFunctorType;
            typedef itk::UnaryFunctorImageFilter<InputReflectanceImageType, InputReflectanceImageType, VectorChangeFunctorType> ChangeValueVectorImageFilterType;
            typedef LinearSubSampleVectorImageFilter<InputReflectanceImageType, InputReflectanceImageType> QuicklookReflectanceImageGeneratorType;
            typedef otb::VectorRescaleIntensityImageFilter<InputReflectanceImageType, OutputImageType> VectorRescaleIntensityImageFilterType;

            typedef ScalarToRainbowVectorImageFilter<PXDInputImageType, OutputImageType> ScalarToRainbowVectorImageFilterType;
            typedef LinearSubSampleImageFilter<PXDInputImageType, PXDInputImageType> QuicklookPXDImageGeneratorType;

            typedef typename MultiChannelExtractROIFilterType::Pointer MultiChannelExtractROIFilterPointerType;
            typedef typename ChangeValueVectorImageFilterType::Pointer ChangeValueVectorImageFilterPointerType;
            typedef typename QuicklookReflectanceImageGeneratorType::Pointer QuicklookReflectanceImageGeneratorPointerType;
            typedef typename VectorRescaleIntensityImageFilterType::Pointer VectorRescaleIntensityImageFilterPointerType;
            typedef typename ScalarToRainbowVectorImageFilterType::Pointer ScalarToRainbowVectorImageFilterPointerType;
            typedef typename QuicklookPXDImageGeneratorType::Pointer QuicklookPXDImageGeneratorPointerType;
            typedef WriteTxtOnImageFilter<OutputImageType, OutputImageType> WriteTxtOnImageFilterType;
            typedef typename WriteTxtOnImageFilterType::Pointer WriteTxtOnImageFilterPointerType;
            typedef otb::ImageFileReader<OutputImageType> OutputImageReaderType;
            typedef typename OutputImageReaderType::Pointer OutputImageReaderPointerType;

            /** Input image accessors */
            vnsSetGetImageMacro( PXDInput, PXDInputImageType, 0)
            vnsSetGetImageMacro( SREInput, InputReflectanceImageType, 1)
            vnsSetGetImageMacro( FREInput, InputReflectanceImageType, 2)

            /** Output image accessors */
            OutputImageType*
            GetPXDOutput()
            {
                return m_PXDOutputImage;
            }
            OutputImageType*
            GetSREOutput()
            {
                return m_SREOutputImage;
            }
            OutputImageType*
            GetFREOutput()
            {
                if (m_UseFRE == true)
                {
                    return m_FREOutputImage;
                }
                else
                {
                    vnsExceptionBusinessMacro("FRE output not available.")
                }
            }

            /** Set the Font file name */

            /** Set/Get Macro */
            itkSetMacro(Ratio, unsigned int)
            itkGetMacro(Ratio, unsigned int)

            itkSetMacro(QLRedBand, unsigned int)
            itkGetMacro(QLRedBand, unsigned int)

            itkSetMacro(QLGreenBand, unsigned int)
            itkGetMacro(QLGreenBand, unsigned int)

            itkSetMacro(QLBlueBand, unsigned int)
            itkGetMacro(QLBlueBand, unsigned int)

            itkSetMacro(MinReflBRed, InputReflectanceImageInternalPixelType)
            itkGetMacro(MinReflBRed, InputReflectanceImageInternalPixelType)

            itkSetMacro(MinReflBGreen, InputReflectanceImageInternalPixelType)
            itkGetMacro(MinReflBGreen, InputReflectanceImageInternalPixelType)

            itkSetMacro(MinReflBBlue, InputReflectanceImageInternalPixelType)
            itkGetMacro(MinReflBBlue, InputReflectanceImageInternalPixelType)

            itkSetMacro(MaxReflBRed, InputReflectanceImageInternalPixelType)
            itkGetMacro(MaxReflBRed, InputReflectanceImageInternalPixelType)

            itkSetMacro(MaxReflBGreen, InputReflectanceImageInternalPixelType)
            itkGetMacro(MaxReflBGreen, InputReflectanceImageInternalPixelType)

            itkSetMacro(MaxReflBBlue, InputReflectanceImageInternalPixelType)
            itkGetMacro(MaxReflBBlue, InputReflectanceImageInternalPixelType)

            itkSetMacro(NoData, InputReflectanceImageInternalPixelType)
            itkGetMacro(NoData, InputReflectanceImageInternalPixelType)

            itkSetMacro(NoDataReplaceValue, OutputImageInternalPixelType)
            itkGetMacro(NoDataReplaceValue, OutputImageInternalPixelType)

            itkSetMacro(NbDate, unsigned int)
            itkGetMacro(NbDate, unsigned int)

            itkSetMacro(OutputMin, OutputImageInternalPixelType)
            itkGetMacro(OutputMin, OutputImageInternalPixelType)

            itkSetMacro(OutputMax, OutputImageInternalPixelType)
            itkGetMacro(OutputMax, OutputImageInternalPixelType)

            /** Set the Text Color */
            void
            SetTextColor(int redVal, int greenVal, int blueVal)
            {
                m_FREWriteTxtOnImageFilter->SetTextColor(redVal, greenVal, blueVal);
                m_SREWriteTxtOnImageFilter->SetTextColor(redVal, greenVal, blueVal);
                m_PXDWriteTxtOnImageFilter->SetTextColor(redVal, greenVal, blueVal);
            }

            itkSetMacro(Date, std::string)
            itkGetMacro(Date, std::string)

itkSetMacro        (FontSize, unsigned int)
        itkGetMacro(FontSize, unsigned int)

        itkSetMacro(FontFileName, std::string)
        itkGetMacro(FontFileName, std::string)

        itkSetMacro(UseFRE, bool)
        itkGetMacro(UseFRE, bool)

        void
        CheckInputs(void);
        void
        Generate(void);

    protected:
        /** Constructor */
        L3QuicklookTool();

        /** Destructor */
        virtual
        ~L3QuicklookTool();

        /** Composite Filter method. */
        //virtual void GenerateData(void);
        /** PrintSelf method */
        virtual void
        PrintSelf(std::ostream& os, itk::Indent indent) const;

    private:
        L3QuicklookTool(const Self&); //purposely not implemented
        void
        operator=(const Self&);//purposely not implemented

        unsigned int m_Ratio;
        unsigned int m_QLRedBand;
        InputReflectanceImageInternalPixelType m_MinReflBRed;
        InputReflectanceImageInternalPixelType m_MaxReflBRed;
        unsigned int m_QLGreenBand;
        InputReflectanceImageInternalPixelType m_MinReflBGreen;
        InputReflectanceImageInternalPixelType m_MaxReflBGreen;
        unsigned int m_QLBlueBand;
        InputReflectanceImageInternalPixelType m_MinReflBBlue;
        InputReflectanceImageInternalPixelType m_MaxReflBBlue;
        InputReflectanceImageInternalPixelType m_NoData;
        OutputImageInternalPixelType m_NoDataReplaceValue;
        unsigned int m_NbDate;
        OutputImageInternalPixelType m_OutputMin;
        OutputImageInternalPixelType m_OutputMax;

        QuicklookPXDImageGeneratorPointerType m_PXDQuicklookGenerator;
        ScalarToRainbowVectorImageFilterPointerType m_ScalarToRainbowVectorImageFilter;
        WriteTxtOnImageFilterPointerType m_PXDWriteTxtOnImageFilter;

        MultiChannelExtractROIFilterPointerType m_SREMultiChannelExtractROIFilter;
        ChangeValueVectorImageFilterPointerType m_SREChangeValueVectorImageFilter;
        QuicklookReflectanceImageGeneratorPointerType m_SREQuicklookGenerator;
        VectorRescaleIntensityImageFilterPointerType m_SREVectorRescaleIntensityImageFilter;
        WriteTxtOnImageFilterPointerType m_SREWriteTxtOnImageFilter;

        MultiChannelExtractROIFilterPointerType m_FREMultiChannelExtractROIFilter;
        ChangeValueVectorImageFilterPointerType m_FREChangeValueVectorImageFilter;
        QuicklookReflectanceImageGeneratorPointerType m_FREQuicklookGenerator;
        VectorRescaleIntensityImageFilterPointerType m_FREVectorRescaleIntensityImageFilter;
        WriteTxtOnImageFilterPointerType m_FREWriteTxtOnImageFilter;

        OutputImageReaderPointerType m_ScalarToRainbowVectorImageFilterReader;
        OutputImageReaderPointerType m_SREVectorRescaleIntensityImageFilterReader;
        OutputImageReaderPointerType m_FREVectorRescaleIntensityImageFilterReader;

        std::string m_Date;
        unsigned int m_FontSize;
        std::string m_FontFileName;

        bool m_UseFRE;

        OutputImagePointer m_PXDOutputImage;
        OutputImagePointer m_FREOutputImage;
        OutputImagePointer m_SREOutputImage;

    };

}
// End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL3QuicklookTool.txx"
#endif

#endif /* __vnsL3QuicklookTool_h */
