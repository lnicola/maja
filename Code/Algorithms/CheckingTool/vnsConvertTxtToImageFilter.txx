// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2526-CNES : 9 avril 2018 : Montée de version de d'OTB 6.0            *                                                                                                         *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 13 mars 2014 : Modifications mineures                     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 22 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsConvertTxtToImageFilter_txx
#define __vnsConvertTxtToImageFilter_txx

#include "vnsConvertTxtToImageFilter.h"
#include "gd.h"
#include "itksys/SystemTools.hxx"
#include "vnsMacro.h"

namespace vns
{

    /** Constructor */
    template<class TOutputImage>
        ConvertTxtToImageFilter<TOutputImage>::ConvertTxtToImageFilter() :
                m_FontSize(10), m_Red(255), m_Green(255), m_Blue(255)
        {
            m_OutputImage = OutputImageType::New();
            m_FontFileName = "";
            m_Text = "";
        }

    /** Destructor */
    template<class TOutputImage>
        ConvertTxtToImageFilter<TOutputImage>::~ConvertTxtToImageFilter()
        {
        }

    /** PrintSelf method */
    template<class TOutputImage>
        void
        ConvertTxtToImageFilter<TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

    /** Convert method with gd*/
    template<class TOutputImage>
        void
        ConvertTxtToImageFilter<TOutputImage>::Convert()
        {
            int color(0);

            char * text = const_cast<char *>(m_Text.data());
            char * fontName = const_cast<char *>(m_FontFileName.c_str()); //data());
            int brect[8];

            // Checks in the filename is ok
            if (itksys::SystemTools::FileExists(m_FontFileName.c_str()) == false)
            {
                vnsExceptionBusinessMacro(" The FontFileName <" << m_FontFileName << "> is not initialized or the path is wrong!")
            }

            // If the text is empty, throws an exception
            if (m_Text == "")
            {
                vnsExceptionBusinessMacro(" The Text is not initialized !")
            }

            vnsLogDebugMacro("ConvertTxtToImageFilter::Convert(): with fontName " << fontName << "',  FontSize '" << m_FontSize << "' and text '" << text << "'.")
            vnsLogSuperDebugMacro("Start 'gdImageStringFT' ...")
            gdImageStringFT(NULL, brect, 0, fontName, m_FontSize, 0., 0, 0, text);
	    
            const int brect_width = brect[4] - brect[0];
            const int brect_height = brect[1] - brect[5];
            // Loops on the dimensions
            for (int i = 0; i < 8; i++)
            {
	      vnsLogSuperDebugMacro("     brect[" << i << "] = " << brect[i])
            }
            vnsLogSuperDebugMacro("   =>   brect:                  " << brect[4] << ";" << brect[0]);
	    vnsLogSuperDebugMacro("                                " << brect[1] << ";" << brect[5]);
	    vnsLogSuperDebugMacro("   =>   brect_width             " << brect_width);
	    vnsLogSuperDebugMacro("   =>   brect_height            " << brect_height)

            if ((brect_width < 0) || (brect_height < 0))
            {
                vnsExceptionBusinessMacro(
                        " ConvertTxtToImageFilter: The image size [" << brect_width << ";" << brect_height << "] is bad !")
            }
   	    vnsLogSuperDebugMacro("Start 'gdImageCreateTrueColor' ...")
            // Init the GD image pointer
            gdImagePtr im = gdImageCreateTrueColor(brect_width, brect_height);

            vnsLogSuperDebugMacro("Start 'gdImageColorAllocate' ...")
            color = gdImageColorAllocate(im, m_Red, m_Green, m_Blue);
            vnsLogSuperDebugMacro("Start 'gdImageStringFT' with x = " << brect[0] / 2 << " and y = " << brect_height - brect[1] << " ...");
            // Write the text in color
            gdImageStringFT(im, brect, color, fontName, m_FontSize, 0, brect[0] / 2, brect_height - brect[1], text);

            // Create vector image
            typename OutputImageType::SizeType size;
            typename OutputImageType::RegionType full;
            typename OutputImageType::IndexType index_pixel;
            size[0] = gdImageSX(im);
            size[1] = gdImageSY(im);
            full.SetSize(size);
            const unsigned int l_NumberOfComponentsPerPixel(3);
            m_OutputImage->SetRegions(full);
            m_OutputImage->SetNumberOfComponentsPerPixel(l_NumberOfComponentsPerPixel);
            m_OutputImage->Allocate();

            typedef typename OutputImageType::PixelType OutputPixelType;
            OutputPixelType zero(l_NumberOfComponentsPerPixel);
            zero.Fill(0);
            // Initialize
            m_OutputImage->FillBuffer(zero);

            // Loop on gd image and set in the otb::image
            for (unsigned int x = 0; x < size[0]; x++)
            {
                for (unsigned int y = 0; y < size[1]; y++)
                {
                    OutputPixelType current(l_NumberOfComponentsPerPixel);
                    int p = gdImageGetPixel(im, x, y);
                    current[0] = gdImageRed(im, p);
                    current[1] = gdImageGreen(im, p);
                    current[2] = gdImageBlue(im, p);

                    index_pixel[0] = x;
                    index_pixel[1] = y;
                    m_OutputImage->SetPixel(index_pixel, current);
                }
            }

            gdImageDestroy(im);
            // MACCS 3.2 : Add gdFontCacheShutdown
           gdFontCacheShutdown();

	   vnsLogSuperDebugMacro("ConvertTxtToImageFilter<TOutputImage>::Convert done.")
        }

} // End namespace vns

#endif /* __vnsConvertTxtToImageFilter_txx */
