// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1755-CNES : 6 juillet 2016 : Implémentation pourcentages utils/valids *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-144674-CS : 26 mai 2016 : Modifications cosmetique et menage, etc...  *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-120653-CS : 3 avril 2014 : Correction de règles de qualité			*
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 17 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL3Extract_txx
#define __vnsL3Extract_txx

#include "vnsL3Extract.h"
#include <cmath>

namespace vns
{

    /** Constructor */
    template<class TInputReflectanceImage, class TInputMaskImage>
        L3Extract<TInputReflectanceImage, TInputMaskImage>::L3Extract() :
                m_NoData(0.), m_PXDMeans(0.), m_PXDStDv(0.), m_UseFRE(true), m_SREStatIsValid(false), m_FREStatIsValid(false), m_PXDStatIsValid(
                        false)
        {
            //Create input images pointer
            m_SATImage = InputMaskImageType::New();
            m_PXDImage = InputMaskImageType::New();
            m_SREImage = InputReflectanceImageType::New();
            m_FREImage = InputReflectanceImageType::New();
        }

    /** Destructor */
    template<class TInputReflectanceImage, class TInputMaskImage>
        L3Extract<TInputReflectanceImage, TInputMaskImage>::~L3Extract()
        {
        }

    template<class TInputReflectanceImage, class TInputMaskImage>
        void
        L3Extract<TInputReflectanceImage, TInputMaskImage>::ComputeIndexStat(IndexType & id)
        {
            // Update output information image
            m_SATImage->UpdateOutputInformation();
            m_PXDImage->UpdateOutputInformation();
            m_SREImage->UpdateOutputInformation();

            //Get Radius
            unsigned int radius = this->GetRadius();

            //initialize variables for computing means

            //Set valid to false
            m_SREStatIsValid = false;
            m_FREStatIsValid = false;
            m_PXDStatIsValid = false;

            //InputReflectanceImagePixelType freMeans;
            //freMeans.SetSize(m_FREImage->GetNumberOfComponentsPerPixel());
            //freMeans.Fill(0);

            InputReflectanceImagePixelType srePixVal;
            //InputReflectanceImagePixelType frePixVal;

            //compute upper left point and size of the extract using m_Radius.
            // -1 to follow OTB framework where indexes start at 0
            unsigned int startX = id[0] - radius - 1;
            unsigned int startY = id[1] - radius - 1;
            unsigned int sizeX = 2 * radius + 1;
            unsigned int sizeY = 2 * radius + 1;

            // Initialization
            RegionType l_ExtractedRegion;
            IndexType idROI;
            idROI[0] = startX;
            idROI[1] = startY;
            l_ExtractedRegion.SetIndex(idROI);
            SizeType size;
            size[0] = sizeX;
            size[1] = sizeY;
            l_ExtractedRegion.SetSize(size);
            m_ValidPixelsPercentage.SetSize(m_SREImage->GetNumberOfComponentsPerPixel());
            m_ValidPixelsPercentage.Fill(0.);
            m_UsefulPixelsPercentage.SetSize(m_SREImage->GetNumberOfComponentsPerPixel());
            m_UsefulPixelsPercentage.Fill(0.);

            // Check work region availability
            RegionType l_Region = m_SATImage->GetLargestPossibleRegion();

            if (l_Region != m_PXDImage->GetLargestPossibleRegion() || l_Region != m_SREImage->GetLargestPossibleRegion()) //|| l_Region !=  m_FREImage->GetLargestPossibleRegion() )
            {
                vnsExceptionBusinessMacro("Input images size mismatch");
            }

            // The FRE image is optional in the L2 and therefore in the L3 products
            if (m_UseFRE == true)
            {
                m_FREImage->UpdateOutputInformation();
                if (l_Region != m_FREImage->GetLargestPossibleRegion())
                {
                    vnsExceptionBusinessMacro("Input images size mismatch");
                }
            }

            if (l_ExtractedRegion.Crop(l_Region) != false)
            {
                //get the pixel neighborhood for computing sre and fre means (or only the pixel if radius=0)
                //this is made with MultiChannelExtractROI and not NeighborrhoodIterator
                //to avoid load in memory the entire image.

                // Extract the selected region in the L3 product
                ExtractROIMaskImagePointer l_SATExtract = ExtractROIMaskImageType::New();
                ExtractROIMaskImagePointer l_PXDExtract = ExtractROIMaskImageType::New();
                MultiChannelExtractROIPointer l_SREExtract = MultiChannelExtractROIType::New();

                l_SATExtract->SetInput(m_SATImage);
                l_SATExtract->SetExtractionRegion(l_ExtractedRegion);

                l_PXDExtract->SetInput(m_PXDImage);
                l_PXDExtract->SetExtractionRegion(l_ExtractedRegion);

                l_SREExtract->SetInput(m_SREImage);
                l_SREExtract->SetExtractionRegion(l_ExtractedRegion);

                // Cast from Image to VectorImage
                typename CasterType::Pointer l_PXDCaster = CasterType::New();
                l_PXDCaster->SetInput(l_PXDExtract->GetOutput());

                // Compute statistics
                StatisticsMaskFilterPointer l_SATStater = StatisticsMaskFilterType::New();
                StatisticsVectorMaskFilterPointer l_PXDStater = StatisticsVectorMaskFilterType::New();
                StatisticsImageFilterPointer l_SREStater = StatisticsImageFilterType::New();

                l_SATStater->SetInput(l_SATExtract->GetOutput());

                l_PXDStater->SetInput(l_PXDCaster->GetOutput());
                l_PXDStater->SetMaskInput(l_SATExtract->GetOutput());
                l_PXDStater->SetForegroundValue(0);
                l_PXDStater->SetEnableStandardDeviation(true);

                l_SREStater->SetInput(l_SREExtract->GetOutput());
                l_SREStater->SetMaskInput(l_SATExtract->GetOutput());
                l_SREStater->SetForegroundValue(0);
                l_SREStater->SetEnableStandardDeviation(true);
                l_SREStater->SetEnableSecondOrderStats(false);
                l_SREStater->SetEnableExcludeValue(true);
                l_SREStater->SetExcludeValue(m_NoData);
                vnsLogDebugMacro("m_NoData = "<< m_NoData)

                // Launch stat computation
                l_PXDStater->Update();
                l_SATStater->Update();
                l_SREStater->Update();

                //push back computed sre and fre Means in output vectors
                // if no valid pixel found => set 0
                m_SREStatIsValid = l_SREStater->GetIsValid();
                vnsLogDebugMacro("m_SREStatIsValid = "<< Utilities::BoolToLowerString(m_SREStatIsValid))
                vnsLogDebugMacro("l_SREStater->GetNbOfValidValues()           = "<< l_SREStater->GetNbOfValidValues())
                vnsLogDebugMacro("l_SREStater->GetNbOfValidValuesPerBand()    = "<< l_SREStater->GetNbOfValidValuesPerBand())
                vnsLogDebugMacro("l_SREStater->GetNbOfExcludedValuesPerBand() = "<< l_SREStater->GetNbOfExcludedValuesPerBand())
                if (l_SREStater->GetNbOfValidValues() == 0)
                {
                    InputReflectanceImagePixelType sreMeans;
                    sreMeans.SetSize(m_SREImage->GetNumberOfComponentsPerPixel());
                    sreMeans.Fill(ERROR_VALUE);
                    InputReflectanceImagePixelType res;
                    res.SetSize(m_SREImage->GetNumberOfComponentsPerPixel());
                    res.Fill(0.);
                    m_SREMeans = sreMeans;
                    m_SREStDv = res;
                    m_UsefulPixelsPercentage = res;
                    m_ValidPixelsPercentage = res;
                    m_PXDMeans = ERROR_VALUE;
                    m_PXDStDv = 0;
                }
                else
                {
                    m_SREMeans = l_SREStater->GetMean();
                    m_SREStDv = l_SREStater->GetStandardDeviation();
                    m_PXDMeans = l_PXDStater->GetMean()[0];
                    m_PXDStDv = l_PXDStater->GetStandardDeviation()[0];
                    //Percentage of useful/valid pixels
                    m_UsefulPixelsPercentage = (l_SREStater->GetNbOfValidValuesPerBand() * 100.0) / (sizeX * sizeY);
                    InputReflectanceImagePixelType l_nbExcludedValuePerBand(m_SREImage->GetNumberOfComponentsPerPixel());
                    l_nbExcludedValuePerBand.Fill(0);
                    l_nbExcludedValuePerBand = l_SREStater->GetNbOfExcludedValuesPerBand() - l_SREStater->GetNbOfExcludedValues();
                    for (unsigned int i = 0; i < m_SREImage->GetNumberOfComponentsPerPixel(); i++)
                    {
                        if (vnsEqualsDoubleMacro(m_UsefulPixelsPercentage[i], 0))
                        {
                            m_SREMeans[i] = ERROR_VALUE;
                            m_SREStDv[i] = 0;
                        }
                        if (l_ExtractedRegion.GetNumberOfPixels() > l_nbExcludedValuePerBand[i])
                        {
                            m_ValidPixelsPercentage[i] = l_SREStater->GetNbOfValidValuesPerBand()[i] * 100.0
                                    / (l_ExtractedRegion.GetNumberOfPixels() - l_nbExcludedValuePerBand[i]);
                        }
                    }
                }
                // Check if at least one pixel is saturated in the region
                if (l_SATStater->GetSum() > 0)
                {
                    m_SATVal = 1;
                }
                else
                {
                    m_SATVal = 0;
                }
                // If the FRE image is available
                if (m_UseFRE == true)
                {
                    m_FREImage->UpdateOutputInformation();
                    if (l_Region != m_FREImage->GetLargestPossibleRegion())
                    {
                        vnsExceptionBusinessMacro("Input images size mismatch");
                    }

                    InputReflectanceImagePixelType freMeans;
                    freMeans.SetSize(m_FREImage->GetNumberOfComponentsPerPixel());
                    freMeans.Fill(ERROR_VALUE);
                    InputReflectanceImagePixelType frePixVal;

                    // Extracted the selected region in the FRE image
                    MultiChannelExtractROIPointer l_FREExtract = MultiChannelExtractROIType::New();

                    l_FREExtract->SetInput(m_FREImage);
                    l_FREExtract->SetExtractionRegion(l_ExtractedRegion);

                    // Compute statistics in the FRE image
                    StatisticsImageFilterPointer l_FREStater = StatisticsImageFilterType::New();
                    l_FREStater->SetInput(l_FREExtract->GetOutput());
                    l_FREStater->SetMaskInput(l_SATExtract->GetOutput());
                    l_FREStater->SetForegroundValue(0);
                    l_FREStater->SetEnableStandardDeviation(true);
                    l_FREStater->SetEnableExcludeValue(true);
                    l_FREStater->SetExcludeValue(m_NoData);

                    l_FREStater->Update();
                    m_FREStatIsValid = l_FREStater->GetIsValid();


                    if (l_FREStater->GetNbOfValidValues() == 0)
                    {
                        InputReflectanceImagePixelType res;
                        res.SetSize(m_FREImage->GetNumberOfComponentsPerPixel());
                        res.Fill(0.);
                        m_FREMeans = freMeans;
                        m_FREStDv = res;
                    }
                    else
                    {
                        m_FREMeans = l_FREStater->GetMean();
                        InputReflectanceImagePixelType l_nbValidValuePerBand = l_FREStater->GetNbOfValidValuesPerBand();
                        for (unsigned int i = 0; i < m_FREImage->GetNumberOfComponentsPerPixel(); i++)
                        {
                            if (vnsEqualsDoubleMacro(l_nbValidValuePerBand[i], 0))
                            {
                                m_FREMeans[i] = ERROR_VALUE;
                            }
                            m_FREStDv = l_FREStater->GetStandardDeviation();
                        }
                    }
                }
                else
                {
                    InputReflectanceImagePixelType res;
                    res.SetSize(1);
                    res.Fill(0.);
                    m_FREMeans = res;
                    m_FREStDv = res;
                }
            }
        }

    /** PrintSelf method */
    template<class TInputReflectanceImage, class TInputMaskImage>
        void
        L3Extract<TInputReflectanceImage, TInputMaskImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL3Extract_txx */
