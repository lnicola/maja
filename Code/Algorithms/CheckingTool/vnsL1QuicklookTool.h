// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 07 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 30 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL1QuicklookTool_h
#define __vnsL1QuicklookTool_h

#include "vnsChangeValueVectorFunctor.h"

#include "itkProcessObject.h"
#include "otbMultiChannelExtractROI.h"
#include "vnsLinearSubSampleVectorImageFilter.h"
#include "otbVectorRescaleIntensityImageFilter.h"
#include "vnsWriteTxtOnImageFilter.h"
#include "otbImageFileReader.h"

namespace vns
{
    /** \class  L1QuicklookTool
     * \brief This class implements ROI extraction and quicklook generation for L1 data.
     *

     *
     *
     * \author CS Systemes d'Information
     *
     * \sa ImageToImageFilter
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TInputImage, class TOutputImage>
        class L1QuicklookTool : public itk::ProcessObject
        {
        public:
            /** Standard class typedefs. */
            typedef L1QuicklookTool Self;
            typedef itk::ProcessObject Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L1QuicklookTool, ProcessObject )


            /** Some convenient typedefs. */
            typedef TInputImage InputImageType;
            typedef typename InputImageType::ConstPointer InputImageConstPointer;
            typedef typename InputImageType::RegionType RegionType;
            typedef typename InputImageType::PixelType InputImagePixelType;
            typedef typename InputImageType::InternalPixelType InputImageInternalPixelType;
            typedef typename InputImageType::SizeType SizeType;
            typedef TOutputImage OutputImageType;
            typedef typename OutputImageType::Pointer OutputImagePointer;
            typedef typename OutputImageType::PixelType OutputImagePixelType;
            typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;

            typedef otb::MultiChannelExtractROI<InputImageInternalPixelType, InputImageInternalPixelType> MultiChannelExtractROIFilterType;
            typedef Functor::VectorChangeValue<InputImagePixelType, InputImagePixelType> VectorChangeFunctorType;
            typedef itk::UnaryFunctorImageFilter<InputImageType, InputImageType, VectorChangeFunctorType> ChangeValueVectorImageFilterType;
            typedef LinearSubSampleVectorImageFilter<InputImageType, InputImageType> QuicklookImageGeneratorType;
            typedef otb::VectorRescaleIntensityImageFilter<InputImageType, OutputImageType> VectorRescaleIntensityImageFilterType;
            typedef WriteTxtOnImageFilter<OutputImageType, OutputImageType> WriteTxtOnImageFilterType;

            typedef typename MultiChannelExtractROIFilterType::Pointer MultiChannelExtractROIFilterPointerType;
            typedef typename ChangeValueVectorImageFilterType::Pointer ChangeValueVectorImageFilterPointerType;
            typedef typename QuicklookImageGeneratorType::Pointer QuicklookImageGeneratorPointerType;
            typedef typename VectorRescaleIntensityImageFilterType::Pointer VectorRescaleIntensityImageFilterPointerType;
            typedef typename WriteTxtOnImageFilterType::Pointer WriteTxtOnImageFilterPointerType;

            typedef otb::ImageFileReader<OutputImageType> OutputImageFileReaderType;
            typedef typename OutputImageFileReaderType::Pointer OutputImageFileReaderPointerType;

            /** Definition of Spectral Camera use */
            typedef enum
            {
                EXTRACT = 0, QUICKLOOK, NB_METHODS
            } MethodType;

            /** Set the method */
            itkSetEnumMacro(Method,MethodType)

            vnsSetGetImageMacro(TOAInput, InputImageType, 0)

            itkSetMacro(ULIndexX,unsigned int)

            itkGetMacro(ULIndexX,unsigned int)


            itkSetMacro(ULIndexY,unsigned int)

            itkGetMacro(ULIndexY,unsigned int)


            itkSetMacro(NbLinesX,unsigned int)

            itkGetMacro(NbLinesX,unsigned int)


            itkSetMacro(NbLinesY,unsigned int)

            itkGetMacro(NbLinesY,unsigned int)


            itkSetMacro(Extract, bool)

            itkGetMacro(Extract, bool)


            itkSetMacro(Ratio, unsigned int)

            itkGetMacro(Ratio, unsigned int)


            itkSetMacro(QLRedBand, unsigned int)

            itkGetMacro(QLRedBand, unsigned int)


            itkSetMacro(QLGreenBand, unsigned int)

            itkGetMacro(QLGreenBand, unsigned int)


            itkSetMacro(QLBlueBand, unsigned int)

            itkGetMacro(QLBlueBand, unsigned int)


            itkSetMacro(MinReflBRed, InputImageInternalPixelType)

            itkGetMacro(MinReflBRed, InputImageInternalPixelType)


            itkSetMacro(MinReflBGreen, InputImageInternalPixelType)

            itkGetMacro(MinReflBGreen, InputImageInternalPixelType)


            itkSetMacro(MinReflBBlue, InputImageInternalPixelType)

            itkGetMacro(MinReflBBlue, InputImageInternalPixelType)


            itkSetMacro(MaxReflBRed, InputImageInternalPixelType)

            itkGetMacro(MaxReflBRed, InputImageInternalPixelType)


            itkSetMacro(MaxReflBGreen, InputImageInternalPixelType)

            itkGetMacro(MaxReflBGreen, InputImageInternalPixelType)


            itkSetMacro(MaxReflBBlue, InputImageInternalPixelType)

            itkGetMacro(MaxReflBBlue, InputImageInternalPixelType)


            itkSetMacro(NoData, InputImageInternalPixelType)

            itkGetMacro(NoData, InputImageInternalPixelType)


            itkSetMacro(NoDataReplaceValue, OutputImageInternalPixelType)

            itkGetMacro(NoDataReplaceValue, OutputImageInternalPixelType)


            itkSetMacro(OutputMin, OutputImageInternalPixelType)

            itkGetMacro(OutputMin, OutputImageInternalPixelType)


            itkSetMacro(OutputMax, OutputImageInternalPixelType)

            itkGetMacro(OutputMax, OutputImageInternalPixelType)


            /** Set the Font file name */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,FontFileName, std::string)

            /** Set the Text */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,Text, std::string)

            /** Font size for GD library */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,FontSize, unsigned int)

            /** Set the Text Color */
            void
            SetTextColor(int redVal, int greenVal, int blueVal)
            {
                m_WriteTxtOnImageFilter->SetTextColor(redVal, greenVal, blueVal);
            }

            OutputImagePointer
            GetOutput(void)
            {
                return m_OutputImage;
            }

            virtual void
            Generate(void);

        protected:
            /** Constructor */
            L1QuicklookTool();

            /** Destructor */
            virtual
            ~L1QuicklookTool();

            /** PrintSelf method */
            virtual void
            PrintSelf(std::ostream& os, itk::Indent indent) const;

            //virtual void GenerateOutputInformation(void);
            void
            CheckInputs(void);

        private:
            L1QuicklookTool(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented

            unsigned long m_ULIndexX;
            unsigned long m_ULIndexY;
            unsigned long m_NbLinesX;
            unsigned long m_NbLinesY;
            bool m_Extract;
            unsigned int m_Ratio;
            unsigned int m_QLRedBand;
            InputImageInternalPixelType m_MinReflBRed;
            InputImageInternalPixelType m_MaxReflBRed;
            unsigned int m_QLGreenBand;
            InputImageInternalPixelType m_MinReflBGreen;
            InputImageInternalPixelType m_MaxReflBGreen;
            unsigned int m_QLBlueBand;
            InputImageInternalPixelType m_MinReflBBlue;
            InputImageInternalPixelType m_MaxReflBBlue;

            InputImageInternalPixelType m_NoData;
            OutputImageInternalPixelType m_NoDataReplaceValue;

            OutputImageInternalPixelType m_OutputMin;
            OutputImageInternalPixelType m_OutputMax;

            MultiChannelExtractROIFilterPointerType m_MultiChannelExtractROIFilter;
            ChangeValueVectorImageFilterPointerType m_ChangeValueVectorImageFilter;
            QuicklookImageGeneratorPointerType m_QuicklookGenerator;
            VectorRescaleIntensityImageFilterPointerType m_VectorRescaleIntensityImageFilter;
            WriteTxtOnImageFilterPointerType m_WriteTxtOnImageFilter;
            OutputImageFileReaderPointerType m_VectorRescaleIntensityImageFilterReader;

            MethodType m_Method;

            OutputImagePointer m_OutputImage;
        };

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL1QuicklookTool.txx"
#endif

#endif /* __vnsL1QuicklookTool_h */
