// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153436-CS : 4 avril 2017 : Retrait de variables inutiles             *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 4-7-3 : FA : LAIG-FA-MAC-141974-CS : 15 fevrier 2016 : Controle sur les bornes Min et Max pour *
 *                                                                 l' adaptation de la dynamique de l'image *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 24 sept. 2010 : Creation                                            *
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsScalarToRainbowVectorImageFilter_txx
#define __vnsScalarToRainbowVectorImageFilter_txx

#include "vnsScalarToRainbowVectorImageFilter.h"

namespace vns
{

    namespace Functor
    {

        template<class TScalar, class TPixel>
            ScalarToRainbowVariableLengthVectorPixelFunctor<TScalar, TPixel>::ScalarToRainbowVariableLengthVectorPixelFunctor()
            {
                m_Minimum = 0;
                m_Maximum = itk::NumericTraits<ScalarType>::max();
                m_Hinc = 0;
            }

        template<class TScalar, class TPixel>
            TPixel
            ScalarToRainbowVariableLengthVectorPixelFunctor<TScalar, TPixel>::operator()(const TScalar & v) const
            {
                // Init the hinc
                // MACCS 4.7.2 : correction for possible division by zero
                // const double hinc = 0.6 / (m_Maximum - m_Minimum);
                const double hinc = m_Hinc;

                //  If v <= Min
                //  TODO : perfo : compute ans constant value for each pixel
                if (v <= m_Minimum)
                {
                    TPixel ans;
                    ans.SetSize(3);
                    typedef typename TPixel::ValueType ValueType;
                    ans.Fill(itk::NumericTraits<ValueType>::NonpositiveMin());

                    return ans;
                    //hue=0.6;
                }
                //  If v >= Max
                if (v >= m_Maximum)
                {
                    TPixel ans;
                    ans.SetSize(3);
                    typedef typename TPixel::ValueType ValueType;
                    ans.Fill(itk::NumericTraits<ValueType>::max());

                    return ans;
                    //hue =0.0;
                }
                //Computes hue value
                const double hue = 0.6 - (v - m_Minimum) * hinc;
                // Compute the sat value
                const double sat = 0.99;
                const double val = itk::NumericTraits<ValueType>::max();

                return m_HSVToVariableLengthVectorFunctor(hue, sat, val);
            }

    } // end namespace Functor

    template<class TInputImage, class TOutputImage>
        ScalarToRainbowVectorImageFilter<TInputImage, TOutputImage>::ScalarToRainbowVectorImageFilter()
        {
        }

    template<class TInputImage, class TOutputImage>
        void
        ScalarToRainbowVectorImageFilter<TInputImage, TOutputImage>::GenerateOutputInformation()
        {
            Superclass::GenerateOutputInformation();
            this->GetOutput()->SetNumberOfComponentsPerPixel(3);
        }

} // end namespace vns

#endif
