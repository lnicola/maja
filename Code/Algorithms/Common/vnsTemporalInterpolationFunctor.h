/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
// clang-format off
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#ifndef __vnsTemporalInterpolationFunctor_h
#define __vnsTemporalInterpolationFunctor_h

#include "vnsLoggers.h"
#include "itkVariableLengthVector.h"

namespace vns
{
namespace Functor
{

/** \class  TemporalInterpolationFunctor
 * \brief This class proceed to the temporal interpolation of two images.
 *
 * Takes two images as input.
 *
 * Call SetJulianDates() to provide the date of the first and second input image,
 * and the desired date for the interpolation.
 *
 * Input and output images are of type otb::VectorImage<double>.
 *
 * \author CS GROUP - France
 *
 * \ingroup L2
 *
 */
class TemporalInterpolationFunctor
{
public:
  TemporalInterpolationFunctor()
    : m_WeightBefore(0),
      m_WeightAfter(1)
  {
  }

  virtual ~TemporalInterpolationFunctor()
  {
  }

  /** Set the dates used for the interpolation
   *
   * The dates must be Julian Dates. The dates are used to compute
   * weights that will be applied to the input images.
   *
   * \param[in] interpolDate: date of the interpolation
   * \param[in] beforeDate: date of the image before interpolation date
   * \param[in] afterDate: date of the image after interpolation date
   */
  void SetJulianDates(double interpolDate, double beforeDate, double afterDate)
  {
    this->m_WeightAfter = (interpolDate - beforeDate) / (afterDate - beforeDate);
    this->m_WeightBefore = 1 - this->m_WeightAfter;

    vnsLogDebugMacro("Temporal interpolation parameters:\n"
                     "  - Julian date for interpolation: " << std::fixed << std::setprecision(20) << interpolDate << "\n"
                     "  - Julian date before: " << beforeDate << "\n"
                     "  - Julian date after: " << afterDate << "\n"
                     "  - Weight before: " << this->m_WeightBefore << "\n"
                     "  - Weight after: " << this->m_WeightAfter << "\n")
  }

  inline itk::VariableLengthVector<double> operator()(const itk::VariableLengthVector<double>& inBefore,
                                                      const itk::VariableLengthVector<double>& inAfter) const
  {
    return inAfter * m_WeightAfter + inBefore * m_WeightBefore;
  }

  size_t OutputSize(const std::array<size_t, 2> & nbBands) const
  {
    assert(nbBands[0] == nbBands[1]);
    return nbBands[0];
  }

private:
  /** Weights declaration */
  double m_WeightBefore;
  double m_WeightAfter;
};

}
}

#endif /* __vnsTemporalInterpolationFunctor_h */
