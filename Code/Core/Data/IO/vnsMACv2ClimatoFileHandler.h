/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation                                           *
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$                                                                                                     *
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsMACv2ClimatoFileHandler_h
#define __vnsMACv2ClimatoFileHandler_h

#include "itkObject.h"
#include "itkObjectFactory.h"
#include <string>
#include <vector>
#include <list>
#include <map>
#include "vnsUtilities.h"
#include "itkLinearInterpolateImageFunction.h"


namespace vns
{
/** \class  MACv2ClimatoFileHandler
 * \brief This class implements the basic functionnalities to get aot from MACv2 Climato file.
 *
 *
 * \author CS Systemes d'Information
 *
 *
 * \ingroup L2
 * \ingroup L3
 * \ingroup Checktool
 *
 */
class MACv2ClimatoFileHandler : public itk::Object
{
public:
  /** Typedef for Corners */
  typedef class Corner
  {
  public:
    double Longitude;
    double Latitude;
    int    Line;
    int    Column;
  } CornerType;
  /**Internal data types */
  typedef struct MACv2File
  {
    std::string              AOT_Filename;        // Filename of the AOT data
    double                   JulianDayAquisition; // JulianDay of the CAMS Data
  } MACv2FileType;

  /** Standard class typedefs. */
  typedef MACv2ClimatoFileHandler       Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  typedef double                         PixelType;
  typedef otb::VectorImage<PixelType, 2> ImageType;
  typedef otb::Image<PixelType, 2>       AOTImageType;
  typedef ImageType::PointType           PointType;
  typedef ImageType::SizeType            SizeType;
  typedef ImageType::RegionType          RegionType;
  typedef ImageType::PixelType           VectorPixel;

  /** Reader typedef. */
  typedef otb::ImageFileReader<ImageType> ReaderType;

  /** Interpolator typedef. */
  typedef itk::LinearInterpolateImageFunction<ImageType> InterpolatorType;
  typedef InterpolatorType::ContinuousIndexType          ContinuousIndexType;

  /** Creation through object factory macro */
  itkTypeMacro(MACv2ClimatoFileHandler, itk::Object)

  /** Type macro */
  itkNewMacro(Self)

  typedef Utilities::ListOfStrings               ListOfStrings;
  typedef Utilities::ListOfFilenames             ListOfFilenames;
  typedef Utilities::ListOfDoubles               ListOfDoubles;
  typedef Utilities::ListOfUIntegers             ListOfUIntegers;
  typedef std::map<std::string, VectorPixel>     AOTMapType;

  /** Set the MACv2 file before the date*/
  void SetBeforeClimato(const std::string& pBeforeCamsAOT,
                        const double pTimeJD)
  {
    this->m_beforeFile.AOT_Filename        = pBeforeCamsAOT;
    this->m_beforeFile.JulianDayAquisition = pTimeJD;
    this->m_beforeFileFound                = true;
  }

  /** Set the MACv2 file after the date*/
  void SetAfterClimato(const std::string& pAfterCamsAOT,
                      const double pTimeJD)
  {
    this->m_afterFile.AOT_Filename        = pAfterCamsAOT;
    this->m_afterFile.JulianDayAquisition = pTimeJD;
    this->m_afterFileFound                = true;
  }

  /** Return a pointer to the AOT image */
  AOTImageType* GetAOTImage()
  {
    return m_AOTImage.GetPointer();
  }

  /** Read the MACv2 climato Data and extract the aot information
   * */
  void ExtractAOTData(const CornerType& p_CenterCorner, const double pImageProductAquisitionTimeJD);

  /**
   * Return either the data is valid,
   * any access to data when the valid flag is false will result in an exception
   */
  bool isValid()
  {
    return m_IsValid;
  };

protected:
  /** Constructor */
  MACv2ClimatoFileHandler();
  /** Destructor */
  virtual ~MACv2ClimatoFileHandler();
  /** PrintSelf method */
  virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

  enum Mode
  {
    BEFORE,
    AFTER
  };

  // Internal subroutines

  /* Process either the MACv2 climato file before or after*/
  void TreatOneFile(MACv2FileType const& p_ClimatoFiles, const CornerType& p_CenterCorner, const Mode& p_mode);
  
  /* Get the value of the MACv2 climato file at CenterCorner */
  std::pair<std::string, VectorPixel> GetClimatoValueAndDescription(const std::string& filename, const CornerType& p_CenterCorner);
  
  /* Temporal interpolation between after and before data*/
  void TemporalInterpolation();
  
  /* is this dataset valid */
  bool        isValidDataset(const std::string& filename);
  
  // NETCDF Metadata utility functions
  MACv2ClimatoFileHandler::ListOfStrings GetMetadataValueAsListOfString(const ImageType* image, std::string const& name);
  std::string                    GetMetadataValueAsString(const ImageType* image, std::string const& name);
  std::vector<double>            GetMetadataValueAsDoubleVector(const ImageType* image, std::string const& name);

  // Compute aot 
  void ComputeAOT(const std::string& filename, const Mode& p_mode);

  // Cleanup function
  void cleanup();
  // Verify that input data has been filled
  void verifyData();

  // Constant on the MAXIMUM number of dataset possible in a netcdf
  static const unsigned int NETCDF_MAX_DATASET_NUMBER;

  // CAMS Filename
  MACv2FileType m_beforeFile;
  bool          m_beforeFileFound;
  MACv2FileType m_afterFile;
  bool          m_afterFileFound;

  // Validity flag
  bool   m_IsValid;
  double m_ImageTimeAquisitionJD;

  // Internal algo data
  AOTMapType        m_beforeAOTs;       // AOT for before file
  AOTMapType        m_afterAOTs;        // AOT for before file

  // Image of the AOT dataset
  AOTImageType::Pointer m_AOTBeforeImage;
  AOTImageType::Pointer m_AOTAfterImage;
  AOTImageType::Pointer m_AOTImage;


private:
  MACv2ClimatoFileHandler(const Self&); // purposely not implemented
  void operator=(const Self&);  // purposely not implemented
};

} // End namespace vns

#endif /* __vnsMACv2ClimatoFileHandler */