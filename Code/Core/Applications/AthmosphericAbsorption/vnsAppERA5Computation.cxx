/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#include "vnsLoggers.h"
#include "otbWrapperCompositeApplication.h"
#include "otbWrapperApplicationFactory.h"

namespace vns
{
namespace Wrapper
{

using namespace otb::Wrapper;

class ERA5Computation : public CompositeApplication
{
public:
  /** Standard class typedefs. */
  using Self = ERA5Computation;
  using Superclass = otb::Wrapper::Application;
  using Pointer = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Standard macro */
  itkNewMacro(Self)

  itkTypeMacro(ERA5Computation, otb::Wrapper::Application)

  /** Some convenient typedefs. */

private:
  void DoInit()
  {
    SetName("ERA5Computation");
    SetDescription("ERA5Computation algo.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("ERA5 specific humidities processing preparation");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    ClearApplications();
    AddApplication("ERA5TemporalInterpolation", "tempinterp", "Temporal interpolation step");
    AddApplication("SetOrigin", "setorigin", "Origin setting step");
    AddApplication("TileFusion", "tilefusion", "The TileFusion step");
    AddApplication("Superimpose", "superimpose", "Spatial interpolation step");
    AddApplication("ERA5WaterVaporComputation", "watervapor", "Water vapor computation step");

    ShareParameter("datejulian", "tempinterp.datejulian");
    ShareParameter("dateutc", "tempinterp.dateutc");

    AddParameter(ParameterType_Group, "before", "ERA5 data before the date");
    MandatoryOff("before");
    ShareParameter("before.datejulian", "tempinterp.before.datejulian");
    ShareParameter("before.dateutc", "tempinterp.before.dateutc");
    ShareParameter("before.era5file", "tempinterp.before.era5file");

    AddParameter(ParameterType_Group, "after", "ERA5 data after the date");
    MandatoryOff("after");
    ShareParameter("after.datejulian", "tempinterp.after.datejulian");
    ShareParameter("after.dateutc", "tempinterp.after.dateutc");
    ShareParameter("after.era5file", "tempinterp.after.era5file");

    ShareParameter("dtm", "watervapor.dtm");
    Connect("superimpose.inr", "watervapor.dtm");

    ShareParameter("wvmap", "watervapor.out");

    Connect("watervapor.pl", "tempinterp.pl");

    ShareParameter("ram", "superimpose.ram");
    Connect("watervapor.ram", "superimpose.ram");
    Connect("tempinterp.ram", "superimpose.ram");
  }

  void DoUpdateParameters()
  {
  }

  void DoExecute()
  {
    // Temporal interpolation
    ExecuteInternal("tempinterp");

    // Avoid data cut on greenwich meridian
    GetInternalApplication("setorigin")->SetParameterInputImage("in", GetInternalApplication("tempinterp")->GetParameterOutputImage("out"));
    GetInternalApplication("setorigin")->SetParameterFloat("originx", -360.0);
    GetInternalApplication("setorigin")->SetParameterFloat("originy", 90.0);
    ExecuteInternal("setorigin");
    GetInternalApplication("tilefusion")->AddImageToParameterInputImageList("il", GetInternalApplication("setorigin")->GetParameterOutputImage("out"));
    GetInternalApplication("tilefusion")->AddImageToParameterInputImageList("il", GetInternalApplication("tempinterp")->GetParameterOutputImage("out"));
    GetInternalApplication("tilefusion")->SetParameterInt("cols", 2);
    GetInternalApplication("tilefusion")->SetParameterInt("rows", 1);
    ExecuteInternal("tilefusion");

    // Set up the spatial interpolation with Superimpose
    GetInternalApplication("superimpose")->SetParameterInputImage("inm", GetInternalApplication("tilefusion")->GetParameterOutputImage("out"));
    GetInternalApplication("superimpose")->SetParameterString("interpolator", "linear");
    ExecuteInternal("superimpose");

    // Water vapor computation
    GetInternalApplication("watervapor")->SetParameterInputImage("in", GetInternalApplication("superimpose")->GetParameterOutputImage("out"));
    ExecuteInternal("watervapor");
  }

};

} // namespace Wrapper
} // namespace vns


OTB_APPLICATION_EXPORT(vns::Wrapper::ERA5Computation)
