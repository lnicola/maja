/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbGenericRSTransform.h"
#include "itkLinearInterpolateImageFunction.h"
#include "vnsLoggers.h"
#include "vnsUtilities.h"


namespace vns
{

namespace Wrapper
{

using namespace otb::Wrapper;

class AOTResampling : public Application
{
public:
  /** Standard class typedefs. */
  typedef AOTResampling             Self;
  typedef otb::Wrapper::Application     Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(AOTResampling, otb::Wrapper::Application);

  /** Input AOT image typedef */
  typedef DoubleImageType            AOTImageType;
  typedef AOTImageType::Pointer      AOTImagePointerType;
  typedef AOTImageType::PixelType    AOTPixelType;

  /** Input DTM image typedef */
  typedef DoubleImageType                 InputDTMType;
  typedef InputDTMType::ConstPointer      InputDTMConstPointerType;

  /** Output imge typedef */
  typedef DoubleImageType                    OutputImageType;

  /** Filters typedef */
  /** Interpolator typedef. */
  // Projection transform type
  typedef otb::GenericRSTransform<>                         GEOTransformType;
  typedef itk::LinearInterpolateImageFunction<AOTImageType> AOTInterpolatorType;
  typedef AOTInterpolatorType::ContinuousIndexType          AOTContinuousIndexType;


private:
  void DoInit()
  {
    SetName("AOTResampling");
    SetDescription("Generate the AOT from Cams or MacV2 climato resampled at coarse resolution");
    Loggers::GetInstance()->Initialize(GetName());

    // Documentation
    SetDocLongDescription("This application resample the cam AOT at coarse resolution");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    // Current input
    AddParameter(ParameterType_InputImage, "dtm", "dtm image");
    SetParameterDescription("dtm", "dtm of the image at coarse resolution");
    AddParameter(ParameterType_Group, "aot", "aot group");
    MandatoryOff("aot");
    AddParameter(ParameterType_InputImage, "aot.cams", "AOT from CAMS image");
    SetParameterDescription("aot.cams", "AOT from CAMS image");
    MandatoryOff("aot.cams");
    AddParameter(ParameterType_InputImage, "aot.climato", "AOT from MacV2 climato image");
    SetParameterDescription("aot.climato", "AOT from MacV2 climato image");
    MandatoryOff("aot.climato");
    
    // Output
    AddParameter(ParameterType_OutputImage, "aotresampled", "aotresampled");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);

  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    // Check the input conditions
    bool l_aotFromCams = this->HasValue("aot.cams");
    bool l_aotFromClimato = this->HasValue("aot.climato");

    if (l_aotFromCams && l_aotFromClimato)
    {
      vnsExceptionDataMacro("Only one aot can be resampled!!!");
    }

    // Get the dtm
    InputDTMConstPointerType lDTMSub = GetParameterDoubleImage("dtm");
    // CAMS Images
    AOTImagePointerType lAOTCamsPtr;
    AOTImagePointerType l_AOTCamsSub;

    // Load the image 
    if (l_aotFromCams)
    {
      lAOTCamsPtr = GetParameterDoubleImage("aot.cams");
    } else if (l_aotFromClimato)
    {
      lAOTCamsPtr = GetParameterDoubleImage("aot.climato");
    } else
    {
      vnsExceptionDataMacro("At least one aot should be provided !!!");
    }
    lAOTCamsPtr->UpdateOutputInformation();
    lAOTCamsPtr->SetRequestedRegionToLargestPossibleRegion();
    lAOTCamsPtr->Update();

    // L2Coarse area from DTM
    AreaType l_AreaToL2CoarseResolution;
    l_AreaToL2CoarseResolution.Origin  = lDTMSub->GetOrigin();
    l_AreaToL2CoarseResolution.Spacing = lDTMSub->GetSignedSpacing();
    l_AreaToL2CoarseResolution.Size    = lDTMSub->GetLargestPossibleRegion().GetSize();

    // Expand AOT from CAMS
    l_AOTCamsSub = AOTImageType::New();
    l_AOTCamsSub->SetRegions(lDTMSub->GetLargestPossibleRegion());
    l_AOTCamsSub->SetOrigin(lDTMSub->GetOrigin());
    l_AOTCamsSub->SetSignedSpacing(lDTMSub->GetSignedSpacing());
    l_AOTCamsSub->SetProjectionRef(lDTMSub->GetProjectionRef());
    l_AOTCamsSub->Allocate(true);

    // Instanciate Image->WGS transform
    GEOTransformType::Pointer img2wgs = GEOTransformType::New();
    img2wgs->SetOutputProjectionRef(lAOTCamsPtr->GetProjectionRef());
    // Set output transform info
    img2wgs->SetInputProjectionRef(lDTMSub->GetProjectionRef());
    img2wgs->SetInputOrigin(lDTMSub->GetOrigin());
    img2wgs->SetInputSpacing(lDTMSub->GetSignedSpacing());
    img2wgs->InstantiateTransform();
    GEOTransformType::InputPointType inPts;
    // Loop on the target AOT extent pixels
    for (unsigned int i = 0; i < l_AreaToL2CoarseResolution.Size[0]; i++)
    {
      for (unsigned int j = 0; j < l_AreaToL2CoarseResolution.Size[1]; j++)
      {
        // Compute Lat,Long coordinate for the point
        // ---------------------------------------------------------------------------------------------
        inPts[0] = i;
        inPts[1] = j;
        // Get the transformed point
        GEOTransformType::OutputPointType l_pointWGS = img2wgs->TransformPoint(inPts);
        // AOT interpolate at this lat/long
        AOTContinuousIndexType  l_contindex;
        AOTImageType::PointType l_point;
        l_point[0] = l_pointWGS[0];
        // Cyclic rotation on longitude
        // (only if the aot comes from cams, because the lon are from 0 to 360)
        if ( (l_pointWGS[0] < 0) && l_aotFromCams)
        {
          l_point[0] = 360 + l_pointWGS[0];
        }
        l_point[1] = l_pointWGS[1];
        // Retrieve the index in the original image
        lAOTCamsPtr->TransformPhysicalPointToContinuousIndex(l_point, l_contindex);
        // Interpolate the AOT at pixel coordinates
        AOTInterpolatorType::Pointer interp = AOTInterpolatorType::New();
        interp->SetInputImage(lAOTCamsPtr);
        AOTPixelType            l_outPix = static_cast<AOTPixelType>(interp->EvaluateAtContinuousIndex(l_contindex));
        AOTImageType::IndexType inIdx;
        inIdx[0] = i;
        inIdx[1] = j;
        // Set interpolated AOT value in the output image
        l_AOTCamsSub->SetPixel(inIdx, l_outPix);
      }
    }

    // Set outputs
    SetParameterOutputImagePixelType("aotresampled", ImagePixelType_float);
    SetParameterOutputImage<OutputImageType>("aotresampled", l_AOTCamsSub);
  }

private:
  /** Filters declaration */

};

} // namespace Wrapper
} // namespace vns

OTB_APPLICATION_EXPORT(vns::Wrapper::AOTResampling)
