/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 juil. 2010 : Creation
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "vnsL2Extract.h"
#include "vnsDataManagerTestingLibrary.h"
#include "itksys/SystemTools.hxx"
#include "otbGenericRSTransform.h"
#include <vector>
#include <string>


namespace vns
{
namespace Wrapper
{

using namespace otb::Wrapper;

class Extract : public Application
{
public:
  /** Standard class typedefs. */
  typedef Extract                       Self;
  typedef otb::Wrapper::Application     Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(Extract, otb::Wrapper::Application);

  /** Some convenient typedefs. */
  typedef VNSRealVectorImageType          ReflectanceImageType;
  typedef ReflectanceImageType::PixelType ReflectanceImagePixelType;
  typedef VNSRealImageType::PixelType     RealImagePixelType;
  typedef VNSMaskImageType::PixelType     MaskPixelType;

  /** L2 Extract Tool */
  typedef L2Extract<ReflectanceImageType, VNSRealImageType, VNSUnsignedIntegerImageType, VNSMaskImageType> L2ExtractType;
  typedef L2ExtractType::Pointer                                                                           L2ExtractPointer;

  // Projection transform type
  typedef otb::GenericRSTransform<> TransformType;


private:
  void DoInit()
  {
    SetName("Extract");
    SetDescription("Extract algo.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("Extract informations from L2 product. These informations are computed for a list of Pixels and are computed using a neighborhood");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    // Input Images
    AddParameter(ParameterType_InputImage, "sre", "sre image");
    SetParameterDescription("sre", "L2 SRE Image");

    AddParameter(ParameterType_InputImage, "refsre", "reference sre image");
    SetParameterDescription("refsre", "Reference L2 SRE Image");

    AddParameter(ParameterType_InputImage, "fre", "fre image");
    SetParameterDescription("fre", "L2 FRE image");
    MandatoryOff("fre");

    AddParameter(ParameterType_InputImage, "vap", "vap image");
    SetParameterDescription("vap", "L2 VAP image");

    AddParameter(ParameterType_InputImage, "aot", "aot image");
    SetParameterDescription("aot", "L2 AOT image");

    AddParameter(ParameterType_InputImage, "cld", "cld image");
    SetParameterDescription("cld", "L2 CLD image");

    AddParameter(ParameterType_InputImage, "wat", "wat image");
    SetParameterDescription("wat", "L2WAT image");

    AddParameter(ParameterType_InputImage, "edg", "edg image");
    SetParameterDescription("edg", "L2EDG image");

    AddParameter(ParameterType_StringList, "listbandcode", "List of band codes");
    AddParameter(ParameterType_StringList, "bandidsl2coarse", "list of IDS corresponding to band codes");
    AddParameter(ParameterType_Int, "nbbands", "total number of bands for current the product");
    AddParameter(ParameterType_Group, "currentpixel", "current pixel informations");
    AddParameter(ParameterType_Float, "currentpixel.x", "Current pixel X coordinate");
    AddParameter(ParameterType_Float, "currentpixel.y", "Current pixel Y coordinate");
    AddParameter(ParameterType_String, "currentpixel.freename", "Current pixel name");
    AddParameter(ParameterType_String, "currentpixel.unit", "Current pixel unit");
    AddParameter(ParameterType_Int, "radius", "radius of neighborhood for reflectance image");
    AddParameter(ParameterType_Int, "aotradius", "radius of neighborhood for AOT");
    AddParameter(ParameterType_Int, "vapradius", "radius of neighborhood for water vapor");
    AddParameter(ParameterType_Bool, "usefre", "use fre images");
    MandatoryOff("usefre");
    AddParameter(ParameterType_Float, "nodata", "no data value");
    AddParameter(ParameterType_Float, "vapnodata", "vap no data value");
    AddParameter(ParameterType_Float, "aotnodata", "aot no data value");

    AddParameter(ParameterType_Int, "cloudvalr1", "output cloud value for resolution R1");
    SetParameterRole("cloudvalr1", Role_Output);
    AddParameter(ParameterType_Int, "cloudvalr2", "output cloud value for resolution R2");
    SetParameterRole("cloudvalr2", Role_Output);
    AddParameter(ParameterType_Int, "cloudvalr3", "output cloud value for resolution R3");
    SetParameterRole("cloudvalr3", Role_Output);
    AddParameter(ParameterType_Int, "edgevalr1", "output edge value for resolution R1");
    SetParameterRole("edgevalr1", Role_Output);
    AddParameter(ParameterType_Int, "edgevalr2", "output edge value for resolution R2");
    SetParameterRole("edgevalr2", Role_Output);
    AddParameter(ParameterType_Int, "edgevalr3", "output edge value for resolution R3");
    SetParameterRole("edgevalr3", Role_Output);
    AddParameter(ParameterType_Int, "watervalr1", "output water value for resolution R1");
    SetParameterRole("watervalr1", Role_Output);
    AddParameter(ParameterType_Int, "watervalr2", "output water value for resolution R2");
    SetParameterRole("watervalr2", Role_Output);
    AddParameter(ParameterType_Int, "watervalr3", "output water value for resolution R3");
    SetParameterRole("watervalr3", Role_Output);

    AddParameter(ParameterType_StringList, "sremeans", "Mean SRE for each band");
    SetParameterRole("sremeans", Role_Output);
    AddParameter(ParameterType_StringList, "fremeans", "Mean FRE for each band");
    SetParameterRole("fremeans", Role_Output);
    AddParameter(ParameterType_StringList, "srestdv", "Standard deviation in SRE for each band");
    SetParameterRole("srestdv", Role_Output);
    AddParameter(ParameterType_StringList, "frestdv", "Standard deviation in FRE for each band");
    SetParameterRole("frestdv", Role_Output);
    AddParameter(ParameterType_StringList, "usefulpixpercent", "useful pixels percentage");
    SetParameterRole("usefulpixpercent", Role_Output);
    AddParameter(ParameterType_StringList, "validpixpercent", "valid pixels percentage");
    SetParameterRole("validpixpercent", Role_Output);
    AddParameter(ParameterType_Float, "aotmeans", "mean AOT");
    SetParameterRole("aotmeans", Role_Output);
    AddParameter(ParameterType_Float, "vapmeans", "mean VAP");
    SetParameterRole("vapmeans", Role_Output);
    AddParameter(ParameterType_Float, "aotstdv", "standard deviation for AOT");
    SetParameterRole("aotstdv", Role_Output);
    AddParameter(ParameterType_Float, "vapstdv", "standard deviation for VAP");
    SetParameterRole("vapstdv", Role_Output);
    AddParameter(ParameterType_Float, "aotusefulpixelspercentage", "aot useful pixels percentage");
    SetParameterRole("aotusefulpixelspercentage", Role_Output);
    AddParameter(ParameterType_Float, "aotvalidpixelspercentage", "aot valid pixels percentage");
    SetParameterRole("aotvalidpixelspercentage", Role_Output);
    AddParameter(ParameterType_Float, "vapusefulpixelspercentage", "vap useful pixels percentage");
    SetParameterRole("vapusefulpixelspercentage", Role_Output);
    AddParameter(ParameterType_Float, "vapvalidpixelspercentage", "vap valid pixels percentage");
    SetParameterRole("vapvalidpixelspercentage", Role_Output);
    AddParameter(ParameterType_Int, "srestatisvalid", "Boolean to represent if sre stats are valid");
    SetParameterRole("srestatisvalid", Role_Output);
    AddParameter(ParameterType_Int, "frestatisvalid", "Boolean to represent if fre stats are valid");
    SetParameterRole("frestatisvalid", Role_Output);
    AddParameter(ParameterType_Int, "aotstatisvalid", "Boolean to represent if aot stats are valid");
    SetParameterRole("aotstatisvalid", Role_Output);
    AddParameter(ParameterType_Int, "vapstatisvalid", "Boolean to represent if vap stats are valid");
    SetParameterRole("vapstatisvalid", Role_Output);
    AddParameter(ParameterType_Float, "pixpointx", "Boolean to represent if vap stats are valid");
    SetParameterRole("pixpointx", Role_Output);
    AddParameter(ParameterType_Float, "pixpointy", "Boolean to represent if vap stats are valid");
    SetParameterRole("pixpointy", Role_Output);
    AddParameter(ParameterType_Float, "pixwgsx", "Boolean to represent if vap stats are valid");
    SetParameterRole("pixwgsx", Role_Output);
    AddParameter(ParameterType_Float, "pixwgsy", "Boolean to represent if vap stats are valid");
    SetParameterRole("pixwgsy", Role_Output);
		AddParameter(ParameterType_Float, "ratio", "Ratio between reference and current spacing");
		SetParameterRole("ratio", Role_Output);
    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    // Initializations

    /** Filters declaration */
    L2ExtractPointer l2ExtractFilter = L2ExtractType::New();

    GetParameterDoubleVectorImage("refsre")->UpdateOutputInformation();
    const double l_SpacingRef = GetParameterDoubleVectorImage("refsre")->GetSpacing()[0];
    // Keep track of the transformed in wgs 84 point
    TransformType::OutputPointType l_pointWGS; // In wgs84
    l_pointWGS.Fill(0);
    TransformType::InputPointType l_pointPIX; // In pixel
    l_pointPIX.Fill(0);
    ExtractBase::IndexType l_CurIndex;
    const double           l_SpacingCurrent = GetParameterDoubleVectorImage("sre")->GetSpacing()[0];
    const double           l_Ratio          = l_SpacingRef / l_SpacingCurrent;
    vnsLogDebugMacro("Ratio computed for index of point: " << l_Ratio << " ( from spacing " << l_SpacingRef << "/" << l_SpacingCurrent);

    GetParameterDoubleVectorImage("sre")->UpdateOutputInformation();

    if (GetParameterString("currentpixel.unit") == "pixel")
    {
      // ---------------------------------------------------------------------------------------------
      l_pointPIX[0] = GetParameterFloat("currentpixel.x");
      l_pointPIX[1] = GetParameterFloat("currentpixel.y");
      l_CurIndex[0] = static_cast<int>(l_pointPIX[0] * l_Ratio);
      l_CurIndex[1] = static_cast<int>(l_pointPIX[1] * l_Ratio);
      // Build wgs ref
      OGRSpatialReference oSRS;
      oSRS.SetWellKnownGeogCS("WGS84");
      char* wgsRef = NULL;
      oSRS.exportToWkt(&wgsRef);

      // Instanciate Image->WGS transform
      TransformType::Pointer img2wgs = TransformType::New();
      img2wgs->SetOutputProjectionRef(wgsRef);
      // Set output transform info
      img2wgs->SetInputProjectionRef(GetParameterDoubleVectorImage("sre")->GetProjectionRef());
      img2wgs->SetInputOrigin(GetParameterDoubleVectorImage("sre")->GetOrigin());
      img2wgs->SetInputSpacing(GetParameterDoubleVectorImage("sre")->GetSpacing());
      img2wgs->InstantiateTransform();
      TransformType::InputPointType inPts;
      //-1 because input is in (1,1) convention
      inPts[0] = l_CurIndex[0] - 1;
      inPts[1] = - l_CurIndex[1] + 1;
      // Get the transformed point
      vnsLogDebugMacro("Using point " << inPts << " for inverse lat/lon transform");
      l_pointWGS = img2wgs->TransformPoint(inPts);
    }
    else if (GetParameterString("currentpixel.unit") == "degree")
    {
      // build point
      l_pointWGS[0] = GetParameterFloat("currentpixel.x");
      l_pointWGS[1] = GetParameterFloat("currentpixel.y");

      // Build wgs ref
      OGRSpatialReference oSRS;
      oSRS.SetWellKnownGeogCS("WGS84");
      char* wgsRef = NULL;
      oSRS.exportToWkt(&wgsRef);

      // Instanciate WGS->Image transform
      TransformType::Pointer wgs2img = TransformType::New();
      wgs2img->SetInputProjectionRef(wgsRef);
      // Set output transform info
      wgs2img->SetOutputProjectionRef(GetParameterDoubleVectorImage("sre")->GetProjectionRef());
      wgs2img->SetOutputOrigin(GetParameterDoubleVectorImage("sre")->GetOrigin());
      wgs2img->SetOutputSpacing(GetParameterDoubleVectorImage("sre")->GetSpacing());
      wgs2img->InstantiateTransform();
      TransformType::InputPointType inPts;
      inPts[0]                             = GetParameterFloat("currentpixel.x");
      inPts[1]                             = GetParameterFloat("currentpixel.y");
      TransformType::InputPointType outPts = wgs2img->TransformPoint(inPts);
      //+1 because transformed is in (0,0) convention
      l_CurIndex[0] = itk::Math::Round<int>(outPts[0] + 1.0 * l_Ratio);
      l_CurIndex[1] =  -itk::Math::Round<int>(outPts[1] - 1.0 * l_Ratio); // "-" because transform outputs upward y axis while we expect downward y axis to compute stats
      //+1 because transformed is in (0,0) convention
      l_pointPIX[0] = itk::Math::Round<int>((outPts[0] + l_Ratio) / l_Ratio);
      l_pointPIX[1] = itk::Math::Round<int>((-outPts[1] + l_Ratio) / l_Ratio);
      vnsLogDebugMacro("Point index after tranform : " << l_pointPIX);
    }
    else
    {
      // raise an exception
      vnsExceptionChainMacro("The pixel unit is not initialized !")
    }

    l2ExtractFilter->SetImageRegion(GetParameterDoubleVectorImage("sre")->GetLargestPossibleRegion());
    // Set all the masks associated to the input L2 product
    // L2 VAP image
    l2ExtractFilter->SetVAPImage(GetParameterDoubleImage("vap"));
    // L2 AOT image
    l2ExtractFilter->SetAOTImage(GetParameterDoubleImage("aot"));
    // L2 SRE image
    l2ExtractFilter->SetSREImage(GetParameterDoubleVectorImage("sre"));
    // L2 EDG image
    l2ExtractFilter->SetEDGImage(GetParameterUInt8Image("edg"));
    // L2 WAT image
    l2ExtractFilter->SetWATImage(GetParameterUInt8Image("wat"));
    // L2 CLD image
    l2ExtractFilter->SetCLDImage(GetParameterUInt16Image("cld"));
    // Set the surface reflectance image corrected from slope
    if (IsParameterEnabled("usefre"))
    {
      l2ExtractFilter->SetFREImage(GetParameterDoubleVectorImage("fre")); // L2 FRE image
      l2ExtractFilter->SetUseFRE(true);
    }
    else
    {
      vnsLogWarningMacro("No FRE image in L2 product. The FRE extract won't be done.") l2ExtractFilter->SetUseFRE(false);
    }

    // Set parameters
    l2ExtractFilter->SetPoint(l_CurIndex);                       // which contains Pixel y coordinates
    l2ExtractFilter->SetRadius(GetParameterInt("radius"));       // radius of neighborhood for reflectance image
    l2ExtractFilter->SetAOTRadius(GetParameterInt("aotradius")); // radius of neighborhood for reflectance image
    l2ExtractFilter->SetVapRadius(GetParameterInt("vapradius")); // radius of neighborhood for Vap image
    l2ExtractFilter->SetNoData(GetParameterFloat("nodata"));
    l2ExtractFilter->SetVAPNoData(GetParameterFloat("vapnodata"));
    l2ExtractFilter->SetAOTNoData(GetParameterFloat("aotnodata"));
    // method for computing outputs
    l2ExtractFilter->ComputeStats();

    SetParameterInt("cloudvalr1", int(l2ExtractFilter->GetCloudValR1()));
    SetParameterInt("cloudvalr2", int(l2ExtractFilter->GetCloudValR2()));
    SetParameterInt("cloudvalr3", int(l2ExtractFilter->GetCloudValR3()));
    SetParameterInt("edgevalr1", int(l2ExtractFilter->GetEdgeValR1()));
    SetParameterInt("edgevalr2", int(l2ExtractFilter->GetEdgeValR2()));
    SetParameterInt("edgevalr3", int(l2ExtractFilter->GetEdgeValR3()));
    SetParameterInt("watervalr1", int(l2ExtractFilter->GetWaterValR1()));
    SetParameterInt("watervalr2", int(l2ExtractFilter->GetWaterValR2()));
    SetParameterInt("watervalr3", int(l2ExtractFilter->GetWaterValR3()));

    SetParameterFloat("aotmeans", l2ExtractFilter->GetAOTMeans());
    SetParameterFloat("vapmeans", l2ExtractFilter->GetVAPMeans());
    SetParameterFloat("aotstdv", l2ExtractFilter->GetAOTStDv());
    SetParameterFloat("vapstdv", l2ExtractFilter->GetVAPStDv());

    SetParameterFloat("aotusefulpixelspercentage", l2ExtractFilter->GetAOTUseFulPixelsPercentage());
    SetParameterFloat("aotvalidpixelspercentage", l2ExtractFilter->GetAOTValidPixelsPercentage());
    SetParameterFloat("vapusefulpixelspercentage", l2ExtractFilter->GetVAPUseFulPixelsPercentage());
    SetParameterFloat("vapvalidpixelspercentage", l2ExtractFilter->GetVAPValidPixelsPercentage());

    SetParameterInt("srestatisvalid", int(l2ExtractFilter->GetSREStatIsValid()));
    SetParameterInt("frestatisvalid", int(l2ExtractFilter->GetFREStatIsValid()));
    SetParameterInt("aotstatisvalid", int(l2ExtractFilter->GetAOTStatIsValid()));
    SetParameterInt("vapstatisvalid", int(l2ExtractFilter->GetVAPStatIsValid()));

    // Compute the mean values of the surface reflectance of the extract per resolution
    const std::vector<std::string> l_ListOfBandCode = GetParameterStringList("listbandcode");
    const unsigned int             l_NbBandCodes    = l_ListOfBandCode.size();
    const unsigned int             l_NbL2Bands      = GetParameterInt("nbbands");
    std::vector<std::string>       l_SREMeansStr(l_NbL2Bands, "0.0");
    std::vector<std::string>       l_FREMeansStr(l_NbL2Bands, "0.0");
    std::vector<std::string>       l_SREStDvStr(l_NbL2Bands, "0.0");
    std::vector<std::string>       l_FREStDvStr(l_NbL2Bands, "0.0");
    std::vector<std::string>       l_UsefulpixpercentStr(l_NbL2Bands, "0.0");
    std::vector<std::string>       l_ValidpixpercentStr(l_NbL2Bands, "0.0");
    for (unsigned int b = 0; b < l_NbBandCodes; b++)
    {
      // for each band
      const std::string idStr   = GetParameterStringList("bandidsl2coarse").at(b);
      const int         id      = std::stoi(idStr);
      l_SREMeansStr[id]         = std::to_string(l2ExtractFilter->GetSREMeans()[b]);
      l_FREMeansStr[id]         = std::to_string(l2ExtractFilter->GetFREMeans()[b]);
      l_SREStDvStr[id]          = std::to_string(l2ExtractFilter->GetSREStDv()[b]);
      l_FREStDvStr[id]          = std::to_string(l2ExtractFilter->GetFREStDv()[b]);
      l_UsefulpixpercentStr[id] = std::to_string(l2ExtractFilter->GetUseFulPixelsPercentage()[b]);
      l_ValidpixpercentStr[id]  = std::to_string(l2ExtractFilter->GetValidPixelsPercentage()[b]);
    }

    SetParameterStringList("sremeans", l_SREMeansStr);
    SetParameterStringList("fremeans", l_FREMeansStr);
    SetParameterStringList("srestdv", l_SREStDvStr);
    SetParameterStringList("frestdv", l_FREStDvStr);
    SetParameterStringList("usefulpixpercent", l_UsefulpixpercentStr);
    SetParameterStringList("validpixpercent", l_ValidpixpercentStr);

    SetParameterFloat("pixpointx", l_pointPIX[0]);
    SetParameterFloat("pixpointy", l_pointPIX[1]);
    SetParameterFloat("pixwgsx", l_pointWGS[0]);
    SetParameterFloat("pixwgsy", l_pointWGS[1]);
    SetParameterFloat("ratio",l_Ratio);
  }
};

} // namespace Wrapper
} // namespace vns

OTB_APPLICATION_EXPORT(vns::Wrapper::Extract)
