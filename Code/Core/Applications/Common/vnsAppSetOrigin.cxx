/*
 * Copyright (C) 2023 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CSGROUP - France                                                                                 *
 *                                                                                                          *
 ************************************************************************************************************/
#include "vnsLoggers.h"
#include "vnsUtilities.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

namespace vns
{

namespace Wrapper
{

using namespace otb::Wrapper;

class SetOrigin : public Application
{
public:
  /** Standard class typedefs. */
  using Self = SetOrigin;
  using Superclass = Application;
  using Pointer = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Standard macro */
  itkNewMacro(Self)

  itkTypeMacro(SetOrigin, otb::Application)

  /** Some convenient typedefs. */


private:
  void DoInit()
  {
    SetName("SetOrigin");
    SetDescription("SetOrigin.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("Set the origin of the input images");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Writer");

    AddParameter(ParameterType_InputImage, "in", "Input image");
    SetParameterDescription("in", "The input image");

    AddParameter(ParameterType_OutputImage, "out", "Output image");
    SetParameterDescription("out", "The output image");

    AddParameter(ParameterType_Float, "originx", "Origin x");
    SetParameterDescription("originx", "X coordinate of the new origin");
    AddParameter(ParameterType_Float, "originy", "Origin y");
    SetParameterDescription("originy", "Y coordinate of the new origin");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    FloatVectorImageType::Pointer inImage = GetParameterImage("in");
    itk::MetaDataDictionary&  dict = inImage->GetMetaDataDictionary();

    FloatVectorImageType::PointType origin;
    origin[0] = GetParameterFloat("originx");
    origin[1] = GetParameterFloat("originy");
    inImage->SetOrigin(origin);

    SetParameterOutputImage("out", inImage);
  }

};
}
}

OTB_APPLICATION_EXPORT(vns::Wrapper::SetOrigin)
