LES ESSAIS DE VALIDATION
=====================================

Objectifs
----------

Les essais menés durant la phase d’intégration et de validation sont des essais dits fonctionnels et/ou numériques. Ils permettent de tester toutes les fonctionnalités du logiciel avec variation des paramètres d’entrée et des contextes d’utilisation. Ils couvrent les cas d’activation nominaux, les cas dégradés, les tests de performances. Ils permettent de répondre à la question « est-ce que le logiciel répond bien au besoin ? ».

L’objectif général de la validation est de garantir que le ou les composants implémentés fournissent des résultats exacts. Des tests de non-régression sont également mis en place afin de garantir l’exactitude des résultats au fur et à mesure des développements. Cette phase de validation permet notamment de : 

- Vérifier l’adéquation des fonctionnalités offertes par le système aux exigences,
- Garantir, en complément des différents essais réalisés au niveau des composants, une vérification des exigences compréhensible par l’utilisateur,
-  Valider les interfaces avec les entités externes,
-  Pérenniser un ensemble de scripts/jeux de données/résultats pour chaque essai afin de pouvoir les rejouer ultérieurement en cas d’évolution du système,
- Apprêter le système pour son entrée en recette.


Description
-----------

Le processus de validation est itératif. Il est initialisé pendant la phase de conception, précisé tout au long de la progression du développement, et complété pour la livraison sur site. Le processus est composé des étapes suivantes :

- Définition des tests : cette activité consiste à prévoir les tests qui devront démontrer que le système est conforme aux exigences utilisateur. Cette définition est réalisée en deux étapes :

 * La première consiste à produire un plan de validation. Elle s’attachera notamment à définir et identifier les essais unitaires et données constituant le plan de validation. Cette étape est réalisée durant les phases de conception et éventuellement complété au cours des développements, 
 * La deuxième consiste à produire les plans d’essais détaillés (PED) de chacun des essais. Ces PED contiennent toutes les informations décrites section 3.6.1.1,

- Vérification de la couverture des essais : cette activité est réalisée avec l’outil Reqtify, et une matrice de couverture « exigence/cas de tests » est produite.
- Les étapes d’exécution sont les suivantes :
- Génération de la version logicielle à valider par l’équipe projet à partir des sources gérés par l’équipe de gestion en configuration CS (la procédure de génération fait l’objet elle-même d’un test),
- Installation du logiciel (l’installation fait l’objet elle-même d’un test),
- Pour chaque essai de validation :
  
  * Installation des contextes d’essais, tels que définis dans le PED de l’essai,
  * Génération automatique du journal d’essai en début d’essai en fonction de sa description dans le PED,
  * Mise en œuvre de l’essai et rédaction de la partie résultats obtenus dans le journal d’essai,
  * Récupération des contextes de fin d’essai et exécution éventuelle de la procédure de vérification des résultats (comparaison du contexte récupéré par rapport à un contexte de référence).
  * Mise à jour du rapport de la campagne d’essais.

La phase de validation conduit à la mise en œuvre de l’ensemble des cas de tests identifiés. Ces cas de tests sont complétés et finalisés lors de cette phase. Les données et outils de validation sont mis en œuvre et adaptés le cas échéant. Les tests et données de validation sont maintenus pendant l’ensemble du cycle de vie du logiciel. Ce processus est tout d’abord mis en œuvre sur la machine de développement à CS sur laquelle est également réalisée la recette usine des chaînes MAJA L2, L3 ainsi que des outils associés. Suite à une phase d’intégration sur la configuration cible CNES, les tests associés à des produits simulés (choix du capteur) pourront être exécutés. La recette site sera réalisée sur la configuration cible CNES.

Formalisation des tests de validation 
--------------------------------------

La nomenclature adoptée pour l’identification des essais d’intégration et de validation est la
suivante :

S2-<traitement>-<num d’essai>-<nature>-<type>-<produit>-<thématique>_<MODE>

Ex : S2-L2INIT-001-F-N-VENUS-CLOUD

Avec :

Traitement identifie le type de traitement et peut prendre les valeurs :

- L2INIT : Chaîne L2 en mode Init,
- L2NOMINAL : Chaîne L2 en mode Nominal,
- L2BACKWARD : Chaîne L2 en mode Backward,

Numéro d’essai :

- 000 : valeur codée sur 3 caractères, valeur unique pour chaque <traitement> 

Nature identifie la nature du test et peut prendre les valeurs :

- F : test fonctionnel,
- N : test numérique,

Type identifie le type de l’essai et peut prendre les valeurs :

- N : nominal,
- D : dégradé,
- L : aux limites

Produit identifie le type de produit image utilisé et peut prendre les valeurs :

- VENUS : produit image Venus,
- FORMOSAT : produit image Formosat-2,
- LANDSAT : produit image Landsat,
- SENTINEL2 : produit image Sentinel2

Thématique est une abréviation du thème autour duquel s’organise le test, sur 10 caractères maxi-
mum.

Le mode peut prendre les valeurs :

- CHAIN : éxecution de la chaine
- COMP_ASCII : non regression des fichiers textes
- COMP_IMAGE : Comparaison des images

LISTE DES ESSAIS
------------------

Ce chapitre présente la liste des essais mis en place pour valider MAJA.

==== ==== =================================================================== ============================================================================================================
num  Val  Id                                                                  Commentaire
==== ==== =================================================================== ============================================================================================================
1     OK  pyTv-S2-L2NOMINAL-001-SENTINEL2-ALGO_CHAIN                          Validation fonctionnelle et numérique de la chaîne L2 avec un produit SENTINEL2 natif PDGS en mode NOMINAL.
2     OK  pyTv-S2-L2NOMINAL-001-SENTINEL2-ALGO_COMP_IMAGE                     Non regression Images du test CHAIN correspondant
3     OK  pyTv-S2-L2NOMINAL-001-SENTINEL2-ALGO_COMP_ASCII                     Non regression Texte du test CHAIN correspondant
4     OK  pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD_CHAIN                           Validation fonctionnelle et numérique de la chaîne L2 avec un produit SENTINEL2 natif PDGS en mode INIT (nouveau format ESA).
5     OK  pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD_COMP_IMAGE                      Non regression Images du test CHAIN correspondant
6     OK  pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD_COMP_ASCII                      Non regression Texte du test CHAIN correspondant
7     OK  pyTv-S2-L2INIT-011-SENTINEL2-MUSCATE-GENERAL_CHAIN                  Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode INIT.
8     OK  pyTv-S2-L2INIT-011-SENTINEL2-MUSCATE-GENERAL_COMP_IMAGE             Non regression Images du test CHAIN correspondant
9     OK  pyTv-S2-L2INIT-011-SENTINEL2-MUSCATE-GENERAL_COMP_ASCII             Non regression Texte du test CHAIN correspondant
10    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-CAMS_CHAIN                     Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode INIT avec CAMS.
11    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-CAMS_COMP_IMAGE                Non regression Images du test CHAIN correspondant
12    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-CAMS_COMP_ASCII                Non regression Texte du test CHAIN correspondant
13    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-NEWCAMS_CHAIN                  Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode INIT avec NewCAMS.
14    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-NEWCAMS_COMP_IMAGE             Non regression Images du test CHAIN correspondant
15    OK  pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-NEWCAMS_COMP_ASCII             Non regression Texte du test CHAIN correspondant
16    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-GENERAL_CHAIN           Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode NOMINAL.
17    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-GENERAL_COMP_IMAGE      Non regression Images du test CHAIN correspondant
18    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-GENERAL_COMP_ASCII      Non regression Texte du test CHAIN correspondant
19    OK  pyTv-S2-L2BACKWARD-011-F-N-SENTINEL2_MUSCATE-GENERAL_CHAIN          Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode BACKWARD.
20    OK  pyTv-S2-L2BACKWARD-011-F-N-SENTINEL2_MUSCATE-GENERAL_COMP_IMAGE     Non regression Images du test CHAIN correspondant
21    OK  pyTv-S2-L2BACKWARD-011-F-N-SENTINEL2_MUSCATE-GENERAL_COMP_ASCII     Non regression Texte du test CHAIN correspondant
==== ==== =================================================================== ============================================================================================================

==== ==== =================================================================== ============================================================================================================
num  Val  Id                                                                  Commentaire
==== ==== =================================================================== ============================================================================================================
22    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES_CHAIN            Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode NOMINAL ayant la particularité d'avoir des angles manquant pour un détecteur
23    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES_COMP_IMAGE       Non regression Images du test CHAIN correspondant
24    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES_COMP_ASCII       Non regression Texte du test CHAIN correspondant
25    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-LUTDIRCOR_CHAIN         Validation fonctionnelle et numérique avec un produit SENTINEL2 Muscate en mode NOMINAL en utilisant les LUTs pour la correction directionnelle.
26    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-LUTDIRCOR_COMP_IMAGE    Non regression Images du test CHAIN correspondant
27    OK  pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-LUTDIRCOR_COMP_ASCII    Non regression Texte du test CHAIN correspondant
28    OK  pyTv-S2-L2INIT-CROSSWRITING-SENTINEL2-SENTINEL2MUSCATE              Validation fonctionnelle et numérique avec un produit SENTINEL2 Natif vers Muscate en mode INIT .
29    OK  pyTv-S2-L2INIT-CROSSWRITING-LANDSAT8-LANDSAT8MUSCATE                Validation fonctionnelle et numérique avec un produit LANDSAT8 Natif vers Muscate en mode INIT .
30    OK  pyTv-S2-L2INIT-CROSSWRITING-VENUS-VENUSMUSCATE                      Validation fonctionnelle et numérique avec un produit VENUS Natif vers Muscate en mode INIT.
31    OK  pyTv-VE-L2INIT-VENUS-MUSCATE_CHAIN                                  Validation fonctionnelle et numérique avec un produit VENUS Muscate en mode INIT.
32    OK  pyTv-VE-L2INIT-VENUS-MUSCATE_COMP_IMAGE                             Non regression Images du test CHAIN correspondant
33    OK  pyTv-VE-L2INIT-VENUS-MUSCATE_COMP_ASCII                             Non regression Texte du test CHAIN correspondant
34    OK  pyTv-STARTMAJA_CHAIN                                                Validaton fonctionnelle et numérique de la chaîne StartMaja avec un produit Sentinel en mode INIT et NOMINAL
35    OK  pyTv-STARTMAJA_COMP_IMAGE                                           Non regression Images du test CHAIN correspondant
36    OK  pyTv-STARTMAJA_COMP_ASCII                                           Non regression Texte du test CHAIN correspondant
37    OK  pyTv-TapisS2_CHAIN                                                  Validaton fonctionnelle et numérique de la chaîne StartMaja avec un produit Sentinel en mode BACKWARD et avec une seule de résolution de sortie
38    OK  pyTv-TapisS2_COMP_IMAGE                                             Non regression Images du test CHAIN correspondant
39    OK  pyTv-TapisS2_COMP_ASCII                                             Non regression Texte du test CHAIN correspondant
40    OK  pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN								  Validaton fonctionnelle et numérique du support des produits VenusVM05 via StartMaja
41    OK  pyTv-STARTMAJA-VENUS-NATIF-VM05_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
42    OK  pyTv-STARTMAJA-VENUS-NATIF-VM05_COMP_ASCII						  Non regression Texte du test CHAIN correspondant
43    OK  pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN                      Validaton fonctionnelle et numérique de la chaîne StartMaja avec un produit SPOT3 HRV2 avec des données MAC2.
44    OK  pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_COMP_IMAGE                 Non regression Images du test CHAIN correspondant
45    OK  pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_COMP_ASCII                 Non regression Texte du test CHAIN correspondant
46    OK  pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_CHAIN                       Validaton fonctionnelle et numérique de la chaîne StartMaja avec un produit SPOT4 HRVIR 2 avec des données CAMS.
47    OK  pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_COMP_IMAGE                  Non regression Images du test CHAIN correspondant
48    OK  pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_COMP_ASCII                  Non regression Texte du test CHAIN correspondant
49    OK  pyTv-SPOT1-HRV1-L2INIT-CLIMATO_CHAIN								  Validation fonctionnelle et numérique avec un produit SPOT1 HRV1 Muscate en mode INIT.
50    OK  pyTv-SPOT1-HRV1-L2INIT-CLIMATO_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
51    OK  pyTv-SPOT1-HRV1-L2INIT-CLIMATO_COMP_ASCII			    			  Non regression Texte du test CHAIN correspondant
52    OK  pyTv-SPOT1-HRV2-L2INIT-CLIMATO_CHAIN								  Validation fonctionnelle et numérique avec un produit SPOT1 HRV2 Muscate en mode INIT.
53    OK  pyTv-SPOT1-HRV2-L2INIT-CLIMATO_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
54    OK  pyTv-SPOT1-HRV2-L2INIT-CLIMATO_COMP_ASCII			    			  Non regression Texte du test CHAIN correspondant
55    OK  pyTv-SPOT2-HRV1-L2INIT-CLIMATO_CHAIN								  Validation fonctionnelle et numérique avec un produit SPOT2 HRV1 Muscate en mode INIT.
56    OK  pyTv-SPOT2-HRV1-L2INIT-CLIMATO_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
57    OK  pyTv-SPOT2-HRV1-L2INIT-CLIMATO_COMP_ASCII			    			  Non regression Texte du test CHAIN correspondant
58    OK  pyTv-SPOT2-HRV2-L2INIT-CLIMATO_CHAIN								  Validation fonctionnelle et numérique avec un produit SPOT2 HRV2 Muscate en mode INIT.
59    OK  pyTv-SPOT2-HRV2-L2INIT-CLIMATO_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
60    OK  pyTv-SPOT2-HRV2-L2INIT-CLIMATO_COMP_ASCII			    			  Non regression Texte du test CHAIN correspondant
61    OK  pyTv-SPOT3-HRV1-L2INIT-CLIMATO_CHAIN								  Validation fonctionnelle et numérique avec un produit SPOT3 HRV1 Muscate en mode INIT.
62    OK  pyTv-SPOT3-HRV1-L2INIT-CLIMATO_COMP_IMAGE  						  Non regression Images du test CHAIN correspondant
63    OK  pyTv-SPOT3-HRV1-L2INIT-CLIMATO_COMP_ASCII			    			  Non regression Texte du test CHAIN correspondant
64    OK  pyTv-SPOT4-HRVIR1-L2INIT-CAMS_CHAIN                                 Validation fonctionnelle et numérique avec un produit SPOT4 HRVIR1 Muscate en mode INIT.
65    OK  pyTv-SPOT4-HRVIR1-L2INIT-CAMS_COMP_IMAGE                            Non regression Images du test CHAIN correspondant
66    OK  pyTv-SPOT4-HRVIR1-L2INIT-CAMS_COMP_ASCII                            Non regression Texte du test CHAIN correspondant
67    OK  pyTv-SPOT5-HRG1-L2INIT-CAMS_CHAIN                                   Validation fonctionnelle et numérique avec un produit SPOT5 HRG1 Muscate en mode INIT.
68    OK  pyTv-SPOT5-HRG1-L2INIT-CAMS_COMP_IMAGE                              Non regression Images du test CHAIN correspondant
69    OK  pyTv-SPOT5-HRG1-L2INIT-CAMS_COMP_ASCII                              Non regression Texte du test CHAIN correspondant
70    OK  pyTv-SPOT5-HRG2-L2INIT-CAMS_CHAIN                                   Validation fonctionnelle et numérique avec un produit SPOT5 HRG2 Muscate en mode INIT.
71    OK  pyTv-SPOT5-HRG2-L2INIT-CAMS_COMP_IMAGE                              Non regression Images du test CHAIN correspondant
72    OK  pyTv-SPOT5-HRG2-L2INIT-CAMS_COMP_ASCII                              Non regression Texte du test CHAIN correspondant
==== ==== =================================================================== ============================================================================================================



.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2NOMINAL-001-SENTINEL2-ALGO

	Valids/pyTv-S2-L2NOMINAL-001-SENTINEL2-ALGO

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD

	Valids/pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-011-SENTINEL2-MUSCATE-GENERAL

	Valids/pyTv-S2-L2INIT-011-SENTINEL2-MUSCATE-GENERAL


.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-CAMS

	Valids/pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-CAMS


.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-NEWCAMS 

	Valids/pyTv-S2-L2INIT-012-SENTINEL2-MUSCATE-NEWCAMS

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-GENERAL

	Valids/pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-GENERAL

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2BACKWARD-011-F-N-SENTINEL2_MUSCATE-GENERAL

	Valids/pyTv-S2-L2BACKWARD-011-F-N-SENTINEL2_MUSCATE-GENERAL


.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES

	Valids/pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-LUTDIRCOR

	Valids/pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-LUTDIRCOR

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-CROSSWRITING-SENTINEL2-SENTINEL2MUSCATE

	Valids/pyTv-S2-L2INIT-CROSSWRITING-SENTINEL2-SENTINEL2MUSCATE


.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-CROSSWRITING-LANDSAT8-LANDSAT8MUSCATE

	Valids/pyTv-S2-L2INIT-CROSSWRITING-LANDSAT8-LANDSAT8MUSCATE

.. toctree::
	:maxdepth: 5
	:caption: pyTv-S2-L2INIT-CROSSWRITING-VENUS-VENUSMUSCATE

	Valids/pyTv-S2-L2INIT-CROSSWRITING-VENUS-VENUSMUSCATE

.. toctree::
	:maxdepth: 5
	:caption: pyTv-VE-L2INIT-VENUS-MUSCATE

	Valids/pyTv-VE-L2INIT-VENUS-MUSCATE


.. toctree::
	:maxdepth: 5
	:caption: pyTv-STARTMAJA

	Valids/pyTv-STARTMAJA


.. toctree::
	:maxdepth: 5
	:caption: pyTv-TapisS2

	Valids/pyTv-TapisS2


.. toctree::
	:maxdepth: 5
	:caption: pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN

	Valids/pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN

	Valids/pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_CHAINN

	Valids/pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT1-HRV1-L2INIT-CLIMATO_CHAIN

	Valids/pyTv-SPOT1-HRV1-L2INIT-CLIMATO_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT1-HRV2-L2INIT-CLIMATO_CHAIN

	Valids/pyTv-SPOT1-HRV2-L2INIT-CLIMATO_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT2-HRV1-L2INIT-CAMS_CHAIN

	Valids/pyTv-SPOT2-HRV1-L2INIT-CAMS_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT2-HRV2-L2INIT-CAMS_CHAIN

	Valids/pyTv-SPOT2-HRV2-L2INIT-CAMS_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT3-HRV1-L2INIT-CLIMATO_CHAIN

	Valids/pyTv-SPOT3-HRV1-L2INIT-CLIMATO_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT4-HRVIR1-L2INIT-CAMS_CHAIN

	Valids/pyTv-SPOT4-HRVIR1-L2INIT-CAMS_CHAIN

.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT5-HRG1-L2INIT-CAMS_CHAIN

	Valids/pyTv-SPOT5-HRG1-L2INIT-CAMS_CHAIN


.. toctree::
	:maxdepth: 5
	:caption: pyTv-SPOT5-HRG2-L2INIT-CAMS_CHAIN

	Valids/pyTv-SPOT5-HRG2-L2INIT-CAMS_CHAIN




