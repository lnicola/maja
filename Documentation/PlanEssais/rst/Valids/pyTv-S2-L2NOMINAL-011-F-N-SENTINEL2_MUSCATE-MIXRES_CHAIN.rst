pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****

Validation fonctionnelle et numérique de la chaîne L2 (Pre-PostProcessing, entrée/sorties) en mode NOMINAL pour des séries temporelles générées avec des DEM à des résolutions différentes.
Les produits utilisés sont des SENTINEL2 Muscate.


Objectif
********

L’objectif de cet essai est de valider le fonctionnement global de la chaîne L2 en mode NOMINAL sur des produits SENTINEL2 Muscate générées avec des DEM à des résolutions différentes.
La particularité de ce produit est qu'il manque des angles pour un détecteur DEFTOO et que l'emprise de ce détecteur est entièrement recouverte par un autre détecteur qui lui ne présente pas d'angles manquants.


Description
***********

Les options de traitement sont :

- Méthode SPECTROTEMPORAL pour l’estimation des aérosols,
- Correction de l’environnement et des pentes,
- Ecriture à la résolution L2,
- activation de la correction des cirrus

La validation se fait sur un contexte avec un DEM à 120m et un produit L2 à 240m.


Liste des données d’entrées
***************************

Les images utilisées en entrée sont les suivantes :

- SENTINEL2B_20180409-110405-466_L2A_T29RPQ_C_V3-0/SENTINEL2B_20180409-110405-466_L2A_T29RPQ_C_V3-0_MTD_ALL.xml
- SENTINEL2B_20180412-112101-365_L1C_T29RPQ_C_V3-0/SENTINEL2B_20180412-112101-365_L1C_T29RPQ_C_V3-0_MTD_ALL.xml
- S2__TEST_AUX_REFDE2_T29RPQ_0001.DBL

Les fichiers GIPPs utilisés en entrée sont les suivants :

-S2A_TEST_GIP_CKEXTL_S_ALLSITES_00001_20190626_21000101.EEF
-S2A_TEST_GIP_CKQLTL_S_ALLSITES_00001_20190626_21000101.EEF
-S2A_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2A_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.HDR
-S2A_TEST_GIP_L2COMM_L_ALLSITES_00001_20190626_21000101.EEF
-S2A_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2A_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.HDR
-S2A_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2A_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.HDR
-S2A_TEST_GIP_L2SMAC_L_ALLSITES_00001_20190626_21000101.EEF
-S2A_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2A_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.HDR
-S2A_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2A_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.HDR
-S2B_TEST_GIP_CKEXTL_S_ALLSITES_00001_20190626_21000101.EEF
-S2B_TEST_GIP_CKQLTL_S_ALLSITES_00001_20190626_21000101.EEF
-S2B_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2B_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.HDR
-S2B_TEST_GIP_L2COMM_L_ALLSITES_00001_20190626_21000101.EEF
-S2B_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2B_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.HDR
-S2B_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2B_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.HDR
-S2B_TEST_GIP_L2SMAC_L_ALLSITES_00001_20190626_21000101.EEF
-S2B_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2B_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.HDR
-S2B_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.DBL.DIR/
-S2B_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.HDR  


Liste des produits de sortie
****************************

Produit SENTINEL2 L2A MUSCATE

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.
L'ajout des fonctionalités permettant de copier les angles du détecteur adjacent, lorsqu'un detecteur ne possède pas d'angles, ajoute 5 secondes au temps de traitement total.

Epsilon utilisé sur la non regression
*************************************
0.001

Vérifications à effectuer
**************************
Le test génère en sortie une image contenant les angles.

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-S2-L2NOMINAL-011-F-N-SENTINEL2_MUSCATE-MIXRES_CHAIN


Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 640 (C) ; MACCS-Exigence 650 (C) ;



Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
|today|              OK
================== =================

