pyTv-SPOT1-HRV1-L2INIT-CLIMATO_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****

Validation fonctionnelle et numérique de la chaîne L2 (Pre-PostProcessing, entrée/sorties) en mode INIT
sur un produit L1C SPOT1 HRV1 en utilisant des données de la climato MAC2.


Objectif
********

L’objectif de cet essai est de valider le fonctionnement global de la chaîne L2 avec un produit SPOT1 HRV1 au format Muscate.


Description
***********

Les options de traitement sont :

- Estimation des aérosols depuis la climato MAC2,
- Correction de l’environnement et des pentes,
- Ecriture à la résolution L2,

La validation se fait sur un contexte avec un DEM à 20m et un produit L2 à 20m.


Liste des données d’entrées
***************************

- SPOT1-HRV1_TEST_GIP_L2ALBD_L_CONTINEN_00001_19860222_20031101.DBL.DIR
- SPOT1-HRV1_TEST_GIP_L2ALBD_L_CONTINEN_00001_19860222_20031101.HDR
- SPOT1-HRV1_TEST_GIP_L2COMM_L_ALLSITES_00001_19860222_20031101.EEF
- SPOT1-HRV1_TEST_GIP_L2DIFT_L_CONTINEN_00001_19860222_20031101.DBL.DIR
- SPOT1-HRV1_TEST_GIP_L2DIFT_L_CONTINEN_00001_19860222_20031101.HDR
- SPOT1-HRV1_TEST_GIP_L2DIRT_L_CONTINEN_00001_19860222_20031101.DBL.DIR
- SPOT1-HRV1_TEST_GIP_L2DIRT_L_CONTINEN_00001_19860222_20031101.HDR
- SPOT1-HRV1_TEST_GIP_L2SITE_S_ALLSITES_00001_19860222_20031101.EEF
- SPOT1-HRV1_TEST_GIP_L2SMAC_L_ALLSITES_00001_19860222_20031101.EEF
- SPOT1-HRV1_TEST_GIP_L2TOCR_L_CONTINEN_00001_19860222_20031101.DBL.DIR
- SPOT1-HRV1_TEST_GIP_L2TOCR_L_CONTINEN_00001_19860222_20031101.HDR
- SPOT1-HRV1-XS_20000224-111306-014_L1C_040-264-0_C_V1-0
- SPOT1-HRV1-XS_20000224-111306_TEST_AUX_REFDE2_040-264-0_1001.DBL.DIR
- SPOT1-HRV1-XS_20000224-111306_TEST_AUX_REFDE2_040-264-0_1001.HDR
- SPOT_OPER_EXO_ERA5_20000224T110000.DBL.DIR
- SPOT_OPER_EXO_ERA5_20000224T110000.HDR
- SPOT_OPER_EXO_ERA5_20000224T120000.DBL.DIR
- SPOT_OPER_EXO_ERA5_20000224T120000.HDR
- SPOT_OPER_EXO_MAC2_19850101T000000_20250101T000000.DBL.DIR
- SPOT_OPER_EXO_MAC2_19850101T000000_20250101T000000.HDR



Liste des produits de sortie
****************************

Produit SPOT1-HRV1 L2A MUSCATE

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0.001

Vérifications à effectuer
**************************
Les tests COMP_ASCII et COMP_IMAGE associés permettent de valider la non regression

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-SPOT1-HRV1-L2INIT-CLIMATO_CHAIN


Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 640 (C) ; MACCS-Exigence 650 (C) ; MACCS-Exigence 50 (C) ; MACCS-Exigence 630 (C) ; [JIRA] MAJA-3340;


Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
|today|              OK
================== =================

