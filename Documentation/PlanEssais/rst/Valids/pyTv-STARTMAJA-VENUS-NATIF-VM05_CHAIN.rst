pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****

Validation fonctionnelle et numérique du support des produits Venus Natif VM05 (resolution 4m) avec Start Maja en mode Backward.


Objectif
********

L’objectif de cet essai est de valider le support des produits VenusVM05 dans Maja.
Plus particulièrement, ce test permet de vérifier la création du DTM à la bonne résolution puis l'exécution de l'orchestrateur maja pour cette version de produits Venus.

Issue Jira/Gitlab associée
**************************
[JIRA] MAJA-3314


Description
***********

Les options de traitement sont :

- Ecriture à la résolution L2,
- Activation de la correction environnementale
- Utilisation d'une valeur constante d'ozone (par défaut à 0.3)


Liste des données d’entrées
***************************

Les images utilisées en entrée sont les suivantes :

- VE_VM05_VSC_L1VALD_NIAKHAR__20220310.DBL.DIR
- VE_VM05_VSC_L1VALD_NIAKHAR__20220310.HDR
- VE_VM05_VSC_L1VALD_NIAKHAR__20220402.DBL.DIR
- VE_VM05_VSC_L1VALD_NIAKHAR__20220402.HDR


Les fichiers GIPPs utilisés en entrée sont les fichiers officiels Venus Natif (sans CAMS).


Liste des produits de sortie
****************************

Produit VENUS L2A MUSCATE

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0.0001

Vérifications à effectuer
**************************
Le test génère en sortie un produit conforme L2 à 4m.

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-STARTMAJA-VENUS-NATIF-VM05


Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 640 (C) ; MACCS-Exigence 650 (C) ;


Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
25/12/2022              OK
================== =================
