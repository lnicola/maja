pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****

Validation fonctionnelle et numérique de StartMaja avec un produit SPOT3 HRV2 en modes INIT.


Objectif
********

L’objectif de cet essai est de valider le fonctionnement global de StartMaja en mode INIT avec un produit SPOT3 HRV2.
Plus particulièrement, ce test permet de vérifier que la chaîn est bien capable de télécharger les GIPPS et autres données nécessaires,
et de bien faire le lien avec l'orchestrateur MAJA.


Description
***********

Les options de traitement sont :

- Estimation des aérosols depuis la climato MAC2,
- Correction de l’environnement et des pentes,
- Ecriture à la résolution L2,


Liste des données d’entrées
***************************

Les images utilisées en entrée sont les suivantes :

- SPOT3-HRV2-XS_19961022-143202-509_L1C_668-319-0_C_V1-0
- SPOT3-HRV2-XS_19961022-143202_TEST_AUX_REFDE2_668-319-0_1001.DBL.DIR
- SPOT3-HRV2-XS_19961022-143202_TEST_AUX_REFDE2_668-319-0_1001.HDR

Les fichiers GIPPs utilisés en entrée sont les suivants :

- SPOT_OPER_EXO_ERA5_19961022T140000.DBL.DIR
- SPOT_OPER_EXO_ERA5_19961022T140000.HDR
- SPOT_OPER_EXO_ERA5_19961022T150000.DBL.DIR
- SPOT_OPER_EXO_ERA5_19961022T150000.HDR



Liste des produits de sortie
****************************

Produit SPO3 HRV2 L2A MUSCATE

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0.0001

Vérifications à effectuer
**************************
Les tests COMP_ASCII et COMP_IMAGE associés permettent de valider la non regression

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN


Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 640 (C) ; MACCS-Exigence 650 (C) ; [JIRA] MAJA-3340;



Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
|today|              OK
================== =================

