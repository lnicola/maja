# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.common.interfaces.maja_xml_input -- shortdesc

orchestrator.common.interfaces.maja_xml_input is a description

It defines classes_and_methods

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
import orchestrator.common.date_utils as date_utils
from orchestrator.common.maja_exceptions import MajaIOException
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.common.earth_explorer.gipp_era5_earth_explorer_xml_file_handler import (
    GippERA5EarthExplorerXMLFileHandler,
)
from orchestrator.common.interfaces.maja_xml_app_cams import *

import os
import math

LOGGER = configure_logger(__name__)


class ERA5FileHandler(object):
    """
    A class to manage ERA5 files and interpolate watervapor data.

    ...

    Attributes
    ----------
        _working_dir : str
            current working directory
        _list_of_era5_description_file : list
            list of ERA5 climato (*.HDR)
        out_wvmap : image
            watervapor map

    Methods
    -------
        initializelistofera5filedescription:
            Allow set the list of ERA5 file (*.HDR)
        searchvalideera5filenames:

        has_ERA5_data:
            Allow to verify that ERA5 files are available for the current date
        extract_ERA5_aot:
            Extract the ERA5 files for specific date
    """
    def __init__(self, time_window, working_dir):
        """
        Constructs all the necessary attributes for the ERA5FileHandler object.

        Parameters
        ----------
            time_window : str
                Number of hours defining the temporal window for ERA5 files
            working_dir : str
                Path to working directory
        """
        self._time_window = time_window
        self._working_dir = working_dir
        self._list_of_era5_description_file = []
        # internal
        self._beforefilefound = False
        self._afterfilefound = False
        self._beforeFile = None
        self._afterFile = None
        self.out_wvmap = None
        self.valid = False
        self.proportions = {}
        self.__era5app = None

    def initializelistofera5filedescription(self, list_of_era5_files):
        """
        Initialize the ERA5FileHandler with ERA5 files

        Parameters
        ----------
            list_of_era5_files : list
                list of candidate era5 description file (HDR file)
        """
        # ---------------------------------------------------------------------------------------------
        self._list_of_era5_description_file = []
        # Loops on the era5 list
        for era5 in list_of_era5_files:
            # Normaly, the prodcut is uncompress and XML file checked in the PreProcessing method
            l_hdrera5filename = os.path.splitext(era5)[0] + ".HDR"
            l_era5filedescription = {}

            # Load the EarthExplorer XML file
            l_handler = GippERA5EarthExplorerXMLFileHandler(l_hdrera5filename)

            # Converts these dates in Julian day
            l_era5filedescription["date_utc"] = l_handler.acquisition_date_time
            l_era5filedescription["date_jd"] = date_utils.get_julianday_as_double(
                date_utils.get_datetime_from_utc(l_handler.acquisition_date_time)
            )
            l_era5filedescription["filename"] = l_hdrera5filename
            # Read the files to get the various era5 part
            l_list_of_date_filenames = l_handler.get_list_of_packaged_dbl_files(
                True, True
            )
            # For each file, search the type
            for f in l_list_of_date_filenames:
                l_filenamename = os.path.basename(f)
                # SH File ?
                if "ERA5_SH" in l_filenamename:
                    l_era5filedescription["era5_file"] = f

            if (
                "era5_file" not in l_era5filedescription
            ):
                LOGGER.info(l_era5filedescription)
                raise MajaIOException("Missing one ERA5 file")

            LOGGER.debug(
                "Add ERA5 file for date ["
                + l_era5filedescription["date_utc"]
                + ";"
                + str(l_era5filedescription["date_jd"])
                + "] -> file "
                + l_era5filedescription["filename"]
            )
            self._list_of_era5_description_file.append(l_era5filedescription)

    def searchvalidera5filenames(self, p_image_time_jd):
        """
        Search era5 files candidate for the date of the product to process

        Parameters
        ----------
            p_image_time_jd : float
                julian date of the product
        """
        self._afterfilefound = False
        self._beforefilefound = False
        # Log the product date
        LOGGER.debug("Searching ERA5 for product julian date : " + str(p_image_time_jd))

        # ---------------------------------------------------------------------------------------------
        # Loops on the era5 files
        for era5 in self._list_of_era5_description_file:
            l_cur_era5_date_jd = era5["date_jd"]
            LOGGER.debug("ERA5 date : " + str(l_cur_era5_date_jd))
            # Is the era5 less than a day before or after
            if math.fabs(p_image_time_jd - l_cur_era5_date_jd) <= (
                self._time_window / 24.0
            ):
                # era5 is before product
                if p_image_time_jd > l_cur_era5_date_jd:
                    if not self._beforefilefound:
                        self._beforeFile = era5
                        self._beforefilefound = True
                        # Log on current
                        LOGGER.debug(
                            "Before era5: "
                            + era5["filename"]
                            + " with date "
                            + era5["date_utc"]
                        )
                    else:
                        # Incoming file is after the stored
                        if self._beforeFile["date_jd"] <= l_cur_era5_date_jd:
                            self._beforeFile = era5
                            LOGGER.debug(
                                "Before era5: "
                                + era5["filename"]
                                + " with date "
                                + era5["date_utc"]
                            )
                else:
                    # era5 is after product
                    if not self._afterfilefound:
                        self._afterFile = era5
                        self._afterfilefound = True
                        LOGGER.debug(
                            "After era5: "
                            + era5["filename"]
                            + " with date "
                            + era5["date_utc"]
                        )
                    else:
                        # Incoming file is before the stored
                        if self._afterFile["date_jd"] > l_cur_era5_date_jd:
                            self._afterFile = era5
                            LOGGER.debug(
                                "After era5: "
                                + era5["filename"]
                                + " with date "
                                + era5["date_utc"]
                            )

        # Do not mix new and old era5, if one has
        if self._afterfilefound and self._beforefilefound:
            if (
                "NbNonInterpolate" in self._afterFile.keys()
                and not "NbNonInterpolate" in self._beforeFile.keys()
            ):
                self._beforefilefound = False
                self._beforeFile = None
            if (
                not "NbNonInterpolate" in self._afterFile.keys()
                and "NbNonInterpolate" in self._beforeFile.keys()
            ):
                self._afterfilefound = False
                self._afterFile = None

        return self._afterfilefound or self._beforefilefound

    def has_ERA5_data(self, p_imageproductaquisitiontimejd):
        """
        Search era5 files candidate for the date of the product to process

        Parameters
        ----------
            p_imageproductaquisitiontimejd : float
                julian date of the product
        """
        # Get the acquisition date in julian day
        self.searchvalidera5filenames(p_imageproductaquisitiontimejd)
        if not self._afterfilefound and not self._beforefilefound:
            LOGGER.info(
                "No ERA5 file found for JD date " + str(p_imageproductaquisitiontimejd)
            )
            return False
        else:
            return True

    def extract_ERA5_datas(
        self, p_imageproductaquisitiontimejd, dtm
    ):
        """
        Extract the watervapor map from ERA5 files in D.E.M area

        Parameters
        ----------
            p_imageproductaquisitiontimejd : float
                julian time of the data to extract
            dtm : str
                path of dtm file
        """
        self.valid = False
        if not self.searchvalidera5filenames(p_imageproductaquisitiontimejd):
            LOGGER.info(
                "No ERA5 found for JD Date " + str(p_imageproductaquisitiontimejd)
            )
            return

        era5_app_param = {
            "datejulian": p_imageproductaquisitiontimejd,
            "dtm": dtm,
            "wvmap": os.path.join(self._working_dir, "wvmap.tiff"),
        }

        if self._beforefilefound:
            era5_app_param["before.dateutc"] = self._beforeFile["date_utc"]
            era5_app_param["before.era5file"] = self._beforeFile["era5_file"]

        if self._afterfilefound:
            era5_app_param["after.dateutc"] = self._afterFile["date_utc"]
            era5_app_param["after.era5file"] = self._afterFile["era5_file"]

        self.__era5app = OtbAppHandler(
            "ERA5Computation", era5_app_param, write_output=True
        )

        self.out_wvmap = self.__era5app.getoutput().get("wvmap")
        self.valid = True
