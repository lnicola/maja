# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler -- shortdesc

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler is a description

It defines classes_and_methods

###################################################################################################
"""

from orchestrator.common.maja_exceptions import MajaDataException
from orchestrator.common.xml_tools import get_root_xml, get_only_value, as_bool
import orchestrator.common.xml_tools as xml_tools


GIPP_CKQLTL_HANDLER_XPATH = {
    "ComputeQL": "/Earth_Explorer_File/Data_Block/Compute_QL/text()",
    "FontSize": "/Earth_Explorer_File/Data_Block/Common_QL_Parameters/Font_Size/text()",
}


class GippCKQLTLEarthExplorerXMLFileHandler(object):
    def __init__(self, gipp_filename):
        self.gipp_filename = gipp_filename
        self.root = get_root_xml(self.gipp_filename, deannotate=True)
        self.ckqltl_values = {}

        for key, value in list(GIPP_CKQLTL_HANDLER_XPATH.items()):
            result = get_only_value(self.root, value)
            if result is not None:
                self.ckqltl_values[key] = result

    def get_value(self, key, check=False):
        if key in self.ckqltl_values:
            return self.ckqltl_values[key]
        else:
            if check:
                return None
            else:
                raise MajaDataException("No " + key + " in CKQLTL dictionnary")

    def get_value_b(self, key, check=False):
        return as_bool(self.get_value(key, check))

    def get_value_i(self, key, check=False):
        return int(self.get_value(key, check))

    def get_l2_count(self):
        return int(
            xml_tools.get_attribute(
                self.root, "/Earth_Explorer_File/Data_Block/L2_List_Of_QL", "count"
            )
        )

    def get_l2_ql_color_band(self, index, color):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/QL_{}_Band"
        return xml_tools.get_xml_string_value(
            self.root, row_path.format(band_selector, color)
        )

    def get_l2_water_vapor_ratio(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Water_Vapor_Ratio"
        )
        return int(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_undersampling_ratio(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Undersampling_Ratio"
        )
        return int(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_aot_ratio(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/AOT_Ratio"
        return int(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_min_ref_color_band(self, index, color):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Min_Ref_{}_Band"
        )
        return float(
            xml_tools.get_xml_string_value(
                self.root, row_path.format(band_selector, color)
            )
        )

    def get_l2_max_ref_color_band(self, index, color):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Max_Ref_{}_Band"
        )
        return float(
            xml_tools.get_xml_string_value(
                self.root, row_path.format(band_selector, color)
            )
        )

    def get_l2_min_water_vapor(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Min_Water_Vapor"
        )
        return float(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_max_water_vapor(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = (
            "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Max_Water_Vapor"
        )
        return float(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_min_aot(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Min_AOT"
        return float(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )

    def get_l2_max_aot(self, index):
        if index is not None:
            band_selector = "[@sn='{}']".format(index)
        else:
            band_selector = ""
        row_path = "/Earth_Explorer_File/Data_Block/L2_List_Of_QL/L2_QL{}/Max_AOT"
        return float(
            xml_tools.get_xml_string_value(self.root, row_path.format(band_selector))
        )


if __name__ == "__main__":
    pass
