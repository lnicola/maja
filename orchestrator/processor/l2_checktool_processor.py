# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor

###################################################################################################
"""

import os

import orchestrator.common.constants as constants
import orchestrator.common.date_utils as date_utils
import orchestrator.common.file_utils as file_utils
import orchestrator.common.gipp_utils as gipp_utils
import orchestrator.common.xml_tools as xml_tools
import orchestrator.plugins.common.factory.product_utils as product_utils
from orchestrator.common.earth_explorer.gipp_ckextl_earth_explorer_xml_file_handler import (
    GippCKEXTLEarthExplorerXMLFileHandler,
)
from orchestrator.common.earth_explorer.gipp_ckqltl_earth_explorer_xml_file_handler import (
    GippCKQLTLEarthExplorerXMLFileHandler,
)
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common.maja_exceptions import MajaChainException
from orchestrator.common.xml_tools import translate_xsl
from orchestrator.plugins.common.factory.maja_l2_image_writer_provider import (
    L2ImageWriterProvider,
)
from orchestrator.plugins.common.factory.maja_plugin_provider import MAJAPluginProvider
from orchestrator.processor.base_processor import BaseProcessor
from orchestrator.plugins.common.factory.maja_l2_image_reader_provider import (
    L2ImageReaderProvider,
)
from orchestrator.plugins.common.base.bands_definition import BandsDefinitions
from orchestrator.modules.maja_module import MajaModule
from orchestrator.common.earth_explorer.earth_explorer_utilities import (
    EarthExplorerUtilities,
)
from orchestrator.common.earth_explorer.header_image_earth_explorer_xml_file_handler import (
    HeaderImageEarthExplorerXMLFileHandler,
)
from orchestrator.common.earth_explorer.earth_explorer_xml_file_handler import (
    EarthExplorerXMLFileHandler,
)
from orchestrator.plugins.common.base.maja_qck_filenames_provider import (
    QCKFilenamesProvider,
)

import orchestrator.processor.l2_processor_image_writer_setup as l2_processor_image_writer_setup

import datetime

LOGGER = configure_logger(__name__)


class L2ChecktoolProcessor(BaseProcessor):
    def __init__(self, l1product, plugin, apphandler, DataDEMMap):
        self._l1product = l1product
        self.plugin = plugin
        self.Satellite = None
        super(L2ChecktoolProcessor, self).__init__(apphandler)
        self._run_extract_tool = False
        self._run_quicklook_tool = False
        self.DataDEMMap = DataDEMMap
        self.GIP_CKEXTL_Filename = ""
        self.GIP_CKQLTL_Filename = ""
        self.CKEXTLCOMMHandler = None
        self.CKQLTLCOMMHandler = None
        self.qck_filename_provider = QCKFilenamesProvider()
        self.schemaLocation = (
            apphandler.get_schemas_root_install_dir() + plugin.PluginName
        )

    def pre_processing(self):
        LOGGER.debug("Starting L2ChecktoolProcessor pre_processing...")

        # Register the GIPP file "GIP_CKEXTL"
        self.GIP_CKEXTL_Filename = gipp_utils.get_gipp_filename_with_mission(
            self._apphandler.get_input_directory(), "GIP_CKEXTL", self.Satellite
        )
        # Apply the stylesheet
        if self._apphandler._stylesheet is not None:
            self.GIP_CKEXTL_Filename = file_utils.copy_file_to_directory(
                self.GIP_CKEXTL_Filename,
                self._apphandler.get_working_directory(),
                notestmode=True,
            )
            translate_xsl(self.GIP_CKEXTL_Filename, self._apphandler._stylesheet)

        LOGGER.info(
            "The GIP_CKEXTL file detected for the satellite '"
            + self.Satellite
            + "' is <"
            + self.GIP_CKEXTL_Filename
            + ">."
        )

        if (
            self._apphandler.get_user_conf()
            .get_Processing()
            .get_CheckXMLFilesWithSchema()
        ):
            xml_tools.check_xml(
                self.GIP_CKEXTL_Filename,
                os.path.join(
                    self._apphandler.get_schemas_root_install_dir(),
                    self.plugin.MAJA_INSTALL_SCHEMAS_DIR,
                ),
            )

        self.CKEXTLCOMMHandler = GippCKEXTLEarthExplorerXMLFileHandler(
            self.GIP_CKEXTL_Filename
        )
        # Disable or Enable processing
        self._run_extract_tool = self.CKEXTLCOMMHandler.get_value_b(
            "ComputeExtractPoints"
        )
        LOGGER.debug("Run extract tool : " + str(self._run_extract_tool))

        # Register the GIPP file "GIP_CKQLTL"
        self.GIP_CKQLTL_Filename = gipp_utils.get_gipp_filename_with_mission(
            self._apphandler.get_input_directory(), "GIP_CKQLTL", self.Satellite
        )
        # Apply the stylesheet
        if self._apphandler._stylesheet is not None:
            self.GIP_CKQLTL_Filename = file_utils.copy_file_to_directory(
                self.GIP_CKQLTL_Filename,
                self._apphandler.get_working_directory(),
                notestmode=True,
            )
            translate_xsl(self.GIP_CKQLTL_Filename, self._apphandler._stylesheet)

        LOGGER.info(
            "The GIP_CKQLTL file detected for the satellite '"
            + self.Satellite
            + "' is <"
            + self.GIP_CKQLTL_Filename
            + ">."
        )

        if (
            self._apphandler.get_user_conf()
            .get_Processing()
            .get_CheckXMLFilesWithSchema()
        ):
            xml_tools.check_xml(
                self.GIP_CKQLTL_Filename,
                os.path.join(
                    self._apphandler.get_schemas_root_install_dir(),
                    self.plugin.MAJA_INSTALL_SCHEMAS_DIR,
                ),
            )

        self.CKQLTLCOMMHandler = GippCKQLTLEarthExplorerXMLFileHandler(
            self.GIP_CKQLTL_Filename
        )
        # Disable or Enable processing
        self._run_quicklook_tool = self.CKQLTLCOMMHandler.get_value_b("ComputeQL")
        LOGGER.debug("Run Quicktool : " + str(self._run_quicklook_tool))
        LOGGER.debug("L2ChecktoolProcessor pre_processing done.")

    def l2chain_post_procesing(self, hdrfilename):

        LOGGER.debug(" Starting scientific processing for checktools...")
        l2_image_reader = L2ImageReaderProvider.create(
            hdrfilename,
            self._apphandler,
            True,
            self.DataDEMMap.get(self._l1product.UniqueSatellite),
        )
        self.Satellite = self._l1product.Satellite

        LOGGER.debug(l2_image_reader)
        LOGGER.debug(self.Satellite)

        self.pre_processing()
        LOGGER.debug(self._run_extract_tool)
        LOGGER.debug(self._run_quicklook_tool)
        if self._run_quicklook_tool is False and self._run_extract_tool is False:
            LOGGER.debug(
                "No L2 check tool processing detected: the 'extract points' and 'checktools'"
                " processing are disabled."
            )
        else:
            self.scientific_processing_one_product(l2_image_reader)

    def scientific_processing_one_product(self, l2_image_reader):

        self.qck_filename_provider.initialize(
            l2_image_reader, self._apphandler.get_output_directory()
        )
        dict_outputs = {}

        if self._run_quicklook_tool == True:
            LOGGER.debug("Running Quicklook computation...")
            l_NbIndex = self.CKQLTLCOMMHandler.get_l2_count()
            l_Fontize = self.CKQLTLCOMMHandler.get_value_i("FontSize")

            for i in range(0, l_NbIndex):
                l_RedBandCode = self.CKQLTLCOMMHandler.get_l2_ql_color_band(
                    i + 1, "Red"
                )
                l_GreenBandCode = self.CKQLTLCOMMHandler.get_l2_ql_color_band(
                    i + 1, "Green"
                )
                l_BlueBandCode = self.CKQLTLCOMMHandler.get_l2_ql_color_band(
                    i + 1, "Blue"
                )
                (
                    redBandId,
                    greenBandId,
                    blueBandId,
                    idresolution,
                ) = self.plugin.BandsDefinitions.get_l2_information_for_quicklook_band_code(
                    l_RedBandCode, l_GreenBandCode, l_BlueBandCode
                )

                dict_params = {}
                dict_params = {
                    "RedBandCode": redBandId,
                    "GreenBandCode": greenBandId,
                    "BlueBandCode": blueBandId,
                    "AOTRatio": self.CKQLTLCOMMHandler.get_l2_aot_ratio(i + 1),
                    "AOTMin": self.CKQLTLCOMMHandler.get_l2_min_aot(i + 1),
                    "AOTMax": self.CKQLTLCOMMHandler.get_l2_max_aot(i + 1),
                    "VAPRatio": self.CKQLTLCOMMHandler.get_l2_water_vapor_ratio(i + 1),
                    "VAPMin": self.CKQLTLCOMMHandler.get_l2_min_water_vapor(i + 1),
                    "VAPMax": self.CKQLTLCOMMHandler.get_l2_max_water_vapor(i + 1),
                    "AOTText": l2_image_reader.Acquisition_Date + "\nAOT",
                    "VAPText": l2_image_reader.Acquisition_Date + "\nVAP",
                    "SREText": l2_image_reader.Acquisition_Date + "\nSRE",
                    "FREText": l2_image_reader.Acquisition_Date + "\nFRE",
                    "FontSize": l_Fontize,
                    "Font": self._apphandler._shareConfigInstallDirectoryRoot
                    + "Fonts/amble/Amble-Italic.ttf",
                    "MinValueBRed": self.CKQLTLCOMMHandler.get_l2_min_ref_color_band(
                        i + 1, "Red"
                    ),
                    "MaxValueBRed": self.CKQLTLCOMMHandler.get_l2_max_ref_color_band(
                        i + 1, "Red"
                    ),
                    "MinValueBGreen": self.CKQLTLCOMMHandler.get_l2_min_ref_color_band(
                        i + 1, "Green"
                    ),
                    "MaxValueBGreen": self.CKQLTLCOMMHandler.get_l2_max_ref_color_band(
                        i + 1, "Green"
                    ),
                    "MinValueBBlue": self.CKQLTLCOMMHandler.get_l2_min_ref_color_band(
                        i + 1, "Blue"
                    ),
                    "MaxValueBBlue": self.CKQLTLCOMMHandler.get_l2_max_ref_color_band(
                        i + 1, "Blue"
                    ),
                    "NoData": l2_image_reader.Nodata_Value,
                    "AOTNoData": l2_image_reader.AOT_Nodata_Value,
                    "VAPNoData": l2_image_reader.VAP_Nodata_Value,
                    "Ratio": self.CKQLTLCOMMHandler.get_l2_undersampling_ratio(i + 1),
                }

                LOGGER.debug(
                    "L2 Image Reader AOT List : "
                    + str(l2_image_reader.get_value("L2AOTList"))
                )
                dict_inputs = {
                    "L2AOT": l2_image_reader.get_value("L2AOTList")[i],
                    "L2VAP": l2_image_reader.get_value("L2VAPList")[i],
                    "L2SRE": l2_image_reader.get_value("L2SREList")[i],
                    "L2CLD": l2_image_reader.get_value("L2CLDList")[i],
                    "L2WAT": l2_image_reader.get_value("L2WATList")[i],
                    "AppHandler": self._apphandler,
                    "L2Reader": l2_image_reader,
                    "Params": dict_params,
                }

                if l2_image_reader.get_value("L2FREList"):
                    dict_inputs["L2FRE"] = l2_image_reader.get_value("L2FREList")[i]
                else:
                    dict_inputs["L2FRE"] = None

                if l2_image_reader.get_value("L2SNOWList"):
                    dict_inputs["L2SNOW"] = l2_image_reader.get_value("L2SNOWList")[i]
                else:
                    dict_inputs["L2SNOW"] = None

                LOGGER.debug(dict_inputs)

                ct_processing = MajaModule.create("LaunchQuickLook")
                ct_processing.launch(dict_inputs, dict_outputs)

            # copy files to output directory
            qlk_path = (
                self.qck_filename_provider.get_dbl_dir()
                + os.path.sep
                + "L2VALD_"
                + l2_image_reader.Nick_Name
                + "_"
                + l2_image_reader.Acquisition_Date
                + "_"
                + "CKQL2_"
            )

            # Setup quicklook images writer
            l2_processor_image_writer_setup.setup_l2qck_image_writer(
                self.qck_filename_provider, qlk_path, dict_inputs, dict_outputs
            )
            # Log system infos
            LOGGER.info(self._apphandler.get_system_infos())

        if self._run_extract_tool == True:
            # Preparation of inputs
            nb_pixels = self.CKEXTLCOMMHandler.get_pixel_count()
            pixcoords = self.CKEXTLCOMMHandler.get_pixel_coordinates()
            LOGGER.debug("Running Extract tool...")

            usefre = l2_image_reader.Adjacency_Effects_And_Slope_Correction
            usefre_bool = True
            if(usefre.lower() == "false"): usefre_bool = False

            #TODO: add function to verify pixcoords is into the ROI
            # pixcoords list is disable and limited to the first site location
            dict_params = {
                "CurrentPixel": pixcoords[0],
                "NbResolutions": len(l2_image_reader.plugin.ListOfL2Resolutions),
                "NbBands": len(
                    l2_image_reader.plugin.BandsDefinitions.get_list_of_band_id_in_l2_coarse()
                ),
                "Radius": self.CKEXTLCOMMHandler.GetRadius(),
                "AOTRadius": self.CKEXTLCOMMHandler.GetAOTVaporRadius(),
                "VAPRadius": self.CKEXTLCOMMHandler.GetWaterVaporRadius(),
                "L2nodata": l2_image_reader.RealL2Nodata_Value,
                "AOTnodata": l2_image_reader.AOT_Nodata_Value,
                "VAPnodata": l2_image_reader.VAP_Nodata_Value,
                "UseFRE": usefre_bool,
            }

            dict_inputs = {
                "L2AOT": l2_image_reader.get_value("L2AOTList"),
                "L2VAP": l2_image_reader.get_value("L2VAPList"),
                "L2SRE": l2_image_reader.get_value("L2SREList"),
                "L2CLD": l2_image_reader.get_value("L2CLDList"),
                "L2EDG": l2_image_reader.get_value("L2EDGList"),
                "L2WAT": l2_image_reader.get_value("L2WATList"),
                "AppHandler": self._apphandler,
                "L2Reader": l2_image_reader,
                "QCKProvider": self.qck_filename_provider,
                "Params": dict_params,
            }

            if l2_image_reader.get_value("L2FREList"):
                dict_inputs["L2FRE"] = l2_image_reader.get_value("L2FREList")
            else:
                dict_inputs["L2FRE"] = None

            ext_processing = MajaModule.create("LaunchExtract")
            ext_processing.launch(dict_inputs, dict_outputs)

            l2_processor_image_writer_setup.setup_l2qck_xml_writer(
                dict_inputs, dict_outputs
            )

        L2COMM_full_path = gipp_utils.get_gipp_filename_with_mission(
            self._apphandler.get_input_directory(), "GIP_L2COMM", self.Satellite
        )

        l2_processor_image_writer_setup.write_earthexplorer_qck_header_file(
            l2_image_reader,
            self._apphandler,
            self.Satellite,
            self.qck_filename_provider,
            self.plugin.L2QCK_FILE_VERSION,
            L2COMM_full_path,
            self.GIP_CKQLTL_Filename,
            self.GIP_CKEXTL_Filename,
        )

        if (
            self._apphandler.get_user_conf()
            .get_Processing()
            .get_CheckXMLFilesWithSchema()
        ):
            xsdfile = (
                self.schemaLocation
                + os.path.sep
                + self.plugin.XSD_PREFIX_MAP[self.Satellite]
                + "QCK_PDTQCK_QualityCheckDataProduct.xsd"
            )
            xml_tools.check_xml(
                self.qck_filename_provider.get_header_filename_fullpath(),
                self.schemaLocation,
                xsdfile,
            )
