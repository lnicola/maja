# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
from orchestrator.common.xml_tools import as_bool
import os
from orchestrator.modules.maja_module import MajaModule

from orchestrator.cots.otb.algorithms.otb_extract_roi import extract_roi
from orchestrator.cots.otb.algorithms.otb_band_math import band_math
from orchestrator.cots.otb.algorithms.otb_apply_mask import apply_mask
from orchestrator.cots.otb.algorithms.otb_rescale import rescale_intensity
from orchestrator.common.maja_exceptions import *
import orchestrator.common.constants as constants

LOGGER = configure_logger(__name__)


class MajaLaunchQuickLook(MajaModule):

    """
    classdocs

    """

    NAME = "LaunchQuickLook"

    def __init__(self):
        """
        Constructor
        """
        super(MajaLaunchQuickLook, self).__init__()
        self.l2_pipeline = OtbPipelineManager()
        self.in_keys_to_check = [
            "AppHandler",
            "L2Reader",
            "L2AOT",
            "L2SRE",
            "L2FRE",
            "L2VAP",
            "L2CLD",
            "L2SNOW",
            "L2WAT",
            "Params.RedBandCode",
            "Params.GreenBandCode",
            "Params.BlueBandCode",
            "Params.AOTRatio",
            "Params.AOTMin",
            "Params.AOTMax",
            "Params.VAPRatio",
            "Params.VAPMin",
            "Params.VAPMax",
            "Params.AOTText",
            "Params.VAPText",
            "Params.SREText",
            "Params.FREText",
            "Params.FontSize",
            "Params.Font",
            "Params.MinValueBRed",
            "Params.MaxValueBRed",
            "Params.MinValueBGreen",
            "Params.MaxValueBGreen",
            "Params.MinValueBBlue",
            "Params.MaxValueBGreen",
            "Params.NoData",
            "Params.AOTNoData",
            "Params.VAPNoData",
        ]

        self.out_keys_to_check = []
        self.out_keys_provided = ["aot", "vap", "sre"]

    def run(self, dict_of_input, dict_of_output):

        # Get some data
        ct_working = (
            dict_of_input.get("AppHandler")
            .get_directory_manager()
            .get_temporary_directory("CheckingTool_", do_always_remove=True)
        )
        inputs = {}
        inputs["aot"] = {
            "in": dict_of_input["L2AOT"],
            "ratio": dict_of_input.get("Params").get("AOTRatio"),
            "min": dict_of_input.get("Params").get("AOTMin"),
            "max": dict_of_input.get("Params").get("AOTMax"),
            "text": dict_of_input.get("Params").get("AOTText"),
        }

        inputs["vap"] = {
            "in": dict_of_input["L2VAP"],
            "ratio": dict_of_input.get("Params").get("VAPRatio"),
            "min": dict_of_input.get("Params").get("VAPMin"),
            "max": dict_of_input.get("Params").get("VAPMax"),
            "text": dict_of_input.get("Params").get("VAPText"),
        }

        inputs["sre"] = {
            "in": dict_of_input["L2SRE"],
            "ratio": dict_of_input.get("Params").get("Ratio"),
            "redband": dict_of_input.get("Params").get("RedBandCode"),
            "greenband": dict_of_input.get("Params").get("GreenBandCode"),
            "blueband": dict_of_input.get("Params").get("BlueBandCode"),
            "nodata": dict_of_input.get("Params").get("NoData"),
            "m_MinValueBRed": dict_of_input.get("Params").get("MinValueBRed"),
            "m_MinValueBGreen": dict_of_input.get("Params").get("MinValueBGreen"),
            "m_MinValueBBlue": dict_of_input.get("Params").get("MinValueBBlue"),
            "m_MaxValueBRed": dict_of_input.get("Params").get("MaxValueBRed"),
            "m_MaxValueBGreen": dict_of_input.get("Params").get("MaxValueBGreen"),
            "m_MaxValueBBlue": dict_of_input.get("Params").get("MaxValueBBlue"),
            "text": dict_of_input.get("Params").get("SREText"),
        }

        if dict_of_input["L2FRE"] is not None:
            inputs["fre"] = {
                "in": dict_of_input["L2FRE"],
                "ratio": dict_of_input.get("Params").get("Ratio"),
                "redband": dict_of_input.get("Params").get("RedBandCode"),
                "greenband": dict_of_input.get("Params").get("GreenBandCode"),
                "blueband": dict_of_input.get("Params").get("BlueBandCode"),
                "nodata": dict_of_input.get("Params").get("NoData"),
                "m_MinValueBRed": dict_of_input.get("Params").get("MinValueBRed"),
                "m_MinValueBGreen": dict_of_input.get("Params").get("MinValueBGreen"),
                "m_MinValueBBlue": dict_of_input.get("Params").get("MinValueBBlue"),
                "m_MaxValueBRed": dict_of_input.get("Params").get("MaxValueBRed"),
                "m_MaxValueBGreen": dict_of_input.get("Params").get("MaxValueBGreen"),
                "m_MaxValueBBlue": dict_of_input.get("Params").get("MaxValueBBlue"),
                "text": dict_of_input.get("Params").get("FREText"),
            }

        # SHD Mask
        out_bandmath = os.path.join(ct_working, "shd_bandmath.tif")
        app_bandmath = band_math(
            [
                dict_of_input["L2CLD"][constants.CLOUD_MASK_SHADOWS],
                dict_of_input["L2CLD"][constants.CLOUD_MASK_SHADVAR],
            ],
            "im1b1+im2b1",
            out_bandmath,
            write_output=False,
        )
        self.l2_pipeline.add_otb_app(app_bandmath)

        for k, v in inputs.items():
            # Generate cloud mask quick look
            out_cldquicklook = os.path.join(
                ct_working, "cld_quicklook_{}.tif".format(k)
            )
            param_cldquicklook = {
                "in": dict_of_input["L2CLD"][constants.CLOUD_MASK_ALL_CLOUDS],
                "out": out_cldquicklook,
                "sr": inputs[k]["ratio"],
            }
            app_cldquicklook = OtbAppHandler(
                "Quicklook", param_cldquicklook, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_cldquicklook)

            # Generate water mask quick look
            out_watquicklook = os.path.join(
                ct_working, "wat_quicklook_{}.tif".format(k)
            )
            param_watquicklook = {
                "in": dict_of_input["L2WAT"],
                "out": out_watquicklook,
                "sr": inputs[k]["ratio"],
            }
            app_watquicklook = OtbAppHandler(
                "Quicklook", param_watquicklook, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_watquicklook)

            # Generate shadow mask quick look
            out_shdquicklook = os.path.join(
                ct_working, "shd_quicklook_{}.tif".format(k)
            )
            param_shdquicklook = {
                "in": app_bandmath.getoutput().get("out"),
                "out": out_shdquicklook,
                "sr": inputs[k]["ratio"],
            }
            app_shdquicklook = OtbAppHandler(
                "Quicklook", param_shdquicklook, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_shdquicklook)

            # Generate edge mask
            out_cldedg = os.path.join(ct_working, "cld_edg_{}.tif".format(k))
            param_cldedg = {
                "in": app_cldquicklook.getoutput().get("out"),
                "out": out_cldedg,
            }
            app_cldedg = OtbAppHandler(
                "EdgeExtraction", param_cldedg, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_cldedg)

            out_watedg = os.path.join(ct_working, "wat_edg_{}.tif".format(k))
            param_watedg = {
                "in": app_watquicklook.getoutput().get("out"),
                "out": out_watedg,
            }
            app_watedg = OtbAppHandler(
                "EdgeExtraction", param_watedg, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_watedg)

            out_shdedg = os.path.join(ct_working, "shd_edg_{}.tif".format(k))
            param_shdedg = {
                "in": app_shdquicklook.getoutput().get("out"),
                "out": out_shdedg,
            }
            app_shdedg = OtbAppHandler(
                "EdgeExtraction", param_shdedg, write_output=False
            )
            self.l2_pipeline.add_otb_app(app_shdedg)

            if k == "vap" or k == "aot":
                # Generate input quick look
                out_quicklook = os.path.join(ct_working, "quicklook_{}.tif".format(k))
                param_quicklook = {
                    "in": inputs[k]["in"],
                    "out": out_quicklook,
                    "sr": inputs[k]["ratio"],
                }
                app_quicklook = OtbAppHandler(
                    "Quicklook", param_quicklook, write_output=False
                )
                self.l2_pipeline.add_otb_app(app_quicklook)

                # Rainbow effect
                out_qck_cs = os.path.join(ct_working, "qck_colorscale_{}.tif".format(k))
                param_qck_cs = {
                    "in": app_quicklook.getoutput().get("out"),
                    "out": out_qck_cs,
                    "min": inputs[k]["min"],
                    "max": inputs[k]["max"],
                }
                app_qck = OtbAppHandler("ColorScale", param_qck_cs, write_output=False)
                self.l2_pipeline.add_otb_app(app_qck)

            elif k == "sre" or k == "fre":
                # Extract band of the XS image
                out_extractroi = os.path.join(ct_working, "extract_{}.tif".format(k))
                app_extract = extract_roi(
                    inputs[k]["in"],
                    [
                        inputs[k]["redband"],
                        inputs[k]["greenband"],
                        inputs[k]["blueband"],
                    ],
                    out_extractroi + "?&nodata={}".format(inputs[k]["nodata"]),
                    write_output=False,
                )
                self.l2_pipeline.add_otb_app(app_extract)
                # Change NoData value into noDataReplacePixel
                out_changevalue = os.path.join(
                    ct_working, "extract_{}_replacenodata.tif".format(k)
                )
                param_changevalue = {
                    "in": app_extract.getoutput().get("out"),
                    "out": out_changevalue,
                    "mode": "changevalue",
                    "mode.changevalue.newv": inputs[k]["m_MinValueBRed"],
                }
                app_changevalue = OtbAppHandler(
                    "ManageNoData", param_changevalue, write_output=False
                )
                self.l2_pipeline.add_otb_app(app_changevalue)

                # Generate input quick look
                out_quicklook = os.path.join(ct_working, "quicklook_{}.tif".format(k))
                param_quicklook = {
                    "in": app_changevalue.getoutput().get("out"),
                    "out": out_quicklook,
                    "sr": inputs[k]["ratio"],
                }
                app_quicklook = OtbAppHandler(
                    "Quicklook", param_quicklook, write_output=False
                )
                self.l2_pipeline.add_otb_app(app_quicklook)

                out_rescale = os.path.join(
                    ct_working, "rescale_{}_replacenodata.tif".format(k)
                )
                app_qck = rescale_intensity(
                    app_quicklook.getoutput().get("out"),
                    0,
                    255,
                    out_rescale,
                    inmin=[
                        str(inputs[k]["m_MinValueBRed"]),
                        str(inputs[k]["m_MinValueBGreen"]),
                        str(inputs[k]["m_MinValueBBlue"]),
                    ],
                    inmax=[
                        str(inputs[k]["m_MaxValueBRed"]),
                        str(inputs[k]["m_MaxValueBGreen"]),
                        str(inputs[k]["m_MaxValueBBlue"]),
                    ],
                    write_output=False,
                )
                self.l2_pipeline.add_otb_app(app_qck)
            else:
                raise MajaDataException("Invalid quicklook key.")

            out_cld_bm = os.path.join(ct_working, "cld_bm_{}.tif".format(k))
            app_cld_bm = band_math(
                [app_cldedg.getoutput().get("out")],
                "im1b1>0?0:1",
                out_cld_bm,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_cld_bm)

            out_wat_bm = os.path.join(ct_working, "wat_bm_{}.tif".format(k))
            app_wat_bm = band_math(
                [app_watedg.getoutput().get("out")],
                "im1b1>0?0:1",
                out_wat_bm,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_wat_bm)

            out_shd_bm = os.path.join(ct_working, "shd_bm_{}.tif".format(k))
            app_shd_bm = band_math(
                [app_shdedg.getoutput().get("out")],
                "im1b1>0?0:1",
                out_shd_bm,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_shd_bm)

            # Add cloud
            out_cld_mn = os.path.join(ct_working, "cld_mn_{}.tif".format(k))
            app_cld_mn = apply_mask(
                app_qck.getoutput().get("out"),
                app_cld_bm.getoutput().get("out"),
                ["255", "0", "0"],
                out_cld_mn,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_cld_mn)

            # Add water
            out_wat_mn = os.path.join(ct_working, "wat_mn_{}.tif".format(k))
            app_wat_mn = apply_mask(
                app_cld_mn.getoutput().get("out"),
                app_wat_bm.getoutput().get("out"),
                ["0", "255", "255"],
                out_wat_mn,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_wat_mn)

            # Add shadows
            out_shd_mn = os.path.join(ct_working, "shd_mn_{}.tif".format(k))
            app_shd_mn = apply_mask(
                app_wat_mn.getoutput().get("out"),
                app_shd_bm.getoutput().get("out"),
                ["0", "255", "0"],
                out_shd_mn,
                write_output=False,
            )
            self.l2_pipeline.add_otb_app(app_shd_mn)

            if dict_of_input["L2SNOW"] is not None:
                # Generate snow quick look
                out_snowquicklook = os.path.join(
                    ct_working, "snow_quicklook_{}.tif".format(k)
                )
                param_snowquicklook = {
                    "in": dict_of_input["L2SNOW"],
                    "out": out_snowquicklook,
                    "sr": inputs[k]["ratio"],
                }
                app_snowquicklook = OtbAppHandler(
                    "Quicklook", param_snowquicklook, write_output=False
                )
                self.l2_pipeline.add_otb_app(app_snowquicklook)

                out_snowedg = os.path.join(ct_working, "snow_edg_{}.tif".format(k))
                param_snowedg = {
                    "in": app_snowquicklook.getoutput().get("out"),
                    "out": out_snowedg,
                }
                app_snowedg = OtbAppHandler(
                    "EdgeExtraction", param_snowedg, write_output=False
                )
                self.l2_pipeline.add_otb_app(app_snowedg)

                out_snow_bm = os.path.join(ct_working, "snow_bm_{}.tif".format(k))
                app_snow_bm = band_math(
                    [app_snowedg.getoutput().get("out")],
                    "im1b1>0?0:1",
                    out_snow_bm,
                    write_output=False,
                )
                self.l2_pipeline.add_otb_app(app_snow_bm)

                out_snow_mn = os.path.join(ct_working, "snow_mn_{}.tif".format(k))
                app_snow_mn = apply_mask(
                    app_wat_mn.getoutput().get("out"),
                    app_shd_bm.getoutput().get("out"),
                    ["255"],
                    out_snow_mn,
                    write_output=False,
                )
                self.l2_pipeline.add_otb_app(app_snow_bm)

                out_write = os.path.join(ct_working, "snw_written_{}.jpg".format(k))
                param_write = {
                    "in": app_snow_mn.getoutput().get("out"),
                    "out": out_write + ":uint8",
                    "font": dict_of_input.get("Params").get("Font"),
                    "fontsize": dict_of_input.get("Params").get("FontSize"),
                    "text": inputs[k]["text"],
                    "red": 0,
                    "green": 255,
                    "blue": 255,
                }
                app_write = OtbAppHandler(
                    "WriteTxtOnImage", param_write, write_output=True
                )
                dict_of_output[k] = app_write.getoutput().get("out")

            else:
                out_write = os.path.join(ct_working, "shd_written_{}.jpg".format(k))
                param_write = {
                    "in": app_shd_mn.getoutput().get("out"),
                    "out": out_write + ":uint8",
                    "font": dict_of_input.get("Params").get("Font"),
                    "fontsize": dict_of_input.get("Params").get("FontSize"),
                    "text": inputs[k]["text"],
                    "red": 0,
                    "green": 255,
                    "blue": 255,
                }
                app_write = OtbAppHandler(
                    "WriteTxtOnImage", param_write, write_output=True
                )
                dict_of_output[k] = app_write.getoutput().get("out")
