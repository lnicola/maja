# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {or|)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
from orchestrator.common.xml_tools import as_bool
import os
import math
from orchestrator.modules.maja_module import MajaModule

from orchestrator.cots.otb.algorithms.otb_extract_roi import extract_roi
from orchestrator.cots.otb.algorithms.otb_band_math import band_math
from orchestrator.cots.otb.algorithms.otb_apply_mask import apply_mask
from orchestrator.cots.otb.algorithms.otb_rescale import rescale_intensity
from orchestrator.common.maja_exceptions import *
from operator import add

LOGGER = configure_logger(__name__)


class MajaLaunchExtract(MajaModule):
    """
    classdocs

    """

    NAME = "LaunchExtract"

    def __init__(self):
        """
        Constructor
        """
        super(MajaLaunchExtract, self).__init__()
        self._extract_pipeline = OtbPipelineManager()
        self.in_keys_to_check = [
            "AppHandler",
            "L2Reader",
            "QCKProvider",
            "L2AOT",
            "L2SRE",
            "L2FRE",
            "L2VAP",
            "L2CLD",
            "L2WAT",
            "Params.CurrentPixel",
            "Params.NbResolutions",
            "Params.NbBands",
            "Params.Radius",
            "Params.AOTRadius",
            "Params.VAPRadius",
            "Params.L2nodata",
            "Params.AOTnodata",
            "Params.VAPnodata",
            "Params.UseFRE",
        ]

        self.out_keys_to_check = []
        self.out_keys_provided = [
            "cloudvalr1",
            "cloudvalr2",
            "cloudvalr3",
            "edgevalr1",
            "edgevalr2",
            "edgevalr3",
            "watervalr1",
            "watervalr2",
            "watervalr3",
            "aotmeans",
            "vapmeans",
            "aotstdv",
            "vapstdv",
            "aotusefulpixelspercentage",
            "aotvalidpixelspercentage",
            "vapusefulpixelspercentage",
            "vapvalidpixelspercentage",
            "srestatisvalid",
            "frestatisvalid",
            "aotstatisvalid",
            "vapstatisvalid",
            "sremeans",
            "fremeans",
            "srestdv",
            "frestdv",
            "usefulpixpercent",
            "validpixpercent",
            "pixpointx",
            "pixpointy",
            "pixwgsx",
            "pixwgsy",
        ]

    def run(self, dict_of_input, dict_of_output):
        l_maxRatio = -math.inf
        l_CloudValR1 = 0
        l_WaterValR1 = 0
        l_EdgeValR1 = 0
        l_CloudValR2 = 0
        l_WaterValR2 = 0
        l_EdgeValR2 = 0
        l_CloudValR3 = 0
        l_WaterValR3 = 0
        l_EdgeValR3 = 0
        l_AOTMeans = 0.0
        l_VAPMeans = 0.0
        l_AOTStDv = 0.0
        l_VAPStDv = 0.0
        l_AOTUsefulPixelsPercentage = 0.0
        l_AOTValidPixelsPercentage = 0.0
        l_VAPUsefulPixelsPercentage = 0.0
        l_VAPValidPixelsPercentage = 0.0
        l_SREStatIsValid = 0
        l_FREStatIsValid = 0
        l_AOTStatIsValid = 0
        l_VAPStatIsValid = 0
        l_NbBands = len(
            dict_of_input[
                "L2Reader"
            ].plugin.BandsDefinitions.get_list_of_band_id_in_l2_coarse()
        )

        l_SREMeans = [0] * l_NbBands
        l_SREStDv = [0] * l_NbBands
        l_FREMeans = [0] * l_NbBands
        l_FREStDv = [0] * l_NbBands
        l_UsefulPixelsPercentage = [0] * l_NbBands
        l_ValidPixelsPercentage = [0] * l_NbBands

        l_NbRes = dict_of_input["Params"]["NbResolutions"]
        for i in range(0, l_NbRes):
            listbandcode = [
                str(e)
                for e in dict_of_input[
                    "L2Reader"
                ].plugin.BandsDefinitions.get_list_of_l2_band_code(
                    dict_of_input[
                        "L2Reader"
                    ].plugin.BandsDefinitions.ListOfL2Resolution[i]
                )
            ]
            listidsl2coarse = [
                str(e)
                for e in dict_of_input[
                    "L2Reader"
                ].plugin.BandsDefinitions.get_list_of_l2_band_id(
                    dict_of_input[
                        "L2Reader"
                    ].plugin.BandsDefinitions.ListOfL2Resolution[i]
                )
            ]
            LOGGER.debug("List band code :" + " ".join(listbandcode))
            LOGGER.debug("List ids l2coarse :" + " ".join(listidsl2coarse))
            extract_params = {
                "aot": dict_of_input["L2AOT"][i],
                "vap": dict_of_input["L2VAP"][i],
                "sre": dict_of_input["L2SRE"][i],
                "cld": dict_of_input["L2CLD"][i]["Cloud_Mask_all"],
                "wat": dict_of_input["L2WAT"][i],
                "edg": dict_of_input["L2EDG"][i],
                "listbandcode": listbandcode,
                "bandidsl2coarse": listidsl2coarse,
                "nbbands": l_NbBands,
                "refsre": dict_of_input["L2Reader"].get_value("L2SREList")[0],
                "currentpixel.x": dict_of_input["Params"]["CurrentPixel"][
                    "Coordinate_X"
                ],
                "currentpixel.y": dict_of_input["Params"]["CurrentPixel"][
                    "Coordinate_Y"
                ],
                "currentpixel.freename": dict_of_input["Params"]["CurrentPixel"][
                    "Freename"
                ],
                "currentpixel.unit": dict_of_input["Params"]["CurrentPixel"]["Unit"],
                "radius": dict_of_input["Params"]["Radius"],
                "aotradius": dict_of_input["Params"]["AOTRadius"],
                "vapradius": dict_of_input["Params"]["VAPRadius"],
                "nodata": dict_of_input["Params"]["L2nodata"],
                "aotnodata": dict_of_input["Params"]["AOTnodata"],
                "vapnodata": dict_of_input["Params"]["VAPnodata"],
            }

            if dict_of_input["L2FRE"] is not None:
                extract_params.update({"fre": dict_of_input["L2FRE"][i]})
                extract_params.update({"usefre": dict_of_input["Params"]["UseFRE"]})

            app_extract = OtbAppHandler("Extract", extract_params, write_output=True)
            self._extract_pipeline.add_otb_app(app_extract)

            # set results if available
            l_CloudValR1 = l_CloudValR1 or app_extract.getoutput().get("cloudvalr1")
            l_WaterValR1 = l_WaterValR1 or app_extract.getoutput().get("watervalr1")
            l_EdgeValR1 = l_EdgeValR1 or app_extract.getoutput().get("edgevalr1")
            l_CloudValR2 = l_CloudValR2 or app_extract.getoutput().get("cloudvalr2")
            l_WaterValR2 = l_WaterValR2 or app_extract.getoutput().get("watervalr2")
            l_EdgeValR2 = l_EdgeValR2 or app_extract.getoutput().get("edgevalr2")
            l_CloudValR3 = l_CloudValR3 or app_extract.getoutput().get("cloudvalr3")
            l_WaterValR3 = l_WaterValR3 or app_extract.getoutput().get("watervalr3")
            l_EdgeValR3 = l_EdgeValR3 or app_extract.getoutput().get("edgevalr3")

            # Compute the mean AOT and VAP values per resolution
            l_AOTMeans += app_extract.getoutput().get("aotmeans")
            l_VAPMeans += app_extract.getoutput().get("vapmeans")
            l_AOTStDv += app_extract.getoutput().get("aotstdv")
            l_VAPStDv += app_extract.getoutput().get("vapstdv")

            # //Compute pixel percentage
            l_AOTUsefulPixelsPercentage += app_extract.getoutput().get(
                "aotusefulpixelspercentage"
            )
            l_AOTValidPixelsPercentage += app_extract.getoutput().get(
                "aotvalidpixelspercentage"
            )
            l_VAPUsefulPixelsPercentage += app_extract.getoutput().get(
                "vapusefulpixelspercentage"
            )
            l_VAPValidPixelsPercentage += app_extract.getoutput().get(
                "vapvalidpixelspercentage"
            )

            # // Set results if available
            l_SREStatIsValid = l_SREStatIsValid or app_extract.getoutput().get(
                "srestatisvalid"
            )
            l_FREStatIsValid = l_FREStatIsValid or app_extract.getoutput().get(
                "frestatisvalid"
            )
            l_AOTStatIsValid = l_AOTStatIsValid or app_extract.getoutput().get(
                "aotstatisvalid"
            )
            l_VAPStatIsValid = l_VAPStatIsValid or app_extract.getoutput().get(
                "vapstatisvalid"
            )

            l_currentSREMeans = list(
                map(float, app_extract.getoutput().get("sremeans"))
            )
            l_currentSREStDv = list(map(float, app_extract.getoutput().get("srestdv")))
            l_currentFREMeans = list(
                map(float, app_extract.getoutput().get("fremeans"))
            )
            l_currentFREStDv = list(map(float, app_extract.getoutput().get("frestdv")))
            l_currentUsefulPixPercent = list(
                map(float, app_extract.getoutput().get("usefulpixpercent"))
            )
            l_currentValidPixPercent = list(
                map(float, app_extract.getoutput().get("validpixpercent"))
            )
            # l_SREMeans contains nb_bands zeros after initialization, it is filled for each band depending on the current resolution, so we add the values for each band to the previously initialized list
            l_SREMeans = list(map(add, l_SREMeans, l_currentSREMeans))
            l_SREStDv = list(map(add, l_SREStDv, l_currentSREStDv))
            l_FREMeans = list(map(add, l_FREMeans, l_currentFREMeans))
            l_FREStDv = list(map(add, l_FREStDv, l_currentFREStDv))
            l_UsefulPixelsPercentage = list(
                map(add, l_UsefulPixelsPercentage, l_currentUsefulPixPercent)
            )
            l_ValidPixelsPercentage = list(
                map(add, l_ValidPixelsPercentage, l_currentValidPixPercent)
            )
            ratio = app_extract.getoutput().get("ratio")
            # get the coordinate only for the higher resolution
            if ratio > l_maxRatio:
                dict_of_output["pixpointx"] = app_extract.getoutput().get("pixpointx")
                dict_of_output["pixpointy"] = app_extract.getoutput().get("pixpointy")
                dict_of_output["pixwgsx"] = app_extract.getoutput().get("pixwgsx")
                dict_of_output["pixwgsy"] = app_extract.getoutput().get("pixwgsy")
                l_maxRatio = ratio

        # Compute the global mean AOT and VAP values
        l_AOTMeans /= l_NbRes
        l_VAPMeans /= l_NbRes
        l_AOTStDv /= l_NbRes
        l_VAPStDv /= l_NbRes
        l_AOTUsefulPixelsPercentage /= l_NbRes
        l_AOTValidPixelsPercentage /= l_NbRes
        l_VAPUsefulPixelsPercentage /= l_NbRes
        l_VAPValidPixelsPercentage /= l_NbRes

        LOGGER.debug("AOTMeans : " + str(l_AOTMeans))
        LOGGER.debug("VAPMeans : " + str(l_VAPMeans))
        LOGGER.debug("AOTStDv : " + str(l_AOTStDv))
        LOGGER.debug("VAPStDv : " + str(l_VAPStDv))
        LOGGER.debug("SREMeans : " + str(l_SREMeans))
        LOGGER.debug("SREStDv : " + str(l_SREStDv))

        nb_bands = len(
            dict_of_input[
                "L2Reader"
            ].plugin.BandsDefinitions.get_list_of_band_id_in_l2_coarse()
        )
        # If not valid, force to One value to -999
        if l_SREStatIsValid == 0:
            LOGGER.debug("SRE stats are not valid, filling with ERROR_VALUE")
            l_SREMeans = [-999] * nb_bands
            l_SREStDv = [0.0] * nb_bands
            l_UsefulPixelsPercentage = [0.0] * nb_bands
            l_ValidPixelsPercentage = [0.0] * nb_bands
        # If not valid, force to One value to -999
        if l_FREStatIsValid == 0:
            LOGGER.debug("FRE stats are not valid, filling with ERROR_VALUE")
            l_FREMeans = [-999] * nb_bands
            l_FREStDv = [0.0] * nb_bands
        # If not valid, force to One value to -999
        if l_VAPStatIsValid == 0:
            LOGGER.debug("VAP stats are not valid, filling with ERROR_VALUE")
            l_VAPMeans = -999.0
            l_VAPStDv = 0.0
            l_VAPUsefulPixelsPercentage = 0.0
            l_VAPValidPixelsPercentage = 0.0
        # If not valid, force to One value to -999
        if l_AOTStatIsValid == 0:
            LOGGER.debug("AOT stats are not valid, filling with ERROR_VALUE")
            l_AOTMeans = -999.0
            l_AOTStDv = 0.0
            l_AOTUsefulPixelsPercentage = 0.0
            l_AOTValidPixelsPercentage = 0.0

        # Set outputs to be read by the header writer
        # AOT Outputs
        dict_of_output["aotmeans"] = l_AOTMeans
        dict_of_output["aotstdv"] = l_AOTStDv
        dict_of_output["aotstatisvalid"] = l_AOTStatIsValid
        dict_of_output["aotvalidpixelspercentage"] = l_AOTUsefulPixelsPercentage
        dict_of_output["aotusefulpixelspercentage"] = l_AOTValidPixelsPercentage
        # VAP Outputs
        dict_of_output["vapmeans"] = l_VAPMeans
        dict_of_output["vapstdv"] = l_VAPStDv
        dict_of_output["vapstatisvalid"] = l_VAPStatIsValid
        dict_of_output["vapvalidpixelspercentage"] = l_VAPUsefulPixelsPercentage
        dict_of_output["vapusefulpixelspercentage"] = l_VAPValidPixelsPercentage
        # SRE outputs
        dict_of_output["sremeans"] = l_SREMeans
        dict_of_output["srestdv"] = l_SREStDv
        dict_of_output["srestatisvalid"] = l_SREStatIsValid
        # FRE outputs
        dict_of_output["fremeans"] = l_FREMeans
        dict_of_output["frestdv"] = l_FREStDv
        dict_of_output["frestatisvalid"] = l_FREStatIsValid
        # Image wise useful and valid pixel percentage
        dict_of_output["usefulpixpercent"] = l_UsefulPixelsPercentage
        dict_of_output["validpixpercent"] = l_ValidPixelsPercentage
        # Reflectances, AOT, VAP outputs
        dict_of_output["cloudvalr1"] = l_CloudValR1
        dict_of_output["cloudvalr2"] = l_CloudValR2
        dict_of_output["cloudvalr3"] = l_CloudValR3
        dict_of_output["edgevalr1"] = l_EdgeValR1
        dict_of_output["edgevalr2"] = l_EdgeValR2
        dict_of_output["edgevalr3"] = l_EdgeValR3
        dict_of_output["watervalr1"] = l_WaterValR1
        dict_of_output["watervalr2"] = l_WaterValR2
        dict_of_output["watervalr3"] = l_WaterValR3

        # -------------------------------------------------------------
        # Write the header of the extracted product
        # -------------------------------------------------------------
