# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.algorithms.otb_constant_image import constant_image
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
import orchestrator.common.constants as constants
from orchestrator.modules.maja_module import MajaModule
from orchestrator.common.maja_exceptions import MajaDataException

from osgeo import gdal

import os, io

LOGGER = configure_logger(__name__)


class MajaAOTOnlyFromAux(MajaModule):
    """
    Maja module for AOT from aux data (CAMS or Climato).

    ...

    Methods
    -------
        cleanup:
            Free otb pipeline
        run:
            Run the maja module
    """

    NAME = "AOTOnlyFromAux"

    def __init__(self):
        """
        Init MajaModule
        """
        super(MajaAOTOnlyFromAux, self).__init__()
        self._aot_pipeline = OtbPipelineManager()

        self.in_keys_to_check = [
            "Params.Caching",
            "Params.WriteL2ProductToL2Resolution",
            "AppHandler",
            "Plugin",
            "L1Reader",
            "L2COMM",
            "DEM",
            "L2TOCR",
            "L2SITE",
        ]

        self.out_keys_provided = ["AOT_Sub"]

    def cleanup(self):
        """
        Clean pipeline.
        """
        self._aot_pipeline.free_otb_app()

    def run(self, dict_of_input, dict_of_output):
        """
        Run gdalwarp and differents otb application to compute AOT from aux data.

        Parameters
        ----------
            dict_of_input : dict
                dictionary who contain the inputs need for the different maja module
            dict_of_output : dict
                dictionary who contain the outputs produce by maja module
        """
        LOGGER.info("AOT from aux start")
        LOGGER.debug("Caching %s", dict_of_input.get("Params").get("Caching"))
        # Get parameters from dict_of_input
        l_caching = dict_of_input.get("Params").get("Caching")
        l_writeL2 = dict_of_input.get("Params").get("WriteL2ProductToL2Resolution")

        aot_working = (
            dict_of_input.get("AppHandler")
            .get_directory_manager()
            .get_temporary_directory("AOTFromAux_", do_always_remove=True)
        )

        bands_definition = dict_of_input.get("Plugin").BandsDefinitions

        # Get the world AOT whether from CAMS or Climato
        aot_type = None
        if dict_of_input.get("Params").get("UseCamsData"):
            LOGGER.debug("AOT from CAMS data")
            aot_world = dict_of_input.get("CAMS_AOT")
            aot_type = "aot.cams"
        elif dict_of_input.get("Params").get("UseClimatoData"):
            LOGGER.debug("AOT from Climato data")
            aot_world = dict_of_input.get("CLIMATO_AOT")
            aot_type = "aot.climato"
        else:
            raise MajaDataException(
                "Impossible to compute AOT from aux, no CAMS or Climato data!"
            )

        intermediate_aot_image_sub = os.path.join(aot_working, "intermediate_aot_image_sub.tif")
        aot_mask_sub = os.path.join(aot_working, "aot_mask_sub.tif")

        # Need to resample AOT at coarse resolution
        param_aotresampling = {
            "dtm": dict_of_input.get("DEM").ALC,
            aot_type: aot_world,
            "aotresampled": intermediate_aot_image_sub,
        }
        aotresampling_app = OtbAppHandler("AOTResampling", param_aotresampling, write_output=l_caching)
        if not l_caching:
            self._aot_pipeline.add_otb_app(aotresampling_app)

        # Compute the AOT according to the altitude of the pixel, following an exponential decrease
        aot_image_sub = os.path.join(aot_working, "aot_image_sub.tif")
        mean_alt = str(dict_of_input.get("DEM").ALT_Mean)
        param_aotalt_bandmath_app = {
            "il": [dict_of_input.get("DEM").ALC, aotresampling_app.getoutput()["aotresampled"]],
            "out": aot_image_sub,
            "exp": "(im1b1<" + mean_alt + ")?"
                   + "im2b1:"
                   + "im2b1*exp(-(im1b1-"
                   + mean_alt
                   + ")/2000)"
        }
        aotalt_bandmath_app = OtbAppHandler("BandMath", param_aotalt_bandmath_app, write_output=l_caching)
        if not l_caching:
            self._aot_pipeline.add_otb_app(aotalt_bandmath_app)

        # Creation of aot_mask who contain only 1 (1 = aot from aux)
        # AOT compute only from aux data (CAMS or CLIMATO)
        param_bandmath = {
            "il": [dict_of_input.get("DEM").ALC],
            "out": aot_mask_sub,
            "exp": "1"
        }
        bandmath_app = OtbAppHandler("BandMath", param_bandmath, write_output=l_caching)
        if not l_caching:
            self._aot_pipeline.add_otb_app(bandmath_app)

        dict_of_output["AOT_Sub"] = aotalt_bandmath_app.getoutput().get("out")
        # Write AOT and aotmask to L2 resolution
        # 20m for SPOT 1-4
        # 10m for SPOT 5
        if l_writeL2:
            l_nbRes = len(bands_definition.ListOfL2Resolution)
            aot_list = []
            aotmask_list = []
            for r in range(0, l_nbRes):
                l_res = bands_definition.ListOfL2Resolution[r]
                aot_image = os.path.join(aot_working, "aot_" + l_res + ".tif")
                aot_mask = os.path.join(aot_working, "aotmask_" + l_res + ".tif")
                # Write aot_image
                param_subresampling = {
                    "dtm": dict_of_input.get("DEM").ALTList[r],
                    "im": aot_image_sub,
                    "interp": "linear",
                    "out": aot_image
                }
                resamp_aot_app = OtbAppHandler("Resampling", param_subresampling)
                dict_of_output["AOT_" + l_res] = resamp_aot_app.getoutput().get("out")

                # Write aot_mask
                param_maskresampling = {
                    "dtm": dict_of_input.get("DEM").ALTList[r],
                    "im": aot_mask_sub,
                    "interp": "linear",
                    "out": aot_mask + ":uint8",
                    "threshold": 0.25,
                }
                resamp_aotmask_app = OtbAppHandler("Resampling", param_maskresampling)
                dict_of_output["AOTMASK_" + l_res] = resamp_aotmask_app.getoutput().get(
                    "out"
                )
                aot_list.append(resamp_aot_app.getoutput().get("out"))
                aotmask_list.append(resamp_aotmask_app.getoutput().get("out"))
            # Update output dict with created files
            dict_of_output["L2AOTList"] = aot_list
            dict_of_output["L2AOTMaskList"] = aotmask_list
