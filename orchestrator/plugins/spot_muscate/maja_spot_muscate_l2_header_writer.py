# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.maja_dataset -- shortdesc

orchestrator.plugins.maja_dataset is a description

It defines classes_and_methods

###################################################################################################
"""
from orchestrator.plugins.common.muscate.maja_muscate_l2_header_writer import (
    MajaMuscateL2HeaderWriter,
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_plugin import (
    MajaSpot1MuscatePlugin, MajaSpot2MuscatePlugin, MajaSpot3MuscatePlugin, MajaSpot4MuscatePlugin,
    MajaSpot5MuscatePlugin
)
from orchestrator.plugins.common.base.maja_l2_image_filenames_provider import (
    L2ImageFilenamesProvider,
)

from orchestrator.common import gipp_utils
import orchestrator.common.file_utils as file_utils
import orchestrator.common.date_utils as date_utils
import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.muscate.muscate_xml_file_handler import MuscateXMLFileHandler
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.common.maja_common import PointXY
from orchestrator.common.maja_exceptions import MajaPluginMuscateException
from orchestrator.common.earth_explorer.earth_explorer_utilities import (
    EarthExplorerUtilities,
)
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common import version

import os, datetime
from lxml import etree as ET
import math
import re


LOGGER = configure_logger(__name__)


class MajaSpotMuscateL2HeaderWriter(MajaMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpotMuscateL2HeaderWriter, self).__init__()

    def write(self):
        if self.plugin is None:
            raise MajaPluginMuscateException(
                "Internal error: the variable self.plugin is NULL!"
            )
        if self.l1_plugin is None:
            self.l1_plugin = self.plugin
        if self.l1imageinformationsproviderbase is None:
            raise MajaPluginMuscateException(
                "Internal error: the variable m_L1ImageInformationsProviderBase is NULL!"
            )
            # 4.3
        self.pre_processing()

        # TODO normalmeent plus utile
        # file_utils.create_directory(
        #     "/".join(self.outputl2productfilename.split("/")[:-1])
        # )

        if self.apphandler.ChangeTargetResToR3:
            self.OutResR3 = True
        self.OutResR3 = self.plugin.ConfigUserCamera.get_Business().get_OutResR3()

        # ---------------------------------------------------------------------------------------------
        # Get the "share/config" directory

        root_template_directory = self.apphandler.get_share_config_directory()
        LOGGER.debug(
            "Root template install directory '" + root_template_directory + "'"
        )
        root_shemas_directory = self.apphandler.get_schemas_root_install_dir()
        LOGGER.debug("Root shemas install directory '" + root_shemas_directory + "'")
        lSchemaLocationDirectory = (
                root_shemas_directory + self.plugin.MAJA_INSTALL_SCHEMAS_L2_DIR
        )
        LOGGER.debug("Root install directory '" + lSchemaLocationDirectory + "'")
        # Reference of the L2 output product filename (coe from the L2IOmageFielWriter
        l_ReferenceOutputL2ProductFilename = self.outputl2productfilename
        LOGGER.debug(
            "ReferenceOutputL2ProductFilename: '"
            + l_ReferenceOutputL2ProductFilename
            + "'"
        )
        l_PublicDirectory = os.path.abspath(
            os.path.join(l_ReferenceOutputL2ProductFilename, os.pardir)
        )
        LOGGER.debug("l_PublicDirectory: '" + l_PublicDirectory + "'")
        # ---------------------------------------------------------------------------------------------
        LOGGER.debug("Start MuscateL2HeaderFileWriterProvider::Write() ...")

        LOGGER.debug("XML Header called: ")
        LOGGER.debug(self.plugin)
        LOGGER.debug(self.l1imageinformationsproviderbase.HeaderFilename)
        LOGGER.debug(self.plugin.PluginName)
        # ---------------------------------------------------------------------------------------------
        lCurrentDate = datetime.datetime.now()
        l_Mission = self.l1imageinformationsproviderbase.SatelliteID
        LOGGER.debug("l_Mission: " + l_Mission)
        # Try to load the necessary information before
        # Load the L1 header product test if muscate or EarthExplorer
        if self.l1imageinformationsproviderbase.PluginName != self.plugin.PluginName:
            self.l2imagefilenamesprovider = L2ImageFilenamesProvider()
            self.l2imagefilenamesprovider.initialize(
                self.l1imageinformationsproviderbase,
                self.plugin.ListOfL2Resolutions,
                self.outputdirectory,
                False,
            )  # Check existence

        l_ReferenceProductHeaderId = ""
        l_ReferenceProductInstance = ""
        l_Validity_Start = ""
        l_Validity_Stop = ""
        l_Acquisition_Date = ""
        l_Nick_Name = ""


        # Start write the global header file

        root_sc_xml_templates = (
                root_template_directory + self.plugin.TEMPLATE_GLOBAL_HDR
        )
        # print(root_sc_xml_templates)
        self.write_global_xml_handler(
            root_sc_xml_templates,
            l_ReferenceOutputL2ProductFilename,
            lSchemaLocationDirectory,
            l_Mission,
        )

    def write_global_xml_handler(
            self,
            root_sc_xml_templates,
            output_filename,
            schemaLocationDirectory,
            p_Mission,
    ):
        # ---------------------------------------------------------------------------------------------
        l_CurrentPluginBase = self.plugin
        # Get global information about the output L2 product

        l_BandsDefinitions = l_CurrentPluginBase.BandsDefinitions
        l_ListOfL2Res = l_BandsDefinitions.ListOfL2Resolution
        l_NumberOfResolutions = len(l_ListOfL2Res)
        l_Muscate = self.l1imageinformationsproviderbase.MuscateData
        l_l1info = self.l1imageinformationsproviderbase

        list_bands_to_write = (
            self.plugin.ConfigUserCamera.get_Business().get_WriteBands()
        )

        # ---------------------------------------------------------------------------------------------------
        LOGGER.debug("Write the GLOBAL header file ...")
        # print(l_CurrentPluginBase,
        #      l_BandsDefinitions,
        #      l_ListOfL2Res,
        #      l_NumberOfResolutions,
        #      l_Muscate,
        #      list_bands_to_write,
        #      root_sc_xml_templates)
        current_header_filename = root_sc_xml_templates
        if not os.path.exists(current_header_filename):
            raise MajaPluginMuscateException(
                "Internal error: the template file '"
                + current_header_filename
                + "' doesn't exist !!"
            )
        # Load the file
        output_handler = MuscateXMLFileHandler(current_header_filename)
        LOGGER.debug("output_filename: " + output_filename)
        # Set the main header information
        l_RootBaseFilename = os.path.basename(output_filename).replace(
            "_MTD_ALL.xml", ""
        )
        # PRODUCT_ID : LANDSAT5-TM-XS_20100118-103000-000_L2A_EU93066200A00B_C_V1-0
        if "Node_MetadataFormat" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_MetadataFormat"],
                "//METADATA_FORMAT",
                "//Metadata_Identification/METADATA_FORMAT",
            )
        output_handler.set_value_of("ProductId", l_RootBaseFilename)

        if "ZoneGeo" in l_Muscate:
            l_Site = l_Muscate["ZoneGeo"]
        else:
            l_Site = l_l1info.Site.strip("_")
            l_tileMatch = re.search("^[0-9]{2}[A-Z]{3}$", l_Site)
            if l_tileMatch is not None:
                l_Site = "T" + l_Site

        # Node Dataset_Identification
        l_identifier = l_l1info.get_l2_identifier()
        output_handler.set_value_of("Identifier", l_identifier)
        if len(l_l1info.Instrument):
            output_handler.set_value_of("Instrument", l_l1info.Instrument)
        else:
            output_handler.set_value_of("Instrument", self.plugin.Instrument)

        if len(l_l1info.DataAccess):
            output_handler.set_value_of("DataAccess", l_l1info.DataAccess)

        if len(l_l1info.SpectralContent):
            output_handler.set_value_of("SpectralContent", l_l1info.SpectralContent)

        if "Authority" in l_Muscate:
            output_handler.set_value_of("Authority", l_Muscate["Authority"])
        else:
            output_handler.set_value_of("Authority", "THEIA")

        if "Producer" in l_Muscate:
            output_handler.set_value_of("Producer", l_Muscate["Producer"])
        else:
            output_handler.set_value_of("Producer", "MUSCATE")

        if "Project" in l_Muscate:
            output_handler.set_value_of("Project", l_Muscate["Project"])
        else:
            output_handler.set_value_of("Project", "SENTINEL2")

        output_handler.set_value_of("ZoneGeo", l_Site)

        # Detect the type of site
        l_siteType = "K-J/Sat"

        xml_tools.set_attribute(
            output_handler.root, "//GEOGRAPHICAL_ZONE", "type", l_siteType
        )

        # if ORIGINAL_DATA_DIFFUSER is available
        if "Node_OriginalDataDiffuser" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_OriginalDataDiffuser"], "//ORIGINAL_DATA_DIFFUSER"
            )

        # Node Product_Characteristics
        l_sensingTime = (
                date_utils.get_utc_from_datetime(l_l1info.ProductDate)[4:]
                + "."
                + date_utils.get_date_millisecs_from_tm(l_l1info.ProductDate)
                + "Z"
        )
        if "AcquisitionDate" in l_Muscate:
            output_handler.set_value_of("AcquisitionDate", l_Muscate["AcquisitionDate"])
        else:
            output_handler.set_value_of("AcquisitionDate", l_sensingTime)

        output_handler.set_value_of("ProductionDate", l_l1info.GenerationDateStr)

        output_handler.set_value_of("ProductVersion", l_l1info.ProductVersion)

        output_handler.set_value_of(
            "ProductionSoftware", "MAJA " + version.MAJA_VERSION
        )

        output_handler.set_value_of("Platform", p_Mission)

        if "UTCAcquisitionRangeMean" in l_Muscate:
            output_handler.set_value_of(
                "UTCAcquisitionRangeMean", l_Muscate["UTCAcquisitionRangeMean"]
            )
        else:
            output_handler.set_value_of("UTCAcquisitionRangeMean", l_sensingTime)

        if "UTCAcquisitionRangeDatePrecision" in l_Muscate:
            output_handler.set_value_of(
                "UTCAcquisitionRangeDatePrecision",
                l_Muscate["UTCAcquisitionRangeDatePrecision"],
            )
        else:
            output_handler.set_value_of("UTCAcquisitionRangeDatePrecision", "0.001")

        output_handler.set_value_of("OrbitNumber", l_l1info.OrbitNumber)

        # Writes the filename
        # QUICKLOOK
        xml_tools.set_attribute(
            output_handler.root,
            "//Muscate_Product/QUICKLOOK",
            "bands_id",
            self.quicklookredbandcode
            + ","
            + self.quicklookgreenbandcode
            + ","
            + self.quicklookbluebandcode,
        )

        output_handler.set_string_value(
            "//Muscate_Product/QUICKLOOK", l_RootBaseFilename + "_QKL_ALL.jpg"
        )

        #        If not FRE, remove this node
        #        Else rename and set the values
        #        Image_MACC_template_temporary_node_Flat_Reflectance
        # MACCS 4.7.2 - correction pour FA 1572
        # MACCS 4.7.4 : LAIG-FA-MAC-1623-CNES : write always PDV directory
        l_PublicDirectory = os.path.abspath(os.path.join(output_filename, os.pardir))
        LOGGER.debug("Output public directory : " + l_PublicDirectory)
        LOGGER.debug("No private directory for SPOT")

        # Resolution information
        l_grpSuffixes = l_ListOfL2Res
        if len(l_grpSuffixes) == 1 and not self.OutResR3:
            l_grpSuffixes = ["XS"]
        # ---------------------------------------------------------------------------------------------
        # . Test de la presence du champs  'Useful_Image'
        if "Node_Useful_Image" in l_Muscate:
            LOGGER.debug(
                "The L1 product have 'Useful_Image' files. They are copied in the L2 out product..."
            )
            xml_tools.copies_to_child(
                l_Muscate["Node_Useful_Image"],
                "Mask",
                output_handler.root,
                "//Mask_List",
            )
            for l_grpSuffix in l_grpSuffixes:
                l_XPathRoot_In = (
                        "//Mask_List/Mask[Mask_Properties/NATURE='Useful_Image']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']"
                )
                xnode_in = xml_tools.get_only_value(output_handler.root, l_XPathRoot_In)
                l_L2OutputMaskDirectoryFilename = os.path.join(
                    l_PublicDirectory, "MASKS"
                )
                file_utils.create_directory(l_L2OutputMaskDirectoryFilename)
                # Get the path in the xml product filename
                lPath = os.path.dirname(l_l1info.ProductFileName)
                l_FullPathFilename = os.path.join(lPath, xnode_in.text)
                LOGGER.debug("l_FullPathFilename: " + l_FullPathFilename)
                l_OutputFilename = l_RootBaseFilename + "_USI_" + l_grpSuffix + ".tif"
                xnode_in.text = os.path.join("MASKS", l_OutputFilename)
                # Copy/resampling done by MuscateL2ImageWriter
        else:
            LOGGER.debug("No 'Useful_Image' mask detected in the L1 product.")
        # Fin si manage USI

        if self.writel2products:
            # Resolution information
            for resol in range(l_NumberOfResolutions):
                l_grpSuffix = l_grpSuffixes[resol]
                l_StrResolution = l_BandsDefinitions.ListOfL2Resolution[resol]
                l_ListOfBand = l_BandsDefinitions.get_list_of_l2_band_code(
                    l_StrResolution
                )
                if self.OutResR3:
                    l_grpSuffix = "R3"
                l_NumberOfBands = len(l_ListOfBand)
                if list_bands_to_write is not None:
                    for i in range(l_NumberOfBands):
                        if l_ListOfBand[i] not in list_bands_to_write:
                            output_handler.remove_node(
                                "//Band_Global_List/BAND_ID[text()='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                            output_handler.remove_node(
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List/BAND_ID[text()='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                            attr = xml_tools.get_attribute(
                                output_handler.root, "//Band_Global_List", "count"
                            )
                            xml_tools.set_attribute(
                                output_handler.root,
                                "//Band_Global_List",
                                "count",
                                str(int(attr) - 1),
                            )
                            attr = xml_tools.get_attribute(
                                output_handler.root,
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List",
                                "count",
                            )
                            xml_tools.set_attribute(
                                output_handler.root,
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List",
                                "count",
                                str(int(attr) - 1),
                            )

                for i in range(l_NumberOfBands):
                    if (
                            list_bands_to_write is None
                            or l_ListOfBand[i] in list_bands_to_write
                    ):
                        output_handler.set_string_value(
                            "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            l_RootBaseFilename + "_SRE_" + l_ListOfBand[i] + ".tif",
                        )
                    else:
                        output_handler.remove_node(
                            "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']"
                        )
                if self.adjacencyeffectsandslopecorrection:
                    for i in range(l_NumberOfBands):
                        if (
                                list_bands_to_write is None
                                or l_ListOfBand[i] in list_bands_to_write
                        ):
                            output_handler.set_string_value(
                                "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                                + l_ListOfBand[i]
                                + "']",
                                l_RootBaseFilename + "_FRE_" + l_ListOfBand[i] + ".tif",
                            )
                        else:
                            output_handler.remove_node(
                                "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                for i in range(l_NumberOfBands):
                    if (
                            list_bands_to_write is None
                            or l_ListOfBand[i] in list_bands_to_write
                    ):
                        output_handler.set_string_value(
                            "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            l_RootBaseFilename + "_SRE_" + l_ListOfBand[i] + ".tif",
                        )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Water_Vapor_Content']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    l_RootBaseFilename + "_ATB_" + l_grpSuffix + ".tif",
                )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    l_RootBaseFilename + "_ATB_" + l_grpSuffix + ".tif",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write WVC_Interpolation
                # Only for driver where WaterVapourDetermination is activated
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='WVC_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_IAB_" + l_grpSuffix + ".tif",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write AOT_Interpolation
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='AOT_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_IAB_" + l_grpSuffix + ".tif",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Detailed Cloud
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Detailed_Cloud']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_CLM_" + l_grpSuffix + ".tif",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Edge
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Edge']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_EDG_" + l_grpSuffix + ".tif",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Saturation
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Saturation']/Mask_File_List/MASK_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_SAT_" + l_grpSuffix + ".tif",
                    )
                l1_handler = MuscateXMLFileHandler(self.l1imageinformationsproviderbase.HeaderFilename)
                LOGGER.debug("L1 handler:"+str(l1_handler.get_l1_cld_filename(l_grpSuffix)))
                if l1_handler.get_l1_cld_filename("XS") is not None:
                    LOGGER.debug("MG1 mask detected")
                    # ---------------------------------------------------------------------------------------------------
                    # Write Water
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Water']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + ".tif",
                    )
                    # ---------------------------------------------------------------------------------------------------
                    # Write Cloud
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Cloud']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + ".tif",
                    )
                    # ---------------------------------------------------------------------------------------------------
                    # Write Snow
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + ".tif",
                    )
                else:
                    LOGGER.debug("No MG1 mask detected, remove node on header")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Water']")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Cloud']")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']")

                # ---------------------------------------------------------------------------------------------------
                # Write DFP
                l_DFPMaskAvailable = self.l1_plugin.DFPMasking
                if (
                        l_DFPMaskAvailable
                        and self.plugin.ConfigUserCamera.get_Business().get_WriteDFP()
                ):
                    for i in range(l_NumberOfBands):
                        output_handler.set_string_value(
                            "//Mask_List/Mask[Mask_Properties/NATURE='Defective_Pixel']/Mask_File_List/MASK_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            "MASKS/"
                            + l_RootBaseFilename
                            + "_DFP_"
                            + l_grpSuffix
                            + ".tif",
                        )
                else:
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Defective_Pixel']"
                    )
                # ---------------------------------------------------------------------------------------------
                # LAIG-DM-MAC-2003-CNES : recopie des fichiers detfoot print
                # Uniquement pour Sentinel2 Muscate
                # . Test de la presence du champs  'Detector_Footprint'
                if (
                        "ZoneMaskFileNames" in l_Muscate
                        and self.plugin.ConfigUserCamera.get_Business().get_WriteDTF()
                ):
                    LOGGER.debug(
                        "The L1 product have 'Detector_Footprint' masks. There are copied in the L2 out product..."
                    )
                    # Write the Detfoo in the Xml file, in the MASK Detector_Footprint node !
                    l_XPathRootDetFoot = "//Mask_List/Mask[Mask_Properties/NATURE='Detector_Footprint']/Mask_File_List"
                    xnode = xml_tools.get_only_value(
                        output_handler.root, l_XPathRootDetFoot
                    )
                    main_node = xnode
                    l_L2OutputMaskDirectoryFilename = os.path.join(
                        l_PublicDirectory, "MASKS"
                    )
                    file_utils.create_directory(l_L2OutputMaskDirectoryFilename)
                    for i in range(l_NumberOfBands):
                        l1BandId = l_BandsDefinitions.get_band_id_in_l1(l_ListOfBand[i])
                        l_MapBandDetFnames = l_Muscate["ZoneMaskFileNames"][l1BandId]
                        for det, filename in l_MapBandDetFnames.items():
                            l_FullPathFilename = filename
                            LOGGER.debug("l_FullPathFilename: " + l_FullPathFilename)
                            l_ShortFileName = os.path.basename(l_FullPathFilename)
                            LOGGER.debug("l_ShortFileName: " + l_ShortFileName)
                            # Write inthe XML file
                            # attributes: band_id="B1" detector_id="01"
                            elt = ET.Element("MASK_FILE")
                            elt.set("band_id", l_ListOfBand[i])
                            elt.set("detector_id", det)
                            oss = str(i + 1)
                            elt.set("bit_number", oss)
                            if l_l1info.new_format_detected == True:
                                l_l2DTFFilename = l_ShortFileName.replace(
                                    "_L1C_", "_L2A_"
                                )
                                l_OldName = "{}.tif".format(l_ListOfBand[i])
                                l_NewName = "{}-D{}.tif".format(l_StrResolution, det)
                                l_DTFFilenameWithResol = l_l2DTFFilename.replace(
                                    l_OldName, l_NewName
                                )
                            else:
                                l_DTFFilenameWithResol = l_ShortFileName.replace(
                                    "_L1C_", "_L2A_"
                                )
                            elt.text = os.path.join("MASKS", l_DTFFilenameWithResol)
                            main_node.append(elt)
                            # if we are not in the case of sentinel 2 with new format (PSD > 14.8)
                            # we copy the L1C DTF to the output L2 product
                            if l_l1info.new_format_detected == False:
                                l_OutputFilename = l_ShortFileName.replace(
                                    "_L1C_", "_L2A_"
                                )
                                file_utils.copy_file(
                                    l_FullPathFilename,
                                    os.path.join(
                                        l_L2OutputMaskDirectoryFilename,
                                        l_OutputFilename,
                                    ),
                                )

                else:
                    LOGGER.debug(
                        "No 'Detector_Footprint' masks detected in the L1 product."
                    )
                    # Remove the node
                    xml_tools.remove_node(
                        output_handler.root,
                        "//Mask_List/Mask[Mask_Properties/NATURE='Detector_Footprint']",
                    )

                # Fin si manage Detfoo

                # ---------------------------------------------------------------------------------------------
                # . Test de la presence du champs  'Aberrant_Pixels'
                if ("PIXImages" in l_Muscate) and ("PIXIndices" in l_Muscate):
                    LOGGER.debug(
                        "The L1 product have 'Aberrant_Pixels' masks. There are copied in the L2 out product..."
                    )
                    # Write the PIX in the Xml file, in the MASK Aberrant_Pixels node !
                    l_XPathRootPIX = "//Mask_List/Mask[Mask_Properties/NATURE='Aberrant_Pixels']/Mask_File_List"
                    xnode = xml_tools.get_only_value(
                        output_handler.root, l_XPathRootPIX
                    )
                    main_node = xnode
                    l_L2OutputMaskDirectoryFilename = os.path.join(
                        l_PublicDirectory, "MASKS"
                    )
                    file_utils.create_directory(l_L2OutputMaskDirectoryFilename)
                    for i in range(l_NumberOfBands):
                        l1BandId = l_BandsDefinitions.get_band_id_in_l1(l_ListOfBand[i])
                        l_FullPathFilename = l_Muscate["PIXImages"][l1BandId]
                        LOGGER.debug("l_FullPathFilename: " + l_FullPathFilename)
                        l_ShortFileName = os.path.basename(l_FullPathFilename)
                        LOGGER.debug("l_ShortFileName: " + l_ShortFileName)
                        # Write inthe XML file
                        # attributes: band_id="B1" "bit_number"
                        elt = ET.Element("MASK_FILE")
                        elt.set("band_id", l_ListOfBand[i])
                        oss = l_Muscate["PIXIndices"][l1BandId]
                        elt.set("bit_number", str(oss))
                        l_OutputFilename = l_ShortFileName.replace("_L1C_", "_L2A_")
                        elt.text = os.path.join("MASKS", l_OutputFilename)
                        main_node.append(elt)
                else:
                    LOGGER.debug(
                        "No 'Aberrant_Pixels' masks detected in the L1 product."
                    )
                    # remove node if present
                    xml_tools.remove_node(
                        output_handler.root,
                        "//Mask_List/Mask[Mask_Properties/NATURE='Aberrant_Pixels']",
                    )

                # Fin si manage PIX
        else:
            # Generating default 1000x1000px black quicklook
            root_template_directory = self.apphandler.get_share_config_directory()
            LOGGER.debug(
                "QCK template '"
                + root_template_directory
                + self.plugin.TEMPLATE_PDTQKL_JPG
                + "'"
            )
            LOGGER.debug(
                "QCK dst fname '"
                + l_PublicDirectory
                + "/"
                + l_RootBaseFilename
                + "_QKL_ALL.jpg'"
            )
            file_utils.copy_file(
                root_template_directory + self.plugin.TEMPLATE_PDTQKL_JPG,
                l_PublicDirectory + "/" + l_RootBaseFilename + "_QKL_ALL.jpg",
            )
            # Resolution information
            for resol in range(l_NumberOfResolutions):
                l_grpSuffix = l_grpSuffixes[resol]
                l_StrResolution = l_BandsDefinitions.ListOfL2Resolution[resol]
                l_ListOfBand = l_BandsDefinitions.get_list_of_l2_band_code(
                    l_StrResolution
                )
                l_NumberOfBands = len(l_ListOfBand)
                if self.OutResR3:
                    l_grpSuffix = "R3"
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "",
                    )
                if self.adjacencyeffectsandslopecorrection:
                    for i in range(l_NumberOfBands):
                        output_handler.set_string_value(
                            "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            "",
                        )
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "",
                    )

                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Water_Vapor_Content']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']/Image_File_List/"
                    + "IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # Write Cloud
                l_Nature = "Cloud"
                # Get the parent node
                l_CLDDataBandsSelected = self.plugin.CLDDataBandsSelected
                l_NbCLDDataBandsSelected = len(l_CLDDataBandsSelected)
                xnode = xml_tools.get_only_value(
                    output_handler.root,
                    "//Mask_List/Mask[Mask_Properties/NATURE='"
                    + l_Nature
                    + "']/Mask_File_List",
                )
                main_node = xnode
                for i in l_CLDDataBandsSelected:
                    l_Group = l_grpSuffix
                    elt = ET.Element("MASK_FILE")
                    elt.set("group_id", l_Group)
                    elt.text = ""
                    main_node.append(elt)

                # Write Water
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Water']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Hidden_Surface
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Hidden_Surface']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Topography_Shadow
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Topography_Shadow']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Sun_Too_Low
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Sun_Too_Low']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Tangent_Sun
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Tangent_Sun']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Snow
                if self.l1_plugin.SnowMasking:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------------

                # Write Saturation
                l_Nature = "Saturation"
                for b in l_ListOfBand:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='"
                        + l_Nature
                        + "']/Mask_File_List/MASK_FILE[@band_id='"
                        + b
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------------
                # Write Edge
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Edge']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write AOT_Interpolation
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='AOT_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write WVC_Interpolation
                # Only for driver where WaterVapourDetermination is activated
                if self.l1_plugin.WaterVapourDetermination:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='WVC_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------
                if "ZoneMaskFileNames" in l_Muscate:
                    LOGGER.debug(
                        "The L1 product have 'Detector_Footprint' masks. There are copied in the L2 out product..."
                    )
                    # Write the Detfoo in the Xml file, in the MASK Detector_Footprint node !
                    l_XPathRootDetFoot = "//Mask_List/Mask[Mask_Properties/NATURE='Detector_Footprint']/Mask_File_List"
                    xnode = xml_tools.get_only_value(
                        output_handler.root, l_XPathRootDetFoot
                    )
                    main_node = xnode

                    for bd in range(l_NumberOfBands):

                        l1BandId = l_BandsDefinitions.get_band_id_in_l1(
                            l_ListOfBand[bd]
                        )
                        l_MapBandDetFnames = l_Muscate["ZoneMaskFileNames"][l1BandId]
                        for it in list(l_MapBandDetFnames.values()):
                            # Write inthe XML file
                            elt = ET.Element("MASK_FILE")
                            elt.set("band_id", l_ListOfBand[bd])
                            elt.set("detector_id", it)
                            elt.text = ""
                            main_node.append(elt)

                # Fin si manage Detfoo

        if self.adjacencyeffectsandslopecorrection:
            LOGGER.debug(
                "Renamed child Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance by Image"
            )
            xml_tools.rename_node(
                output_handler.root,
                "//Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance",
                "Image",
            )
        else:
            LOGGER.debug(
                "Removed Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance child"
            )
            xml_tools.remove_node(
                output_handler.root,
                "//Product_Organisation/Muscate_Product/Image_List/"
                + "Image_MACC_template_temporary_node_Flat_Reflectance",
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteSRE():
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Surface_Reflectance']"
            )
        if not self.plugin.ConfigUserCamera.get_Business().get_WriteATB():
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']"
            )
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Water_Vapor_Content']"
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteIAB():
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='WVC_Interpolation']"
            )
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='AOT_Interpolation']"
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteEDG():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Edge']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteSAT():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Saturation']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteQLK():
            output_handler.remove_node("//QUICKLOOK")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteMG2():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Water']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Cloud']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Snow']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Cloud_Shadow']")
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Topography_Shadow']"
            )
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Hidden_Surface']"
            )
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Sun_Too_Low']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Tangent_Sun']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteCLM():
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Detailed_Cloud']"
            )

        if not self.writel2products:
            # DM2706
            xml_tools.remove_node(
                output_handler.root, "//Product_Organisation/Muscate_Product/Image_List"
            )
            xml_tools.remove_node(
                output_handler.root, "//Product_Organisation/Muscate_Product/Mask_List"
            )

        # ----------------------------------------------------------------------
        # Compute Geoposition_Informations
        l_Areas = self.dem.L2Areas
        proj_full = self.dem.ProjCode
        proj_table = proj_full
        proj_code = proj_full
        if proj_full.find(":") != -1:
            proj_table = proj_full[: proj_full.find(":")]
            proj_code = proj_full[proj_full.find(":") + 1:]

        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/GEO_TABLES",
            proj_table,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_TYPE",
            self.dem.ProjType,
        )
        proj_name = self.dem.ProjRef.split('"')[1]
        if "WGS84" in proj_name:
            proj_name = "WGS 84" + proj_name[proj_name.find("WGS84") + 5:]

        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_NAME",
            proj_name,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_CODE",
            proj_code,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Raster_CS/RASTER_CS_TYPE", "CELL"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Raster_CS/PIXEL_ORIGIN", "0"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Metadata_CS/METADATA_CS_TYPE", "CELL"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Metadata_CS/PIXEL_ORIGIN", "0"
        )

        l_isCarto = self.dem.ProjType == "PROJECTED"

        # Corners
        l_spacing = PointXY(l_Areas[0].spacing[0], l_Areas[0].spacing[1])
        l_origin = PointXY(l_Areas[0].origin[0], l_Areas[0].origin[1])
        l_ul = l_origin - 0.5 * l_spacing
        l_vecX = PointXY(l_Areas[0].size[0] * l_Areas[0].spacing[0], 0.0)
        l_vecY = PointXY(0.0, l_Areas[0].size[1] * l_Areas[0].spacing[1])
        l_corners = {}
        l_corners["upperLeft"] = l_ul
        l_corners["upperRight"] = l_ul + l_vecX
        l_corners["lowerRight"] = l_ul + l_vecX + l_vecY
        l_corners["lowerLeft"] = l_ul + l_vecY
        l_corners["center"] = l_ul + 0.5 * (l_vecX + l_vecY)

        l_geopositionPath = "//Geoposition_Informations/Geopositioning/Global_Geopositioning/Point[@name='{}']/{}"
        if l_isCarto:
            LOGGER.debug("Start Conversion ToWKT with the EPSG '" + proj_code + "'...")
            param_conv = {
                "carto.x": 0.0,
                "carto.y": 0.0,
                "mapproj": "epsg",
                "mapproj.epsg.code": int(proj_code),
            }
            for corner, point in l_corners.items():
                # Compute lon lat coordinates of the corner
                param_conv["carto.x"] = point.x
                param_conv["carto.y"] = point.y
                conv_app = OtbAppHandler("ConvertCartoToGeoPoint", param_conv)
                l_lon = conv_app.getoutput().get("long")
                l_lat = conv_app.getoutput().get("lat")
                # Set corner
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LAT"), f"{l_lat:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LON"), f"{l_lon:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "X"), f"{point.x:.1f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "Y"), f"{point.y:.1f}"
                )
        else:
            for corner, point in l_corners.items():
                # Set corner
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LAT"), f"{point.y:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LON"), f"{point.x:.12f}"
                )
                output_handler.remove_node(l_geopositionPath.format(corner, "X"))
                output_handler.remove_node(l_geopositionPath.format(corner, "Y"))

        # Set the XS geopositioning
        l_groupGeopositioningPath = "//Geoposition_Informations/Geopositioning/Group_Geopositioning_List/Group_Geopositioning[@group_id='{}']/{}"
        for idx, res in enumerate(l_grpSuffixes):
            if self.OutResR3:
                res = "R3"
                idx = 2
            l_spacing = PointXY(l_Areas[idx].spacing[0], l_Areas[idx].spacing[1])
            l_ul = (
                    PointXY(l_Areas[idx].origin[0], l_Areas[idx].origin[1])
                    - 0.5 * l_spacing
            )
            if l_isCarto:
                l_str_ulx = f"{l_ul.x:.1f}"
                l_str_uly = f"{l_ul.y:.1f}"
                l_str_xdim = f"{l_spacing.x:.0f}"
                l_str_ydim = f"{l_spacing.y:.0f}"
            else:
                l_str_ulx = f"{l_ul.x:.12f}"
                l_str_uly = f"{l_ul.y:.12f}"
                l_str_xdim = f"{l_spacing.x:.12f}"
                l_str_ydim = f"{l_spacing.y:.12f}"
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "ULX"), l_str_ulx
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "ULY"), l_str_uly
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "XDIM"), l_str_xdim
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "YDIM"), l_str_ydim
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "NROWS"), l_Areas[idx].size[1]
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "NCOLS"), l_Areas[idx].size[0]
            )

        #  ----------------------------------------------------------------------
        if "Node_Geometric_Informations" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_Geometric_Informations"], "//Geometric_Informations"
            )
        else:
            # Fill geometric info node
            l_msza = float(l_l1info.SolarAngle["sun_zenith_angle"])
            l_msaa = float(l_l1info.SolarAngle["sun_azimuth_angle"])
            l_msza = f"{l_msza:.12f}"
            l_msaa = f"{l_msaa:.12f}"
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Sun_Angles/ZENITH_ANGLE",
                l_msza,
            )
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Sun_Angles/AZIMUTH_ANGLE",
                l_msaa,
            )

            l_mvza = float(l_l1info.ViewingAngle["incidence_zenith_angle"])
            l_mvaa = float(l_l1info.ViewingAngle["incidence_azimuth_angle"])
            l_mvza = f"{l_mvza:.12f}"
            l_mvaa = f"{l_mvaa:.12f}"
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Incidence_Angles/ZENITH_ANGLE",
                l_mvza,
            )
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Incidence_Angles/AZIMUTH_ANGLE",
                l_mvaa,
            )

            # Fill Mean viewing angle list
            # Get one mean viewing angle node to copy it
            l_InvBandMap = {v: k for k, v in l_BandsDefinitions.L2CoarseBandMap.items()}
            l_MeanViewingAngles = (
                l_l1info.ListOfViewingAnglesPerBandAtL2CoarseResolution
            )
            l_PathMeanViewingAngles = "//Geometric_Informations/Mean_Value_List/Mean_Viewing_Incidence_Angle_List/Mean_Viewing_Incidence_Angle[@{}='{}']/{}"
            for bname, b in l_BandsDefinitions.L2CoarseBandMap.items():
                l_mvza = float(l_MeanViewingAngles[b]["incidence_zenith_angle"])
                l_mvaa = float(l_MeanViewingAngles[b]["incidence_azimuth_angle"])

                output_handler.set_string_value(
                    l_PathMeanViewingAngles.format("band_id", bname, "ZENITH_ANGLE"),
                    f"{l_mvza:.12f}",
                )
                output_handler.set_string_value(
                    l_PathMeanViewingAngles.format("band_id", bname, "AZIMUTH_ANGLE"),
                    f"{l_mvaa:.12f}",
                )

                # also set per-detector viewing angles (if present)
                if len(l_BandsDefinitions.DetectorMap):
                    det = str(l_BandsDefinitions.DetectorMap[b])
                    if len(det) < 2:
                        det = "0" + det
                    output_handler.set_string_value(
                        l_PathMeanViewingAngles.format(
                            "detector_id", det, "ZENITH_ANGLE"
                        ),
                        f"{l_mvza:.12f}",
                    )
                    output_handler.set_string_value(
                        l_PathMeanViewingAngles.format(
                            "detector_id", det, "AZIMUTH_ANGLE"
                        ),
                        f"{l_mvaa:.12f}",
                    )

            # Fill Sun angle grids
            if len(l_l1info.SolarAngleGrid["Azimuth"]):
                l_PathSunAnglesGrid = (
                    "//Geometric_Informations/Angles_Grids_List/Sun_Angles_Grids/"
                )
                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Azimuth/COL_STEP",
                    l_l1info.SolarAngleGrid["ColStep"],
                )
                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Azimuth/ROW_STEP",
                    l_l1info.SolarAngleGrid["RowStep"],
                )
                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Zenith/COL_STEP",
                    l_l1info.SolarAngleGrid["ColStep"],
                )
                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Zenith/ROW_STEP",
                    l_l1info.SolarAngleGrid["RowStep"],
                )
                xml_tools.set_attribute(
                    output_handler.root,
                    l_PathSunAnglesGrid + "Azimuth",
                    "step_unit",
                    l_l1info.SolarAngleGrid["StepUnit"],
                )
                xml_tools.set_attribute(
                    output_handler.root,
                    l_PathSunAnglesGrid + "Zenith",
                    "step_unit",
                    l_l1info.SolarAngleGrid["StepUnit"],
                )

                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Azimuth/Values_List", ""
                )
                output_handler.set_string_value(
                    l_PathSunAnglesGrid + "Zenith/Values_List", ""
                )

                for line in l_l1info.SolarAngleGrid["Azimuth"]:
                    xml_tools.add_child(
                        output_handler.root,
                        l_PathSunAnglesGrid + "Azimuth/Values_List",
                        "VALUES",
                        line,
                    )
                for line in l_l1info.SolarAngleGrid["Zenith"]:
                    xml_tools.add_child(
                        output_handler.root,
                        l_PathSunAnglesGrid + "Zenith/Values_List",
                        "VALUES",
                        line,
                    )

            # Fill Viewing incidence angle grids
            l_templateBaseNode = xml_tools.get_only_value(
                output_handler.root, "//Viewing_Incidence_Angles_Grids_List", check=True
            )
            for grid in l_l1info.ViewingAngleGrids:
                if l_templateBaseNode is None:
                    raise MajaPluginMuscateException(
                        "Missing node 'Viewing_Incidence_Angles_Grids_List' in dom"
                    )
                if "Band" in grid:
                    l_idxBand = int(grid["Band"])
                    l_bname = l_InvBandMap[l_idxBand]
                    if not l_idxBand == l_BandsDefinitions.L2CoarseBandMap[l_bname]:
                        raise MajaPluginMuscateException(
                            "Unexpected band order! {} is not at index {}".format(
                                l_bname, l_idxBand
                            )
                        )
                    l_band_selector = "[@band_id='{}']".format(l_bname)
                else:
                    l_band_selector = ""
                    l_bname = ""
                l_strDet = grid["Detector"]
                l_str2Det = l_strDet
                if int(l_strDet) < 10:
                    l_str2Det = "0" + l_strDet
                # get the node for band 'l_bname' (or create one)
                l_templateBandNode = xml_tools.get_only_value(
                    l_templateBaseNode,
                    "Band_Viewing_Incidence_Angles_Grids_List" + l_band_selector,
                    check=True,
                )
                if l_templateBandNode is None:
                    l_templateBandNode = ET.Element(
                        "Band_Viewing_Incidence_Angles_Grids_List"
                    )
                    if len(l_bname):
                        l_templateBandNode.set("band_id", l_bname)
                    l_templateBaseNode.append(l_templateBandNode)
                # get the node for band 'l_bname' and detector 'l_str2Det' (or create one)
                l_templateDetNode = xml_tools.get_only_value(
                    l_templateBandNode,
                    "Viewing_Incidence_Angles_Grids[@detector_id='{}']".format(
                        l_str2Det
                    ),
                    check=True,
                )
                if l_templateDetNode is None:
                    l_templateDetNode = ET.Element("Viewing_Incidence_Angles_Grids")
                    l_templateDetNode.set("detector_id", l_str2Det)
                    l_templateBandNode.append(l_templateDetNode)
                # Fill Zenith
                l_templateZenithNode = ET.Element("Zenith")
                l_templateZenithNode.set("step_unit", grid["StepUnit"])
                l_templateZenithNode.set("values_unit", "deg")
                l_templateZenithColNode = ET.Element("COL_STEP")
                l_templateZenithColNode.text = grid["ColStep"]
                l_templateZenithRowNode = ET.Element("ROW_STEP")
                l_templateZenithRowNode.text = grid["RowStep"]
                l_ZenithNode = ET.Element("Values_List")
                for line in grid["Zenith"]:
                    xml_tools.add_child(l_ZenithNode, ".", "VALUES", line)
                l_templateZenithNode.append(l_templateZenithColNode)
                l_templateZenithNode.append(l_templateZenithRowNode)
                l_templateZenithNode.append(l_ZenithNode)
                l_templateDetNode.append(l_templateZenithNode)
                # Fill Azimuth
                l_templateAzimuthNode = ET.Element("Azimuth")
                l_templateAzimuthNode.set("step_unit", grid["StepUnit"])
                l_templateAzimuthNode.set("values_unit", "deg")
                l_templateAzimuthColNode = ET.Element("COL_STEP")
                l_templateAzimuthColNode.text = grid["ColStep"]
                l_templateAzimuthRowNode = ET.Element("ROW_STEP")
                l_templateAzimuthRowNode.text = grid["RowStep"]
                l_AzimuthNode = ET.Element("Values_List")
                for line in grid["Azimuth"]:
                    xml_tools.add_child(l_AzimuthNode, ".", "VALUES", line)
                l_templateAzimuthNode.append(l_templateAzimuthColNode)
                l_templateAzimuthNode.append(l_templateAzimuthRowNode)
                l_templateAzimuthNode.append(l_AzimuthNode)
                l_templateDetNode.append(l_templateAzimuthNode)

        LOGGER.debug(
            xml_tools.get_only_value(output_handler.root, "//Geometric_Informations/Image_Refining", check=True, ))
        if (xml_tools.get_only_value(output_handler.root, "//Geometric_Informations/Image_Refining",
                                     check=True, ) is not None):
            output_handler.remove_node("//Geometric_Informations/Image_Refining")

        xml_tools.add_child(
            output_handler.root,
            "//Geometric_Informations",
            "Image_Refining",
            "",
        )

        if "RefiningStatus" in l_Muscate:
            xml_tools.set_attribute(
                output_handler.root,
                "//Geometric_Informations/Image_Refining",
                "flag",
                l_Muscate["RefiningStatus"])
        else:

            xml_tools.set_attribute(
                output_handler.root,
                "//Geometric_Informations/Image_Refining",
                "flag",
                l_l1info.RefiningStatus,
            )

        # Replace Radiometric_Informations and patch dedicated values
        # (REFLECTANCE_QUANTIFICATION_VALUE, Special_Values_List)

        # ATENTION self.GetReflectanceQuantificationValue() is 0.001 (not 1000)
        # The Quantification valude node is 1000

        output_handler.set_value_of(
            "QuantificationValue", int(1.0 / float(self.reflectancequantificationvalue))
        )
        output_handler.set_value_of(
            "AerosolOpticalThicknessQuantificationValue",
            int(1.0 / float(self.aotquantificationvalue)),
        )
        output_handler.set_value_of(
            "WaterVaporContentQuantificationValue",
            int(1.0 / float(self.vapquantificationvalue)),
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='nodata']",
            self.nodatavalue,
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='water_vapor_content_nodata']",
            self.vapnodatavalue,
        )

        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='water_vapor_content_nodata']",
            self.vapnodatavalue,
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='aerosol_optical_thickness_nodata']",
            self.aotnodatavalue,
        )

        # Spectral band informations
        if "Node_Spectral_Band_Informations_List" in l_Muscate:
            # Copy this node Spectral_Band_Informations_List
            output_handler.replace_node(
                l_Muscate["Node_Spectral_Band_Informations_List"],
                "Spectral_Band_Informations_List",
                "//Radiometric_Informations/Spectral_Band_Informations_List",
            )
        else:
            l_l1bands = list(l_BandsDefinitions.L2CoarseBandMap.keys())
            for spec in l_l1info.SpectralInfo:
                l_b = spec["Band"]
                if l_b in l_l1bands:
                    l_l1bands.remove(l_b)
                else:
                    raise MajaPluginMuscateException("Unexpected band id: " + str(l_b))
                l_baseXPath = "//Spectral_Band_Informations_List/Spectral_Band_Informations[@band_id='{}']".format(
                    l_b
                )
                if self.OutResR3 and l_b in ["B1", "B9", "B10"]:
                    continue
                # Native coefficients
                for measure in [
                    "PhysicalGain",
                    "LuminanceMax",
                    "LuminanceMin",
                    "QuantizeCalMax",
                    "QuantizeCalMin",
                    "RadianceAdd",
                    "RadianceMult",
                    "ReflectanceAdd",
                    "ReflectanceMult",
                    "Ak",
                    "PolarizationCoefficient",
                ]:
                    if measure in spec:
                        if isinstance(spec[measure], float):
                            value = f"{spec[measure]:.12f}"
                        else:
                            value = spec[measure]
                        output_handler.set_string_value(
                            l_baseXPath
                            + "/Calibration_Coefficients_Lists/Native_Coefficients_List/COEFFICIENT[@name='{}']".format(
                                measure
                            ),
                            value,
                        )
                # Solar irradiance
                if "SolarIrradiance" in spec:
                    output_handler.set_string_value(
                        l_baseXPath + "/SOLAR_IRRADIANCE", spec["SolarIrradiance"]
                    )
                # Spatial resolution
                output_handler.set_string_value(
                    l_baseXPath + "/SPATIAL_RESOLUTION",
                    l_BandsDefinitions.get_l1_resolution(
                        l_BandsDefinitions.get_l1_resolution_for_band_code(l_b)
                    ),
                )
                # Wavelength section
                if (
                        ("WavelengthMin" in spec)
                        or ("WavelengthMax" in spec)
                        or ("WavelengthCentral" in spec)
                ):
                    node_wavelength = xml_tools.get_only_value(
                        output_handler.root, l_baseXPath + "/Wavelength"
                    )
                    if "WavelengthMin" in spec:
                        node_min = ET.Element("MIN")
                        node_min.text = spec["WavelengthMin"]
                        node_min.set("unit", "nm")
                        node_wavelength.append(node_min)
                    if "WavelengthMax" in spec:
                        node_max = ET.Element("MAX")
                        node_max.text = spec["WavelengthMax"]
                        node_max.set("unit", "nm")
                        node_wavelength.append(node_max)
                    if "WavelengthCentral" in spec:
                        node_cen = ET.Element("CENTRAL")
                        node_cen.text = spec["WavelengthCentral"]
                        node_cen.set("unit", "nm")
                        node_wavelength.append(node_cen)
                # Spectral response section
                if ("ResponseStep" in spec) and ("ResponseValues" in spec):
                    node_response = xml_tools.get_only_value(
                        output_handler.root, l_baseXPath + "/Spectral_Response"
                    )
                    node_step = ET.Element("STEP")
                    node_step.text = spec["ResponseStep"]
                    node_step.set("unit", "nm")
                    node_response.append(node_step)
                    node_values = ET.Element("VALUES")
                    node_values.text = spec["ResponseValues"]
                    node_response.append(node_values)
            if len(l_l1bands):
                raise MajaPluginMuscateException(
                    "Missing spectral information for bands: " + str(l_l1bands)
                )

        # Update the SPATIAL_RESOLUTION nodes
        for b in l_BandsDefinitions.L2CoarseBandMap.keys():
            if self.OutResR3 and b in ["B1", "B9", "B10"]:
                continue
            if self.OutResR3:
                res_idx = 2
            else:
                b_res = l_BandsDefinitions.get_l1_resolution_for_band_code(b)
                if not b_res in l_ListOfL2Res:
                    continue
                res_idx = l_ListOfL2Res.index(b_res)
            l_SPATPath = "//Spectral_Band_Informations_List/Spectral_Band_Informations[@band_id='{}']/SPATIAL_RESOLUTION".format(
                b
            )
            node_spatial = xml_tools.get_only_value(output_handler.root, l_SPATPath)
            if l_isCarto:
                l_spatial_resolution_in = math.fabs(l_Areas[res_idx].spacing[0])
            else:
                l_ProductImageSizeY = float(l_Areas[res_idx].size[1])
                l_L1SizeY = float(l_l1info.AreaByResolution[res_idx].size[1])
                l_L1L2Ratio = l_ProductImageSizeY / l_L1SizeY
                LOGGER.debug(
                    "lIntL1L2Ratio: "
                    + str(l_L1L2Ratio)
                    + " computed with "
                    + str(l_ProductImageSizeY)
                    + " / "
                    + str(l_L1SizeY)
                )
                if l_L1L2Ratio == 0:
                    raise MajaPluginMuscateException(
                        "Internal error: the L1L2 ratio is null !!"
                    )
                l_spatial_resolution_in = int(
                    int(node_spatial.text) / l_L1L2Ratio + 0.5
                )
            node_spatial.text = "%d" % (l_spatial_resolution_in)

        # Removing information from L1 relative to bands not available in L2 product
        output_handler.remove_node(
            "//Group_Geopositioning_List/Group_Geopositioning[@group_id='PAN']"
        )
        output_handler.remove_node(
            "//Group_Geopositioning_List/Group_Geopositioning[@group_id='TH']"
        )
        if not self.OutResR3:
            output_handler.remove_node(
                "//Group_Geopositioning_List/Group_Geopositioning[@group_id='R3']"
            )

        # Get the parent node
        l_L2RejectedBands = self.plugin.L2RejectedBands
        l_NbL2RejectedBands = len(l_L2RejectedBands)
        # Remove the not L2 bands
        for bd in l_L2RejectedBands:
            output_handler.remove_node(
                "//Spectral_Band_Informations_List/Spectral_Band_Informations[@band_id='"
                + bd
                + "']"
            )
            output_handler.remove_node(
                "//Mean_Viewing_Incidence_Angle_List/Mean_Viewing_Incidence_Angle[@band_id='"
                + bd
                + "']"
            )
            output_handler.remove_node(
                "//Viewing_Incidence_Angles_Grids_List/Band_Viewing_Incidence_Angles_Grids_List[@band_id='"
                + bd
                + "']"
            )

        # Remove the not writed bands mtd
        if list_bands_to_write:
            for resol in range(l_NumberOfResolutions):
                l_StrResolution = l_BandsDefinitions.ListOfL2Resolution[resol]
                l_ListOfBand = l_BandsDefinitions.get_list_of_l2_band_code(
                    l_StrResolution
                )
                l_NumberOfBands = len(l_ListOfBand)
                for i in range(l_NumberOfBands):
                    if l_ListOfBand[i] not in list_bands_to_write:
                        xml_tools.remove_node(
                            output_handler.root,
                            f"//Mean_Viewing_Incidence_Angle_List/Mean_Viewing_Incidence_Angle[@band_id='{l_ListOfBand[i]}']",
                        )
                        xml_tools.remove_node(
                            output_handler.root,
                            f"//Viewing_Incidence_Angles_Grids_List/Band_Viewing_Incidence_Angles_Grids_List[@band_id='{l_ListOfBand[i]}']",
                        )
                        xml_tools.remove_node(
                            output_handler.root,
                            f"//Spectral_Band_Informations_List/Spectral_Band_Informations[@band_id='{l_ListOfBand[i]}']",
                        )

        # Node Quality_Informations
        #   - Source_Product
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/PRODUCT_ID",
            output_handler.get_string_value_of("ProductId"),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/ACQUISITION_DATE",
            output_handler.get_string_value_of("AcquisitionDate"),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/PRODUCTION_DATE",
            output_handler.get_string_value_of("ProductionDate"),
        )
        #   - Global_Index_List
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='CirrusDetected']",
            self.cirrusflag,
            check_if_present=True,
        )
        l1_handler = MuscateXMLFileHandler(self.l1imageinformationsproviderbase.HeaderFilename)
        cloudrate = l1_handler.get_l1_quality_index("CloudPercent")
        snowrate = l1_handler.get_l1_quality_index("SnowPercent")
        LOGGER.debug("Copy cloudrate and snowrate from L1C MTD")
        LOGGER.debug("cloudrate"+str(cloudrate))
        LOGGER.debug("snowrate"+str(cloudrate))

        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='CloudPercent']",
            float(cloudrate),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='SnowPercent']",
            float(snowrate),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='RainDetected']",
            self.rainflag,
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='HotSpotDetected']",
            self.hotspotflag,
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List[@level='N2']/Product_Quality/Global_Index_List/QUALITY_INDEX[@name='SunGlintDetected']",
            self.sunglintflag,
        )

        # Copy node <Product_Quality_List level="Geo">
        if "Node_Product_Quality_List_Geo" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_Product_Quality_List_Geo"],
                "Product_Quality_List",
                "//Current_Product/Product_Quality_List[@level='Geo']",
                check_if_present=True,
            )
        else:
            output_handler.remove_node(
                "//Current_Product/Product_Quality_List[@level='Geo']"
            )

        # Copy node <Product_Quality_List level="Natif">
        if "Node_Product_Quality_List_Natif" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_Product_Quality_List_Natif"],
                "Product_Quality_List",
                "//Current_Product/Product_Quality_List[@level='Natif']",
                check_if_present=True,
            )
        else:
            output_handler.remove_node(
                "//Current_Product/Product_Quality_List[@level='Natif']"
            )

        # Copy this node Processing_Jobs_List
        if "Node_Processing_Jobs_List" in l_Muscate:
            output_handler.replace_node(
                l_Muscate["Node_Processing_Jobs_List"],
                "Processing_Jobs_List",
                "//Production_Informations/Processing_Jobs_List",
                check_if_present=True,
            )
        else:
            l_baseJobXpath = "//Production_Informations/Processing_Jobs_List/Job"
            l_workplan = (
                    "generique_l2_"
                    + date_utils.get_date_yyyymmdd_from_tm(l_l1info.ProductDate)
                    + "_01"
            )
            output_handler.set_string_value(l_baseJobXpath + "/WORKPLAN_ID", l_workplan)
            output_handler.set_string_value(
                l_baseJobXpath + "/PRODUCER_NAME", "MUSCATE"
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/START_PRODUCTION_DATE", l_l1info.GenerationDateStr
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/END_PRODUCTION_DATE", l_l1info.GenerationDateStr
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Inputs_List/PRODUCT/PRODUCT_ID",
                l_l1info.ProductId,
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Inputs_List/PRODUCT/ACQUISITION_DATE",
                l_l1info.AcquisitionStart,
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Inputs_List/PRODUCT/PRODUCTION_DATE",
                l_l1info.GenerationDateStr,
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Outputs_List/PRODUCT/PRODUCT_ID",
                output_handler.get_string_value_of("ProductId"),
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Outputs_List/PRODUCT/ACQUISITION_DATE",
                l_l1info.AcquisitionStart,
            )
            output_handler.set_string_value(
                l_baseJobXpath + "/Products_List/Outputs_List/PRODUCT/PRODUCTION_DATE",
                output_handler.get_string_value_of("AcquisitionDate"),
            )

        # Auxiliary Data List
        l_baseAuxpath = "//Production_Informations/Exogenous_Data_List"
        # GIPP Download URL
        output_handler.set_string_value(
            "//GIPP_Download_URL",
            "https://gitlab.orfeo-toolbox.org/maja/maja-gipp2/-/tree/maja-"
            + version.MAJA_VERSION_MAJOR
            + "."
            + version.MAJA_VERSION_MINOR,
        )
        # List of GIPPS
        listOfGipps = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            self.listofgippsfilenames
        )
        output_handler.write_list_of_gipp_files(
            listOfGipps, p_xpath=l_baseAuxpath + "/List_of_GIPP_Files"
        )
        # CAMS
        if self.usecams:
            l_ListOfCAMSDataFilenames = gipp_utils.get_list_of_gipp_filenames(
                self.apphandler.get_input_directory(), "EXO_CAMS", True
            )
            listOfCams = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
                l_ListOfCAMSDataFilenames
            )
            output_handler.write_list_of_cams_files(listOfCams)

        # ERA5
        l_ListOfERA5DataFilenames = gipp_utils.get_list_of_gipp_filenames(
            self.apphandler.get_input_directory(), "EXO_ERA5", True
        )
        listOfERA5 = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            l_ListOfERA5DataFilenames
        )
        output_handler.write_list_of_era5_files(listOfERA5)

        # DEM file
        demfilename = self.dem.ALT_LogicalName.replace("LOCAL=", "")
        output_handler.set_string_value("//Used_DEM/DEM_Reference", demfilename)
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Min", f"{self.dem.ALT_Min:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Max", f"{self.dem.ALT_Max:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Average", f"{self.dem.ALT_Mean:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Standard_Deviation", f"{self.dem.ALT_Stdv:.9f}"
        )
        # Meteo files
        l_ListOfMETDataFilenames = gipp_utils.get_list_of_gipp_filenames(
            self.apphandler.get_input_directory(), "EXO_METDTA", True
        )
        listOfMetfiles = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            l_ListOfMETDataFilenames
        )
        output_handler.write_list_of_meteo_files(listOfMetfiles)

        output_handler.save_to_file(output_filename)
        # Check the GLOBAL header even if  not valid (NOTV)
        if self.checkxmlfileswithschema:
            xml_tools.check_xml(output_filename, schemaLocationDirectory)


class MajaSpot1MuscateL2HeaderWriter(MajaSpotMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpot1MuscateL2HeaderWriter, self).__init__()
        self.plugin = MajaSpot1MuscatePlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "SPOT1_MUSCATE" or plugin_name == "SPOT1":
            self.apphandler = app_handler
            return True
        else:
            return False


class MajaSpot2MuscateL2HeaderWriter(MajaSpotMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpot2MuscateL2HeaderWriter, self).__init__()
        self.plugin = MajaSpot2MuscatePlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "SPOT2_MUSCATE" or plugin_name == "SPOT2":
            self.apphandler = app_handler
            return True
        else:
            return False


class MajaSpot3MuscateL2HeaderWriter(MajaSpotMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpot3MuscateL2HeaderWriter, self).__init__()
        self.plugin = MajaSpot3MuscatePlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "SPOT3_MUSCATE" or plugin_name == "SPOT3":
            self.apphandler = app_handler
            return True
        else:
            return False


class MajaSpot4MuscateL2HeaderWriter(MajaSpotMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpot4MuscateL2HeaderWriter, self).__init__()
        self.plugin = MajaSpot4MuscatePlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "SPOT4_MUSCATE" or plugin_name == "SPOT4":
            self.apphandler = app_handler
            return True
        else:
            return False


class MajaSpot5MuscateL2HeaderWriter(MajaSpotMuscateL2HeaderWriter):
    def __init__(self):
        super(MajaSpot5MuscateL2HeaderWriter, self).__init__()
        self.plugin = MajaSpot5MuscatePlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "SPOT5_MUSCATE" or plugin_name == "SPOT5":
            self.apphandler = app_handler
            return True
        else:
            return False
