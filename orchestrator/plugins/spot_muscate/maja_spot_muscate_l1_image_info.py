# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.maja_dataset -- shortdesc

orchestrator.plugins.maja_dataset is a description

It defines classes_and_methods


###################################################################################################
"""
from orchestrator.common.muscate.muscate_xml_file_handler import MuscateXMLFileHandler
from orchestrator.common.muscate.muscate_uii_xml_file_handler import (
    MuscateUIIXMLFileHandler,
)
from orchestrator.plugins.common.muscate.maja_muscate_l1_image_info import (
    MajaMuscateL1ImageInformations,
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_plugin import (
    MajaSpot1MuscatePlugin, MajaSpot2MuscatePlugin, MajaSpot3MuscatePlugin, MajaSpot4MuscatePlugin,
    MajaSpot5MuscatePlugin
)
import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.logger.maja_logging import configure_logger

LOGGER = configure_logger(__name__)


class MajaSpot1MuscateL1ImageInformations(MajaMuscateL1ImageInformations):
    def __init__(self):
        super(MajaSpot1MuscateL1ImageInformations, self).__init__()
        self._plugin = MajaSpot1MuscatePlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite
        self.IS_TILE_DEPENDENT = False

    def detect_l1_products(self, input_directory, product_list):
        LOGGER.info("Start SpotMuscate L1 DetectL1Products in " + input_directory)
        MajaMuscateL1ImageInformations.muscate_detect_l1_products(
            input_directory, product_list, self._plugin
        )

    def initialize(self, product_filename, validate=False, schema_path=None):
        LOGGER.info("Start SpotMuscate L1 Initialize on product " + product_filename)
        self.SpectralContent = "XS"
        return self.muscate_initialize(product_filename, self._plugin)


class MajaSpot2MuscateL1ImageInformations(MajaSpot1MuscateL1ImageInformations):
    def __init__(self):
        super(MajaSpot2MuscateL1ImageInformations, self).__init__()
        self._plugin = MajaSpot2MuscatePlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite


class MajaSpot3MuscateL1ImageInformations(MajaSpot1MuscateL1ImageInformations):
    def __init__(self):
        super(MajaSpot3MuscateL1ImageInformations, self).__init__()
        self._plugin = MajaSpot3MuscatePlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite


class MajaSpot4MuscateL1ImageInformations(MajaSpot1MuscateL1ImageInformations):
    def __init__(self):
        super(MajaSpot4MuscateL1ImageInformations, self).__init__()
        self._plugin = MajaSpot4MuscatePlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite


class MajaSpot5MuscateL1ImageInformations(MajaSpot1MuscateL1ImageInformations):
    def __init__(self):
        super(MajaSpot5MuscateL1ImageInformations, self).__init__()
        self._plugin = MajaSpot5MuscatePlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite
