# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||)<
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1_plugin -- shortdesc

orchestrator.plugins.sentinel2.maja_sentinel2_l1_plugin is a description

It defines classes_and_methods

###################################################################################################
"""

from orchestrator.plugins.common.base.maja_plugin_base import PluginBase
from orchestrator.plugins.common.base.bands_definition import SingleResolutionImageInfo
from orchestrator.common.constants import *


class MajaSpotMuscatePlugin(PluginBase):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpotMuscatePlugin, self).__init__()
        self.PluginName = "SPOT_MUSCATE"
        self.UniqueSatellite = "SPOT"
        self.ShortFileType = "SPOT"
        self.Instrument = ""
        self.ListOfSatellites.append("SPOT")

        self.QCK_PREFIX_MAP["SPOT"] = "SPOT"
        self.XSD_PREFIX_MAP["SPOT"] = "SPOT_"
        # Resolution/bands fill
        self.ListOfL1Resolutions.append("XS")
        self.ListOfL2Resolutions.append("XS")
        self.L1BandCharIdList = []
        # -----------------------------------------------------------------
        # Sorted list of band char id used in the  tile PDI_ID
        self.L1BandCharIdList.append("XS1")
        self.L1BandCharIdList.append("XS2")
        self.L1BandCharIdList.append("XS3")
        # General camera configuration
        self.WebSiteURL = ""
        self.ReferenceDate = "20000101"
        self.Creator_Version = "04.00.00"
        self.WideFieldSensor = False
        self.GRIDReferenceAltitudesSOL2GridAvailable = False
        self.WaterVapourDetermination = False
        self.CloudDetermination = False
        self.CirrusFlag = False
        self.CirrusMasking = False
        self.SnowMasking = False
        self.WaterMasking = False
        self.RainDetection = False
        self.HasSwirBand = False
        self.DirectionalCorrection = False
        self.CloudMaskingKnownCloudsAltitude = False
        self.DFPMasking = False
        self.CompositeAlgorithm = False


        # Fill BandsDefinition
        # ---------------------------------------------------------------------------------------------
        # Init the list of L2 resolution
        self.BandsDefinitions.ListOfL2Resolution.append("XS")
        # ---------------------------------------------------------------------------------------------
        # Init the list of L1 resolution
        self.BandsDefinitions.ListOfL1Resolution.append("XS")
        # ---------------------------------------------------------------------------------------------
        # List of indices for the L1 bands
        self.BandsDefinitions.L2CoarseBandMap["XS1"] = 0
        self.BandsDefinitions.L2CoarseBandMap["XS2"] = 1
        self.BandsDefinitions.L2CoarseBandMap["XS3"] = 2

        # Init the list of L2 coarse resolution bands

        # ---------------------------------------------------------------------------------------------
        # ---------------------------------------------------------------------------------------------
        # Definition of the 3 resolutions
        l_XSImageInfo = SingleResolutionImageInfo()
        l_XSImageInfo.Resolution = 20

        # ---------------------------------------------------------------------------------------------
        l_XSImageInfo.ListOfBandCode.append("XS1")
        l_XSImageInfo.ListOfBandCode.append("XS2")
        l_XSImageInfo.ListOfBandCode.append("XS3")
        self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = l_XSImageInfo

        # ---------------------------------------------------------------------------------------------
        # L1
        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1BandMap = self.BandsDefinitions.L2CoarseBandMap
        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1ListOfBandsMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ].ListOfBandCode


        self.MAJA_INSTALL_SCHEMAS_DIR = "SPOT_MUSCATE"

        # TODO
        self.TEMPLATE_TEC_PRIVATE_EEF = (
            "SPOT_MUSCATE/Templates/L2.product/TEC_PRIVATE.EEF"
        )

        # Public Templates
        self.TEMPLATE_PDTQKL_JPG = (
            "SPOT_MUSCATE/Templates/L2.product/DEFAULT_QKL_ALL.jpg"
        )

        # GLOBAL
        self.TEMPLATE_GLOBAL_HDR = "SPOT_MUSCATE/Templates/L2.product/MTD_ALL.xml"
        self.MAJA_INSTALL_SCHEMAS_L2_DIR = "SPOT_MUSCATE/PSC-SL-411-0032-CG_Schemas"

        # -----------------------------------------------------------------------------
        # MACCS VENUS File_Version
        self.L2PRODUCT_FILE_VERSION = "0003"
        self.L3PRODUCT_FILE_VERSION = "0002"
        # Quicklook product (QCK)
        self.L1QCK_FILE_VERSION = "0002"
        self.L2QCK_FILE_VERSION = "0002"
        self.L3QCK_FILE_VERSION = "0002"


class MajaSpot1MuscatePlugin(MajaSpotMuscatePlugin):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpot1MuscatePlugin, self).__init__()
        self.PluginName = "SPOT1_MUSCATE"
        self.UniqueSatellite = "SPOT1"
        self.ShortFileType = "SPOT1"
        self.Instrument = ""
        self.ListOfSatellites = []
        self.ListOfSatellites.append("SPOT1")


class MajaSpot2MuscatePlugin(MajaSpot1MuscatePlugin):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpot2MuscatePlugin, self).__init__()
        self.PluginName = "SPOT2_MUSCATE"
        self.UniqueSatellite = "SPOT2"
        self.ShortFileType = "SPOT2"
        self.Instrument = ""
        self.ListOfSatellites = []
        self.ListOfSatellites.append("SPOT2")


class MajaSpot3MuscatePlugin(MajaSpot1MuscatePlugin):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpot3MuscatePlugin, self).__init__()
        self.PluginName = "SPOT3_MUSCATE"
        self.UniqueSatellite = "SPOT3"
        self.ShortFileType = "SPOT3"
        self.Instrument = ""
        self.ListOfSatellites = []
        self.ListOfSatellites.append("SPOT3")


class MajaSpot4MuscatePlugin(MajaSpot1MuscatePlugin):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpot4MuscatePlugin, self).__init__()
        self.PluginName = "SPOT4_MUSCATE"
        self.UniqueSatellite = "SPOT4"
        self.ShortFileType = "SPOT4"
        self.Instrument = ""
        self.ListOfSatellites = []
        self.ListOfSatellites.append("SPOT4")

        # Add band SWIR
        self.L1BandCharIdList.append("SWIR")
        self.BandsDefinitions.L2CoarseBandMap["SWIR"] = 3
        # Update list for SWIR
        l_XSImageInfo = SingleResolutionImageInfo()
        l_XSImageInfo.Resolution = 20
        l_XSImageInfo.ListOfBandCode.append("XS1")
        l_XSImageInfo.ListOfBandCode.append("XS2")
        l_XSImageInfo.ListOfBandCode.append("XS3")
        l_XSImageInfo.ListOfBandCode.append("SWIR")
        self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = l_XSImageInfo

        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1BandMap = self.BandsDefinitions.L2CoarseBandMap
        self.BandsDefinitions.L1ListOfBandsMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ].ListOfBandCode

        # GLOBAL
        self.TEMPLATE_GLOBAL_HDR = "SPOT4_MUSCATE/Templates/L2.product/MTD_ALL.xml"
        self.MAJA_INSTALL_SCHEMAS_DIR = "SPOT4_MUSCATE"
        self.MAJA_INSTALL_SCHEMAS_L2_DIR = "SPOT4_MUSCATE/PSC-SL-411-0032-CG_Schemas"


class MajaSpot5MuscatePlugin(MajaSpot1MuscatePlugin):
    """
    classdocs
    """

    def __init__(self):
        super(MajaSpot5MuscatePlugin, self).__init__()
        self.PluginName = "SPOT5_MUSCATE"
        self.UniqueSatellite = "SPOT5"
        self.ShortFileType = "SPOT5"
        self.Instrument = ""
        self.ListOfSatellites = []
        self.ListOfSatellites.append("SPOT5")

        # Add band SWIR
        self.L1BandCharIdList.append("SWIR")
        self.BandsDefinitions.L2CoarseBandMap["SWIR"] = 3
        # Update list for SWIR and for the new resolution
        l_XSImageInfo = SingleResolutionImageInfo()
        l_XSImageInfo.Resolution = 10
        l_XSImageInfo.ListOfBandCode.append("XS1")
        l_XSImageInfo.ListOfBandCode.append("XS2")
        l_XSImageInfo.ListOfBandCode.append("XS3")
        l_XSImageInfo.ListOfBandCode.append("SWIR")
        self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = l_XSImageInfo

        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1BandMap = self.BandsDefinitions.L2CoarseBandMap
        self.BandsDefinitions.L1ListOfBandsMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ].ListOfBandCode

        # GLOBAL
        self.TEMPLATE_GLOBAL_HDR = "SPOT5_MUSCATE/Templates/L2.product/MTD_ALL.xml"
        self.MAJA_INSTALL_SCHEMAS_DIR = "SPOT5_MUSCATE"
        self.MAJA_INSTALL_SCHEMAS_L2_DIR = "SPOT5_MUSCATE/PSC-SL-411-0032-CG_Schemas"


