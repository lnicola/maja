# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     |||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1_image_file_reader -- shortdesc

orchestrator.plugins.maja_sentinel2_l1_image_file_reader is a description

It defines classes_and_methods

###################################################################################################
"""
import os
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.algorithms.otb_resample import resample
from orchestrator.cots.otb.algorithms.otb_resample import OtbResampleType
from orchestrator.cots.otb.algorithms.otb_band_math import band_math
from orchestrator.plugins.spot_muscate.maja_spot_muscate_plugin import (
    MajaSpot1MuscatePlugin, MajaSpot2MuscatePlugin, MajaSpot3MuscatePlugin, MajaSpot4MuscatePlugin,
    MajaSpot5MuscatePlugin
)
from orchestrator.cots.otb.algorithms.otb_multiply_by_scalar import multiply_by_scalar
from orchestrator.plugins.common.muscate.maja_muscate_l1_image_file_reader import (
    MuscateL1ImageFileReaderBase,
)

LOGGER = configure_logger(__name__)


class Spot1MuscateL1ImageFileReader(MuscateL1ImageFileReaderBase):
    def __init__(self):
        super(Spot1MuscateL1ImageFileReader, self).__init__()
        self._Satellite = "SPOT1"
        self._plugin = MajaSpot1MuscatePlugin()

    def can_read(self, plugin_name):
        return plugin_name == "SPOT1_MUSCATE"


class Spot2MuscateL1ImageFileReader(MuscateL1ImageFileReaderBase):
    def __init__(self):
        super(Spot2MuscateL1ImageFileReader, self).__init__()
        self._Satellite = "SPOT2"
        self._plugin = MajaSpot2MuscatePlugin()

    def can_read(self, plugin_name):
        return plugin_name == "SPOT2_MUSCATE"


class Spot3MuscateL1ImageFileReader(MuscateL1ImageFileReaderBase):
    def __init__(self):
        super(Spot3MuscateL1ImageFileReader, self).__init__()
        self._Satellite = "SPOT3"
        self._plugin = MajaSpot3MuscatePlugin()

    def can_read(self, plugin_name):
        return plugin_name == "SPOT3_MUSCATE"


class Spot4MuscateL1ImageFileReader(MuscateL1ImageFileReaderBase):
    def __init__(self):
        super(Spot4MuscateL1ImageFileReader, self).__init__()
        self._Satellite = "SPOT4"
        self._plugin = MajaSpot4MuscatePlugin()

    def can_read(self, plugin_name):
        return plugin_name == "SPOT4_MUSCATE"


class Spot5MuscateL1ImageFileReader(MuscateL1ImageFileReaderBase):
    def __init__(self):
        super(Spot5MuscateL1ImageFileReader, self).__init__()
        self._Satellite = "SPOT5"
        self._plugin = MajaSpot5MuscatePlugin()

    def can_read(self, plugin_name):
        return plugin_name == "SPOT5_MUSCATE"