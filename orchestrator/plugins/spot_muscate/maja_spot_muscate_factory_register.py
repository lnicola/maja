# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1_angles -- shortdesc

orchestrator.plugins.sentinel2.maja_sentinel2_l1_angles is a description

It defines classes_and_methods



###################################################################################################
"""
from orchestrator.plugins.common.factory.factory_base import FactoryBase
from orchestrator.plugins.spot_muscate.maja_spot_muscate_l1_image_info import (
    MajaSpot1MuscateL1ImageInformations, MajaSpot2MuscateL1ImageInformations, MajaSpot3MuscateL1ImageInformations,
    MajaSpot4MuscateL1ImageInformations, MajaSpot5MuscateL1ImageInformations
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_l1_image_file_reader import (
    Spot1MuscateL1ImageFileReader, Spot2MuscateL1ImageFileReader, Spot3MuscateL1ImageFileReader,
    Spot4MuscateL1ImageFileReader, Spot5MuscateL1ImageFileReader
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_l2_image_writer import (
    MajaSpot1MuscateL2ImageWriter, MajaSpot2MuscateL2ImageWriter, MajaSpot3MuscateL2ImageWriter,
    MajaSpot4MuscateL2ImageWriter, MajaSpot5MuscateL2ImageWriter
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_l2_header_writer import (
    MajaSpot1MuscateL2HeaderWriter, MajaSpot2MuscateL2HeaderWriter, MajaSpot3MuscateL2HeaderWriter,
    MajaSpot4MuscateL2HeaderWriter, MajaSpot5MuscateL2HeaderWriter
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_l2_image_reader import (
    MajaSpot1MuscateL2ImageReader, MajaSpot2MuscateL2ImageReader, MajaSpot3MuscateL2ImageReader,
    MajaSpot4MuscateL2ImageReader, MajaSpot5MuscateL2ImageReader
)
from orchestrator.plugins.spot_muscate.maja_spot_muscate_plugin import (
    MajaSpotMuscatePlugin, MajaSpot1MuscatePlugin, MajaSpot2MuscatePlugin, MajaSpot3MuscatePlugin, MajaSpot4MuscatePlugin,
    MajaSpot5MuscatePlugin
)


def register_all():
    """
        Add all factory for SPOT 1-5
    """
    # Register L1ImageInformationsBase
    FactoryBase.register("L1ImageInformationsBase", MajaSpot1MuscateL1ImageInformations)
    FactoryBase.register("L1ImageInformationsBase", MajaSpot2MuscateL1ImageInformations)
    FactoryBase.register("L1ImageInformationsBase", MajaSpot3MuscateL1ImageInformations)
    FactoryBase.register("L1ImageInformationsBase", MajaSpot4MuscateL1ImageInformations)
    FactoryBase.register("L1ImageInformationsBase", MajaSpot5MuscateL1ImageInformations)
    # Register PluginBase
    FactoryBase.register("PluginBase", MajaSpotMuscatePlugin)
    FactoryBase.register("PluginBase", MajaSpot1MuscatePlugin)
    FactoryBase.register("PluginBase", MajaSpot2MuscatePlugin)
    FactoryBase.register("PluginBase", MajaSpot3MuscatePlugin)
    FactoryBase.register("PluginBase", MajaSpot4MuscatePlugin)
    FactoryBase.register("PluginBase", MajaSpot5MuscatePlugin)
    # Register L1ImageReaderBase
    FactoryBase.register("L1ImageReaderBase", Spot1MuscateL1ImageFileReader)
    FactoryBase.register("L1ImageReaderBase", Spot2MuscateL1ImageFileReader)
    FactoryBase.register("L1ImageReaderBase", Spot3MuscateL1ImageFileReader)
    FactoryBase.register("L1ImageReaderBase", Spot4MuscateL1ImageFileReader)
    FactoryBase.register("L1ImageReaderBase", Spot5MuscateL1ImageFileReader)
    # Register L2ImageWriterBase
    FactoryBase.register("L2ImageWriterBase", MajaSpot1MuscateL2ImageWriter)
    FactoryBase.register("L2ImageWriterBase", MajaSpot2MuscateL2ImageWriter)
    FactoryBase.register("L2ImageWriterBase", MajaSpot3MuscateL2ImageWriter)
    FactoryBase.register("L2ImageWriterBase", MajaSpot4MuscateL2ImageWriter)
    FactoryBase.register("L2ImageWriterBase", MajaSpot5MuscateL2ImageWriter)
    # Register L2HeaderWriterBase
    FactoryBase.register("L2HeaderWriterBase", MajaSpot1MuscateL2HeaderWriter)
    FactoryBase.register("L2HeaderWriterBase", MajaSpot2MuscateL2HeaderWriter)
    FactoryBase.register("L2HeaderWriterBase", MajaSpot3MuscateL2HeaderWriter)
    FactoryBase.register("L2HeaderWriterBase", MajaSpot4MuscateL2HeaderWriter)
    FactoryBase.register("L2HeaderWriterBase", MajaSpot5MuscateL2HeaderWriter)
    # Register L2ImageReaderBase
    FactoryBase.register("L2ImageReaderBase", MajaSpot1MuscateL2ImageReader)
    FactoryBase.register("L2ImageReaderBase", MajaSpot2MuscateL2ImageReader)
    FactoryBase.register("L2ImageReaderBase", MajaSpot3MuscateL2ImageReader)
    FactoryBase.register("L2ImageReaderBase", MajaSpot4MuscateL2ImageReader)
    FactoryBase.register("L2ImageReaderBase", MajaSpot5MuscateL2ImageReader)
