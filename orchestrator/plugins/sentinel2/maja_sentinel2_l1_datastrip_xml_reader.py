# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1atastrip_xml_reader --

orchestrator.plugins.sentinel2.maja_sentinel2_l1atastrip_xml_reader is a description

It defines classes_and_methods

###################################################################################################
"""


import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.maja_common import *
from orchestrator.common.earth_explorer.earth_explorer_xml_file_handler import (
    EarthExplorerXMLFileHandler,
)
import os

LOGGER = configure_logger(__name__)

XPATH_DS_S2 = {
    "refining_status": "//Geometric_Info/Image_Refining",
}


class MajaSentinel2L1DatastripXmlReader(EarthExplorerXMLFileHandler):
    """
    classdocs
    """

    def __init__(self, datastrip_xml_file):
        """
        Constructor
        """
        super(MajaSentinel2L1DatastripXmlReader, self).__init__(datastrip_xml_file)
        self.datastrip_xml_file = datastrip_xml_file
        self.refining_status = xml_tools.get_attribute(
                self.root, XPATH_DS_S2.get("refining_status"), "flag", check=True
            )
