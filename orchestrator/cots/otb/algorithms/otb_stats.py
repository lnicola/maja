# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.cots.otb.otb_band_math -- shortdesc

orchestrator.cots.otb.otb_band_math is a description

It defines classes_and_methods

###################################################################################################
"""


from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler

LOGGER = configure_logger(__name__)


def stats(
    input_file_path,
    exclude_value=None,
    input_mask=None,
    mask_foreground=None,
    write_output=True,
):
    """
    Instantiate the OTB application Stats
    :param input_file_path: Path to the input file
    :param exclude_value: The excluded value
    :type exclude_value: float
    :param input_mask: Path to the masking image
    :param mask_foreground: Mask Foreround value
    :type mask_foreground: float
    :param write_output: Write the output image to the disk if true
    :type write_output: bool
    :return: The Stats OTB application
    """
    param_stats = {"im": input_file_path}

    if input_mask is not None:
        param_stats["mask"] = input_mask
    if mask_foreground is not None:
        param_stats["maskforeground"] = mask_foreground
    if exclude_value is not None:
        param_stats["exclude"] = exclude_value

    app = OtbAppHandler("Stats", param_stats, write_output)

    return app
