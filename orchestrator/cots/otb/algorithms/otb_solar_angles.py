# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################
orchestrator.cots.otb.otb_solar_angles -- shortdesc

orchestrator.cots.otb.otb_solar_angles is a description

It defines classes_and_methods


###################################################################################################
"""

from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common.maja_exceptions import MajaNotYetImplemented
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler

LOGGER = configure_logger(__name__)


def solar_angle(
    solar_angle_image,
    sol1_height,
    sol_href,
    output_image,
    sol2_in=None,
    sol2_height=None,
    write_output=True,
):
    """
    Instantiate the OTB application SolarAngle
    :param solar_angle_image: Path to the image used as solar angle 1
    :param sol1_height: Height used to compute solar angle 1
    :type sol1_height: int
    :param sol_href: Solar height ref
    :type sol_href: int
    :param output_image: Path to the output solar angle image
    :param sol2_in: Path to the image used as solar angle 2
    :param sol2_height: Height used to compute solar angle 2
    :type sol2_height: int
    :param write_output: Write the output image to the disk if true
    :type write_output: bool
    :return: The SolarAngle OTB application
    """
    # test about input and raise MajaNotYetImplemented is case of sol2 case
    if sol2_in is not None or sol2_height is not None:
        raise MajaNotYetImplemented()

    parameters = {
        "sol1.in": solar_angle_image,
        "sol1.h": sol1_height,
        "solhref": sol_href,
        "sol": output_image,
    }

    app = OtbAppHandler("SolarAngle", parameters, write_output)

    return app
